(function (window) {
	ERP = new ERP();
	ERP.init();
})(window);
function ERP() {
	var self = this;
	var token = $("meta[name='csrf-token']").attr("content");
    $("input.edit-test").attr("disabled", true);
    $(".edit-account").attr("disabled", true);
    $('#exper .btn-box').css('display', 'none');
    $('.upload-file-container input').css('display', 'none');
    $('#formCreateEmp').find('.error').text('');
    $('#form-detailEmp').find('.error').text('');
	this.init = function () {
        self.eventBtnEdit();
        self.eventLinkEdit();
        self.modalEditOpen();
        self.modalEditClose();
        self.modalCreateOpen();
        self.editImageEmployee();
        self.uploadFile();
        self.addEmployer();
        self.removeEmployer();
        // self.updateEmployer();
        self.setValidationFormCreate();
        // self.setValidationFormEdit();
        self.addHistoryEmployer();
        self.removeHistoryEmployer();
        self.eventCheckboxActive();
	}
    self.setValidationFormCreate = function () {
	    var url_check = $("#formCreateEmp").attr('ajax-check');
        $("#formCreateEmp").validate({
            ignore: ":hidden:not(select)",
            rules: {
                name: "required",
                phone: {
                    digits: true,
                    minlength: 10,
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        type: 'POST',
                        url: url_check,
                        data: {
                            '_token' : token
                        }
                    }
                },
                personal_id: {
                    required: true,
                    digits: true,
                    remote: {
                        type: 'POST',
                        url: url_check,
                        data: {
                            '_token' : token
                        }
                    }
                },
            },
            messages: {
                name: "Vui lòng nhập họ tên",
                phone: {
                    digits: "Giá trị phải là số ",
                    minlength: "Số điện thoại phải ít nhất 10 số",
                },
                email: {
                    required: "Vui lòng nhập Email",
                    email: "Địa chỉ email không hợp lệ",
                    remote: "Email đã tồn tại",
                },
                personal_id: {
                    required: "Vui lòng nhập số chứng minh nhân dân",
                    digits: "Giá trị phải là số ",
                    remote: "CMND đã tồn tại",
                },
            }
        });
    }

    self.eventBtnEdit = function () {
        $('#detailEmp .btn-edit').click(function () {
            $("input.edit-test").prop("disabled", false);
            $(".edit-test").prop("disabled", false);
            $('#detailEmp .btn.btn-success').css('display', 'block');
            $('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');
            $('.upload-file-container input').css('display', 'block');
            $('.upload-file-container ').addClass('avatar');
            $('#exper .btn-box').css('display', 'block');
        });
    }
    self.eventLinkEdit = function () {
        $('.group-employee .link-edit').click(function () {
            $("input.edit-test").attr("disabled", false);
            $(".edit-test").prop("disabled", false);
            $('#detailEmp .btn.btn-success').css('display', 'block');
            $('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');
            $('.upload-file-container input').css('display', 'block');
            $('#exper .btn-box').css('display', 'block');

        });
    }
    self.modalCreateOpen = function () {
        $('#createEmp').on('shown.bs.modal', function() {
            $('#formCreateEmp').find('.error').text('');
        });
    }
    self.modalEditOpen = function () {
        $('#detailEmp').on('shown.bs.modal', function() {
            $('#form-detailEmp').find('.error').text('');
            var url_check = $("#form-detailEmp").attr('ajax-check');
            var id = $("#form-detailEmp input[name=_id]").val();
            $("#form-detailEmp").validate({
                ignore: ":hidden:not(select)",
                rules: {
                    name: "required",
                    phone: {
                        digits: true,
                        minlength: 10,
                    },
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            type: 'POST',
                            url: url_check,
                            data: {
                                '_token' : token,
                                '_id' : id,
                            }
                        }
                    },
                    personal_id: {
                        required: true,
                        digits: true,
                        remote: {
                            type: 'POST',
                            url: url_check,
                            data: {
                                '_token' : token,
                                '_id' : id,
                            }
                        }
                    },
                },
                messages: {
                    name: "Vui lòng nhập họ tên",
                    phone: {
                        digits: "Giá trị phải là số ",
                        minlength: "Số điện thoại phải ít nhất 10 số",
                    },
                    email: {
                        required: "Vui lòng nhập Email",
                        email: "Địa chỉ email không hợp lệ",
                        remote: "Email đã tồn tại",
                    },
                    personal_id: {
                        required: "Vui lòng nhập số chứng minh nhân dân",
                        digits: "Giá trị phải là số ",
                        remote: "CMND đã tồn tại",
                    },
                },
                submitHandler: function (form,event) {
                    event.preventDefault();
                    var form = $('form#form-detailEmp')

                    var url = form.attr('action');

                    var data = $('form#form-detailEmp').serialize();
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: data,
                        dataType: "json",
                        success: function (getdata) {
                            console.log(getdata);

                            if (getdata.result) {

                                var id = ( ( getdata.data._id.$oid === null  ) ? '' : getdata.data._id.$oid);
                                var name = ( ( getdata.data.name === null  ) ? '' : getdata.data.name);
                                var ngaysinh = ( ( getdata.data.ngaysinh === null  ) ? '' : getdata.data.ngaysinh);
                                var dept = ( ( getdata.data.dept === null  ) ? '' : getdata.data.dept);
                                var res = ( ( getdata.data.res === null  ) ? '' : getdata.data.res);
                                var employerUpdate = $("[data-id=" + id + "]");
                                employerUpdate.text('');
                                var html =
                                    '<div class="img" style="background-image: url(' + getdata.data.url_image + ');"></div>'
                                    + '<div class="content">'
                                    + '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name btn-edit" employer-id="'+id+'">' + name + '</a>'
                                    + '<p>- Ngày sinh: ' + ngaysinh + '</p>'
                                    + '<p>- Phòng ban: ' + dept + '</p>'
                                    + '<p>- Chức vụ: ' + res + '</p>'
                                    + '</div>'
                                    + '<div class="checkbox checkbox-success ">'
                                    + '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'
                                    + '<label for="checkbox-' + id + '"></label>'
                                    + '</div>'
                                    + '<a href="javascript:void(0)" class="link-edit btn-edit" data-toggle="modal" data-target="#detailEmp" employer-id="'+id+'"><i class="fa fa-pencil "></i></a>'

                                employerUpdate.append(html);
        //                        $('#formCreateEmp').trigger("reset");
                                $('#detailEmp').modal('hide');
                            }
                        }
                    });
                    return false;
                }
            });
        })
    }
    self.modalEditClose = function () {
        $('#detailEmp').on('hidden.bs.modal', function () {
            $('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'block');
            $(".edit-test").prop("disabled", true);
            $('#detailEmp .btn.btn-success').css('display', 'none');
            $('.upload-file-container input').css('display', 'none');
            $('.upload-file-container ').removeClass('avatar');
            $('#exper .btn-box').css('display', 'none');

        })
    }
    self.editImageEmployee = function () {
        $(".employee-image-edit").change(function () {

            var fileExtension = ['jpg', 'png', 'gif'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Only formats are allowed : " + fileExtension.join(', '));
                return;
            }
            var selectedFile = $('.employee-image-edit').prop('files')[0];
            var imgtag = document.getElementById("preview");
            var reader = new FileReader();

            var previewImage = $('.upload-file-container ');


            reader.readAsDataURL(selectedFile);

            var id = $('#form-detailEmp').find('input[name="_id"]').val();
            reader.onload = function (event) {
                //                        imgtag.src = event.target.result;
                //                        $('.upload-file-container').css('abc', 'display:none');

                $("[data-id=" + id + "] > .img").css('background-image', 'url(' + event.target.result + ')');
            };
            var employChild = $("[data-id=" + id + "] > .img");

            var data = new FormData();

            data.append('img', selectedFile);

            data.append('_id', id);

            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/erp/employer/uploadimage',
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {
                    var img = data.data.url_image;
                    img = img + "?t=" + new Date().getTime();
                    $(".upload-file-container").css('background-image', 'url(' + img + ')');
                    //$(".upload-file-container").trigger("chosen:update");
                }
            });
            return false;
        });
    }
    self.uploadFile = function () {
        $(".upload-file .choose-file").change(function () {
            var fileExtension = ['xls', 'xlsx'];
            var empGroup = $(".group-employee");
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Only formats are allowed : " + fileExtension.join(', '));
                return;
            }
            var data = new FormData();
            data.append('file', $('.upload-file .choose-file').prop('files')[0]);
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: '/erp/employer/upload',
                data: data,
                processData: false,
                contentType: false,
                success: function (data) {
                    empGroup.text('');
                    for (i = 0; i < (data.data).length; i++) {
                        console.log(data.data[i].address);
                        var row = data.data[i]
                        var id = ( ( row._id.$oid === undefined || row._id.$oid === null  ) ? '' : row._id.$oid);
                        var ngaysinh = ( ( row.ngaysinh === undefined || row.ngaysinh === null  ) ? '' : row.ngaysinh);
                        var dept = ( ( row.dept === undefined || row.dept === null  ) ? '' : row.dept);
                        var name = ( ( row.name === undefined || row.name === null  ) ? '' : row.name);
                        var res = ( ( row.res === undefined || row.res === null   ) ? '' : row.res);
                        var image = ( ( row.url_image === undefined || row.url_image === null  ) ? '' : row.url_image);
                        var html = '<div class="employee-item" data-id="' + id + '">'
                            + '<div class="img" style="background-image: url(' + image + ');"></div>'
                            + '<div class="content">'
                            + '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name btn-edit" employer-id="'+id+'">' + name + '</a>'
                            + '<p>- Ngày sinh: ' + ngaysinh + '</p>'
                            + '<p>- Phòng ban: ' + dept + '</p>'
                            + '<p>- Chức vụ: ' + res + '</p>'
                            + '</div>'
                            + '<div class="checkbox checkbox-success ">'
                            + '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'
                            + '<label for="checkbox-' + id + '"></label>'
                            + '</div>'
                            + '<a href="javascript:void(0)" class="link-edit btn-edit" data-toggle="modal" data-target="#detailEmp" employer-id="'+id+'"><i class="fa fa-pencil "></i></a>'
                            + '</div>'
                        empGroup.append(html);
                    }
                }
            });
            return false;
        });
    }
    self.addEmployer = function () {
        $( "#formCreateEmp" ).submit(function( event ) {
            event.preventDefault();
            if(!$(this).valid()){
                return;
            }
            var form = $(this);
            var employerGroup = $(".group-employee");
            var img = employerGroup.attr('img-default');
            var name = form.find("#name");
            if (name.val() === "") {
                name.next().text("Chưa nhập tên nhân viên");
                return;
            }
            var url = $(this).attr('action');

//            var data = $('form#formCreateEmp').serialize();

            var data = new FormData($("#formCreateEmp")[0]);
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                processData: false,
                contentType: false,
                success: function (getdata) {
                    //   $('#imagedisplay').html("<img src=" + data.url + "" + data.name + ">");

                    if (getdata.result) {
                        var id = ( ( getdata.data._id.$oid === null  ) ? '' : getdata.data._id.$oid);
                        var ngaysinh = ( ( getdata.data.ngaysinh === null  ) ? '' : getdata.data.ngaysinh);
                        var dept = ( ( getdata.data.dept === null  ) ? '' : getdata.data.dept);
                        var name = ( ( getdata.data.name === null  ) ? '' : getdata.data.name);
                        var res = ( ( getdata.data.res === null  ) ? '' : getdata.data.res);
                        var html = '<div class="employee-item" data-id="' + getdata.data._id.$oid + '">'
                            + '<div class="img" style="background-image: url(' + getdata.data.url_image + ');"></div>'
                            + '<div class="content">'
                            + '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name btn-edit" employer-id="'+id+'">' + name + '</a>'
                            + '<p>- Ngày sinh: ' + ngaysinh + '</p>'
                            + '<p>- Phòng ban: ' + dept + '</p>'
                            + '<p>- Chức vụ: ' + res + '</p>'
                            + '</div>'
                            + '<div class="checkbox checkbox-success ">'
                            + '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'
                            + '<label for="checkbox-' + id + '"></label>'
                            + '</div>'
                            + '<a href="javascript:void(0)" class="link-edit btn-edit" data-toggle="modal" data-target="#detailEmp" employer-id="'+id+'"><i class="fa fa-pencil "></i></a>'
                            + '</div>';
                        employerGroup.append(html);
                        $('#formCreateEmp').trigger("reset");
                        $('#createEmp').modal('hide');

                    }
                }
            });
            return false;
        });
    }
    self.addHistoryEmployer = function () {
        $(document).on('click', '#detailEmp .add-history', function () {
            var html = '<div class="group-company">'
                + '<p class="btn-box btn-delete">'
                + '<i class="fa fa-trash-o"></i>'
                + '<input class="btn-icon edit-test" type="submit" value="">'
                + '</p>'
                + '<input name="company_name_his[]" class="lbl-company edit-test" value="" placeholder="Tên công ty"/>'
                + '<input name="time_work[]" class="des-time edit-test" placeholder="Thời gian" value="" />'
                + '<label>- Nhiệm vụ</label>'
                + '<textarea name="des_his[]" class="textarea edit-test edit-test" placeholder="Mô tả công việc"></textarea>'
                + '</div>'
            $('#exper').append(html);

        });
    }
    self.removeHistoryEmployer = function () {
        $(document).on('click', '#detailEmp .btn-delete', function () {
            $(this).parent().remove();
        });
    }
    self.removeEmployer = function () {
        $(".btn-remove-employer").click(function () {
            var selected = [];
            var url = $(this).attr('ajax-url');
            $(".group-employee .employee-item").each(function () {
                var chkdelete = $(this).find(".chkdelete");
                if ($(chkdelete).is(":checked")) {
                    selected.push(chkdelete.val());
                }
            });
            if (selected.length == 0) {
                alert("chưa chọn nhân viên cần xóa");
                return;
            }
            var token = $("meta[name='csrf-token']").attr("content");
            var data = {
                "_token": token,
                "_id": selected
            };
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                success: function (data) {
                    console.log(data);
                    if (data.result) {
                        for (i = 0; i < selected.length; i++) {
                            $("[data-id=" + selected[i] + "]").css('display', 'none');
                        }
                    }
                }
            });
            return false;
        });
    }

// 	var editEmployer = this.editEmployer = function editEmployer(id) {
//         var imgtag = document.getElementById("preview");
//         $.ajax({
//             type: 'GET',
//             dataType: "json",
//             url: '/erp/employer/' + id,
//             async: false,
//             success: function (data) {
//                 console.log(data);
//                 if (data.success) {
//                     var formdetail = $("#form-detailEmp");
//
//                     if (!data.hasOwnProperty('work_history')) {
//                         $('#exper').find('.group-company').remove();
//                     }
//                     $.each(data, function (key, value) {
//                         if (value !== null) {
//                             if (key == "_id") {
//                                 formdetail.find("#" + key).val(id);
//                             }
//                             else if (key == "url_image") {
// //                                        imgtag.src = value;
//                                 formdetail.find('.upload-file-container').attr("style", 'background-image: url(' + value + ');');
//                             }
//                             else if (key == "work_history") {
//                                 $('#exper').find('.group-company').remove();
//                                 var countWorkHistory = value.length
//                                 for (i = 0; i < countWorkHistory; i++) {
//                                     var company = ((value[i][0].company_name !== 'undefined') ? value[i][0].company_name : '');
//                                     var time = ((value[i][0].time !== 'undefined') ? value[i][0].time : '');
//                                     var des = ((value[i][0].des !== 'undefined') ? value[i][0].des : '');
//                                     var html = '<div class="group-company">'
//                                         + '<p class="btn-box btn-delete" style="display:none">'
//                                         + '<i class="fa fa-trash-o"></i>'
//                                         + '<input class="btn-icon edit-test" type="submit" value="" />'
//                                         + '</p>'
//                                         + '<input name="company_name_his[]" placeholder="Tên công ty" class="lbl-company edit-test" value="' + company + '" disabled="disabled"/>'
//                                         + '<input name="time_work[]" class="des-time edit-test" placeholder="Thời gian" value="' + time + '" disabled="disabled"/>'
//                                         + '<label>- Nhiệm vụ</label>'
//                                         + '<textarea name="des_his[]" class="textarea edit-test edit-test" placeholder="Mô tả công việc" disabled="disabled">' + des + '</textarea>'
//                                         + '</div>'
//                                     $('#exper').append(html);
//                                 }
//                             }
//                             else {
//                                 formdetail.find("#" + key).val(value);
//
//                             }
//                         }
//                     });
//                 }
//             }
//         });
//         return false;
// 	}
//     self.updateEmployer = function () {
//
//         $( "#form-detailEmp" ).submit(function( event ) {
//             event.preventDefault();
//
//             if (!$(this).valid()) {
//                 return;
//             }
//             alert(1);
//             var form = $(this);
//
//             var url = form.attr('action');
//
//             var data = $('form#form-detailEmp').serialize();
//             $.ajax({
//                 type: 'POST',
//                 url: url,
//                 data: data,
//                 dataType: "json",
//                 success: function (getdata) {
//                     console.log(getdata);
//
//                     if (getdata.result) {
//
//                         var id = ( ( getdata.data._id.$oid === null  ) ? '' : getdata.data._id.$oid);
//                         var name = ( ( getdata.data.name === null  ) ? '' : getdata.data.name);
//                         var ngaysinh = ( ( getdata.data.ngaysinh === null  ) ? '' : getdata.data.ngaysinh);
//                         var dept = ( ( getdata.data.dept === null  ) ? '' : getdata.data.dept);
//                         var res = ( ( getdata.data.res === null  ) ? '' : getdata.data.res);
//                         var employerUpdate = $("[data-id=" + id + "]");
//                         employerUpdate.text('');
//                         var html =
//                             '<div class="img" style="background-image: url(' + getdata.data.url_image + ');"></div>'
//                             + '<div class="content">'
//                             + '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name" onclick="editEmployer(' + "'" + id + "'" + ')">' + name + '</a>'
//                             + '<p>- Ngày sinh: ' + ngaysinh + '</p>'
//                             + '<p>- Phòng ban: ' + dept + '</p>'
//                             + '<p>- Chức vụ: ' + res + '</p>'
//                             + '</div>'
//                             + '<div class="checkbox checkbox-success ">'
//                             + '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'
//                             + '<label for="checkbox-' + id + '"></label>'
//                             + '</div>'
//                             + '<a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#detailEmp" onclick="editEmployer(' + "'" + id + "'" + ')"><i class="fa fa-pencil "></i></a>'
//
//                         employerUpdate.append(html);
// //                        $('#formCreateEmp').trigger("reset");
//                         $('#detailEmp').modal('hide');
//                     }
//                 }
//             });
//             return false;
//         });
//     }
    self.eventCheckboxActive = function () {
        $('#checkbox-active input:checkbox').change(function () {
            $(this).closest('.checkbox').children("input").attr('checked', true);
            if ($(this).closest('.checkbox').children("input").is(":checked")) {
                $(".edit-account").attr("disabled", false);
                $("#form-detailEmp").validate({
                    ignore: ":hidden:not(select)",
                    rules: {
                        accountUser: {
                            required: true,
                            email: true
                        },
                        accountPass: {
                            required: true,
                            minlength: 8,
                        },
                    },
                    messages: {
                        accountUser: {
                            required: "Vui lòng nhập Email",
                            email: "Địa chỉ email không hợp lệ",
                        },
                        accountPass: {
                            required: "Vui lòng nhập mật khẩu",
                            minlength: "Số máy quý khách vừa nhập là số không có thực",
                        },
                    },
                    submitHandler: function (form) {
                    }
                });
            }
            else {
                $(".edit-account").attr("disabled", true);
            }
        });
    }
	return self;
}
$('.employee-item .btn-edit').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('employer-id');
    if(id !== undefined){
        editEmployer(id);
    }
})
function  editEmployer(id) {
    var imgtag = document.getElementById("preview");
    $.ajax({
        type: 'GET',
        dataType: "json",
        url: '/erp/employer/' + id,
        async: false,
        success: function (data) {
            // console.log(data);
            if (data.success) {
                var formdetail = $("#form-detailEmp");
                formdetail.find('.edit-test').val('');
                if (!data.hasOwnProperty('work_history')) {
                    $('#exper').find('.group-company').remove();
                }
                $.each(data, function (key, value) {
                    if (value !== null) {
                        if (key == "_id") {
                            formdetail.find("#" + key).val(id);
                        }
                        else if (key == "url_image") {
//                                        imgtag.src = value;
                            formdetail.find('.upload-file-container').attr("style", 'background-image: url(' + value + ');');
                        }
                        else if (key == "work_history") {
                            $('#exper').find('.group-company').remove();
                            var countWorkHistory = value.length
                            for (i = 0; i < countWorkHistory; i++) {
                                var company = ((value[i][0].company_name !== 'undefined') ? value[i][0].company_name : '');
                                var time = ((value[i][0].time !== 'undefined') ? value[i][0].time : '');
                                var des = ((value[i][0].des !== 'undefined') ? value[i][0].des : '');
                                var html = '<div class="group-company">'
                                    + '<p class="btn-box btn-delete" style="display:none">'
                                    + '<i class="fa fa-trash-o"></i>'
                                    + '<input class="btn-icon edit-test" type="submit" value="" />'
                                    + '</p>'
                                    + '<input name="company_name_his[]" placeholder="Tên công ty" class="lbl-company edit-test" value="' + company + '" disabled="disabled"/>'
                                    + '<input name="time_work[]" class="des-time edit-test" placeholder="Thời gian" value="' + time + '" disabled="disabled"/>'
                                    + '<label>- Nhiệm vụ</label>'
                                    + '<textarea name="des_his[]" class="textarea edit-test edit-test" placeholder="Mô tả công việc" disabled="disabled">' + des + '</textarea>'
                                    + '</div>'
                                $('#exper').append(html);
                            }
                        }
                        else {
                            formdetail.find("#" + key).val(value);

                        }
                    }
                });
            }
        }
    });
}
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}