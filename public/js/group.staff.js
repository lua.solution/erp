﻿//validate edit job{menu project}
$(function () {
    $('.onClickEditJob').unbind('click').bind('click', function () {
        var idTask = $(this).attr('task-id');
        var url = $(this).attr('url-ajax');
        var _token = $("meta[name='csrf-token']").attr("content");
        if (url === undefined) {
            return;
        }
        $.ajax({
            url: url,
            type: "POST",
            data: {
                "_token": _token,
                "idTask": idTask
            },
            beforeSend: function (xhr) {
                var token = $('meta[name="csrf_token"]').attr('content');
            },
            success: function (data) {
                //console.log(data);
                var formEdit = $('#form-editJob');
                var managerTask = data.data['task_staff'][0].id;
                formEdit.find('input[name=taskId]').val(idTask);
                formEdit.find('#nameEditJob').val(data.data.name);
                formEdit.find('#editStartDateJob').val(data.data.start);
                formEdit.find('#editEndDateJob').val(data.data.end);
                formEdit.find('#editEndDateJob').val(data.data.end);
                formEdit.find('textarea').val(data.data.desc);
                formEdit.find('#managerTask option[value=' + managerTask + ']').attr("selected", "selected");
                // then add
                $('#memberTask').find("option").each(function () {
                    var option = $(this);
                    var v = $(this).val();
                    var selected = false;
                    $.each(data.data['task_staff'], function (index, staff) {
                        if (Number(staff.action) !== 0 && staff.id === v) {
                            selected = true;
                            staff = undefined;
                            return;
                        }
                    });
                    if (selected) {
                        option.attr('selected', true);
                    }

                });
                // end then add
                $(".chosen-select").trigger("chosen:updated");
            },
            error: function (xhr, textStatus, errorThrown) {
                alert(textStatus);
            },
            timeout: 3000
        });
    });
});
//validate create job{menu project}

//validate edit group {menu project}
$(function () {
    $("#form-editGroup").validate({
        rules: {
            ediGroup: "required",
        },
        messages: {
            ediGroup: "Vui lòng nhập tên nhóm",
        }, submitHandler: function (form) {
            var formSubmit = $(form);
            var url = formSubmit.attr('url-ajax');
            var idGroupTask = formSubmit.find('input[name=idGroupTask]').val();
            var nameGroupTask = formSubmit.find('input[name=editGroup]').val();
            var _token = $("meta[name='csrf-token']").attr("content");
            if (url === undefined) {
                return;
            }
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    "_token": _token,
                    "idGroupTask": idGroupTask,
                    'nameGroupTask': nameGroupTask
                },
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                },
                success: function (data) {
                    console.log(data);
                    // return;
                    formSubmit[0].reset();
                    $('#eidtGroup').modal('hide');
                    editGroupName(idGroupTask, nameGroupTask);
                    showHideInfoJobs();
                    checkWidthGroupJob();
                    resizeGroupJob();
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert(textStatus);
                },
                timeout: 3000
            });
        }
    });

    $("#form-editJob").validate({
        rules: {
            nameEditJob: "required",
        },
        messages: {
            nameEditJob: "Vui lòng nhập tên công việc",
        }, submitHandler: function (form) {
            var formSubmit = $(form);
            var url = formSubmit.attr('url-ajax');
            var taskId = formSubmit.find('input[name=taskId]').val();
            var nameEditJob = formSubmit.find('input[name=nameEditJob]').val();
            var editStartDateJob = formSubmit.find('input[name=editStartDateJob]').val();
            var editEndDateJob = formSubmit.find('input[name=editEndDateJob]').val();
            var managerTask = formSubmit.find('select[name=managerTask]').val();
            var nameManagerTask = formSubmit.find('#managerTask_chosen .chosen-single span').html();
            var memberTask = formSubmit.find('select[name=memberTask]').val();
            var description = formSubmit.find('textarea').val();
            var _token = $("meta[name='csrf-token']").attr("content");
            var dataListJob = [];
            dataListJob = {
                "nameEditJob": nameEditJob,
                "editStartDateJob": editStartDateJob,
                "editEndDateJob": editEndDateJob,
                "managerTask": managerTask,
                "memberTask": memberTask,
                "description": description
            };
            if (url === undefined) {
                return;
            }
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    "_token": _token,
                    'taskId': taskId,
                    'dataListJob': dataListJob
                },
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                },
                success: function (data) {
                    console.log(data);
                    // return;
                    formSubmit[0].reset();
                    $('#editJob').modal('hide');

                    editTask(taskId, nameEditJob, editStartDateJob, editEndDateJob, nameManagerTask);

                    checkWidthGroupJob();
                    resizeGroupJob();
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert(textStatus);
                },
                timeout: 3000
            });
        }
    });
});
//validate create group {menu project}
$(function () {
    var groupJob = $('.group-job');

    var itemGroup = groupJob.find('.item-group-clone');
    var itemJob = itemGroup.find('.item-job');
    // itemGroup = itemGroup.find('.item-job').html('');
    // groupJob.find('.item-group-clone').remove();

    $("#form-createGroup").validate({
        rules: {
            groupTask: "required"
        },
        messages: {
            groupTask: "Vui lòng nhập tên nhóm"
        }, submitHandler: function (form) {
            var formSubmit = $(form);
            var url = formSubmit.attr('url-ajax');
            var idProject = formSubmit.find('input[name=idProject]').val();
            var valNameGroupTask = formSubmit.find('input[name=groupTask]').val();
            var _token = formSubmit.find("meta[name='csrf-token']").attr("content");

            if (url === undefined) {
                return;
            }
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'JSON',
                data: {
                    "_token": _token,
                    "idProject": idProject,
                    'nameGroupTask': valNameGroupTask
                },
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                },
                success: function (data) {
                    console.log(data);
                    var idGroupCreated = data.idGroupCreated;
                    formSubmit[0].reset();
                    $('#createGroup').modal('hide');

                    var row = itemGroup.clone();
                    row.removeClass('hide');

                    row.appendTo(groupJob);
                    row.removeClass('item-group-clone');
                    row.attr('id', 'item-group-' + idGroupCreated);
                    row.find('.title-group').html(valNameGroupTask);
                    row.find('.onClickDelete').attr('group-task-id', idGroupCreated);
                    // append

                    deleteGroup();
                    //resize Group Job
                    resizeGroupJobFirst();
                    showHideInfoJobs();
                    checkWidthGroupJob();
                    resizeGroupJob();
                    appendNow = true;

                },
                error: function (xhr, textStatus, errorThrown) {
                },
                timeout: 3000
            });
        }
    });
    $("#form-createJob").validate({
        rules: {
            nameJob: "required",
        },
        messages: {
            nameJob: "Vui lòng nhập tên công việc",
        }, submitHandler: function (form) {
            var formSubmit = $(form);
            var url = formSubmit.attr('url-ajax');
            var idGroupTask = formSubmit.find('input[name=idGroupTask]').val();
            var idProject = formSubmit.find('input[name=idProject]').val();
            var nameJob = formSubmit.find('input[name=nameJob]').val();
            var startDateJob = formSubmit.find('input[name=startDateJob]').val();
            var endDateJob = formSubmit.find('input[name=endDateJob]').val();
            var managerTask = formSubmit.find('select[name=managerTask]').val();
            var memberTask = formSubmit.find('select[name=memberTask]').val();
            var description = formSubmit.find('textarea[name=description]').val();
            var dataListJob = [];
            dataListJob = {
                "nameJob": nameJob,
                "startDateJob": startDateJob,
                "endDateJob": endDateJob,
                "managerTask": managerTask,
                "memberTask": memberTask,
                "description": description
            };
            var _token = $("meta[name='csrf-token']").attr("content");
            if (url === undefined) {
                return;
            }
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    "_token": _token,
                    "idGroupTask": idGroupTask,
                    'idProject': idProject,
                    'dataListJob': dataListJob
                },
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                },
                success: function (data) {
                    console.log(data);
                    var idTask = data.data.idTaskCreated;
                    var idGroupAndTask = 'group-' + idGroupTask + '-job-' + idTask;
                    var dataStaffOfTask = data.data.dataStaffOfTask;
                    formSubmit[0].reset();
                    $('#createJob').modal('hide');
                    var html = '';
                    $.each(dataStaffOfTask, function (event, el) {
                        html += '<a href="#"><img class="img-member" src="http://erp.dev/img_empl/erp01/' + el.id + '.jpg" onerror="javascript:imgError(this)"></a>';
                        // html += '<a href="#" class="img-member" style="background-image: url(images/'+el.id+'.jpg)"></a>';
                    });
                    var row = itemJob.clone();
                    row.removeClass('hide');
                    row.attr('id', idGroupAndTask);
                    row.find('.pro-content .name').html(nameJob);
                    row.find('.end').html(endDateJob);
                    row.find('.start').html(startDateJob);
                    row.find('.manager-name').html($("select[name=managerTask]").val());
                    row.find('.group-member').html(html);
                    row.find('.onClickDeleteJob').attr('task-id', idTask);
                    row.appendTo($('#item-group-' + idGroupTask + ' .box-job'));

                    deleteJob();
                    showHideInfoJobs();
                    checkWidthGroupJob();
                    resizeGroupJob();

                },
                error: function (xhr, textStatus, errorThrown) {
                    alert(textStatus);
                },
                timeout: 3000
            });


            //code
            //checkWidthGroupJob();
        }
    });
});

//validate create new employee {menu project}
$(function () {
    $("#form-addNewEmployee").validate({
        rules: {
            name: "required",
            email: {
                email: true
            },
            phone: {
                minlength: 10
            },
        },
        messages: {
            name: "Vui lòng nhập họ tên",
            email: {
                email: "Địa chỉ email không hợp lệ",
            },
            phone: {
                minlength: "Mật khẩu ít nhất 8 kí tự"
            }
        }, submitHandler: function (form) {
            //code
        }
    });
});

deleteGroup();
function deleteGroup() {
    $('.onClickDelete').unbind('click').bind('click', function (e) {
        e.preventDefault(e);
        if (confirm('Are you sure?')) {
            var idGroupTask = $(this).attr('group-task-id');
            var url = $(this).attr('url-ajax');
            var _token = $("meta[name='csrf-token']").attr("content");
            if (url === undefined) {
                return;
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    "_token": _token,
                    "idGroupTask": idGroupTask,
                },
                cache: false,
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                },
                success: function (data) {
                    console.log(data);
                    deleteGroupTask(idGroupTask);
                },
                error: function (xhr, textStatus, errorThrown) {
                },
                timeout: 3000,
            });
        } else {
            return;
        }
    });
}

deleteJob();
function deleteJob() {
    $('.onClickDeleteJob').unbind('click').bind('click', function (e) {
        e.preventDefault(e);
        if (confirm('Are you sure?')) {
            var idTask = $(this).attr('task-id');
            var url = $(this).attr('url-ajax');
            var _token = $("meta[name='csrf-token']").attr("content");
            if (url === undefined) {
                return;
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    "_token": _token,
                    "idTask": idTask,
                },
                cache: false,
                beforeSend: function (xhr) {
                    var token = $('meta[name="csrf_token"]').attr('content');
                },
                success: function (data) {
                    console.log(data);
                    deleteTask(idTask);
                },
                error: function (xhr, textStatus, errorThrown) {
                },
                timeout: 3000,
            });
        } else {
            return;
        }
    });
}