
$(document).ready(function () {
    //set height group job
    setHeightGroupMobile()
    //check show/hide information job
    showHideInfoJobs();
    //resize Group Job
    resizeGroupJob();

    //check Width Group Job
    checkWidthGroupJob();

    $('.tbl-web-search').DataTable({
        responsive: true
    });
    //menu left
    if ($(window).width() < 768) {
        $(".sidebar-collapse").addClass("menu-icon");
        menuSidebar();
    }
    if ($(document).width() <= 768) {
        //add page group job on mobile
        pagingGroupJob();
    }
    if ($(document).width() <= 420) {
        //add slide job on mobile
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "swiper.jquery.min.js";
        // Use any selector
        $("head").append(s);


        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            pagination: '.swiper-pagination',
            paginationClickable: true

            //// Navigation arrows
            //nextButton: '.swiper-button-next',
            //prevButton: '.swiper-button-prev',

            //// And if we need scrollbar
            //scrollbar: '.swiper-scrollbar',
        })
    }
    //change view info job{menu project}
    $('.tbl-work').DataTable({
        responsive: true
    });
});

$(window).resize(function () {
    //menu left
    if ($(window).width() < 768) {
        $(".sidebar-collapse").addClass("menu-icon");
        menuSidebar();
    }
    //resize Group Job
    resizeGroupJob();
});

//Drag and Drop

$(document).ready(function () {

    //<div id="div1" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
    //<img id="drag1" src="img_logo.gif" draggable="true"
    //ondragstart="drag(event)" width="336" height="69">
    var idElementActive;
    var idParentOld;
    var idParentCurrent;
    function allowDrop(ev) {
        ev.preventDefault();// cancel the ev event
    }

    function drag(ev) {
        // sets the data type and the value of the dragged data
        // This data will be returned by getData(). Here the ID of current dragged element
        ev.dataTransfer.setData("text", ev.target.id);
        idElementActive = ev.target.id;
        idParentOld = $('#' + idElementActive).closest(".item-group").attr('id');
        // sets another css class
        //ev.target.className = 'item-job dragging';
    }
    function drop(ev) {
        //ev.preventDefault();
        //var data = ev.dataTransfer.getData("text");
        //ev.target.appendChild(document.getElementById(data));
        ev.preventDefault();  // the dragover event needs to be canceled to allow firing the drop event

        // gets data set by setData() for current drag-and-drop operation (the ID of current dragged element),
        // using for parameter a string with the same format set in setData
        var drag_data = ev.dataTransfer.getData('Text');

        // adds /appends the dropped element in the drop-target
        ev.target.appendChild(document.getElementById(drag_data));

        idParentCurrent = $('#' + drag_data).closest(".item-group").attr('id');
        // sets another css class
        //document.getElementById(drag_data).className = 'item-job todrag';

    }

    //function checkDrapAndDrop(ele, old, current) {
    //    ele = (ev.target.id);
    //    alert(ele);
    //    old = ele.closest(".item-group").attr('id');
    //    alert(old);
    //}
    //checkDrapAndDrop(idElementActive, idParentOld, idParentCurrent);

});
//click menu left
$('#toogle-sidebar').click(function () {
    $(".sidebar-collapse").toggleClass("menu-icon");
    menuSidebar();
});


function menuSidebar() {
    if ($(".sidebar-collapse").hasClass("menu-icon")) {
        $('.project-right').addClass("animtion");
        $('.project-right').css({ 'margin-left': 36 });
        $('.project-right').removeClass("re-animtion");
    }
    else {
        $('.project-right').addClass("re-animtion");
        $('.project-right').css({ 'margin-left': 220 });
        $('.project-right').removeClass("animtion");
    }
}

/* add table child into staff */
$("#tbl-work tr.parents .number a").click(function () {
    //    $(this).closest("tr").nextUntil(".parents").toggleClass("open");
    $(this).closest("tr").next('tr').toggleClass("open");
    $(this).toggleClass("show");
});

//add paging GroupJob mobile {menu project}
function pagingGroupJob() {
    var num = $(".group-job").children('.item-group').length;
    var temp = '<ul class="paging-group-job">'
    for (var i = 0; i < num; i++) {
        temp += '<li><a href="#" ></a></li>'
    } temp += '</ul>'
    $(".paging-group-job").append(temp);

}

//check show/hide information job {menu project}
function showHideInfoJobs() {
    var count = $(".box-job").children(".item-job").length;
    if (count >= 4) {
        $(".group-job").find(".info-hide").hide();

        $('.group-job .item-group .item-job .pro-content').after().click(function () {
            $(this).children(".info-hide").slideToggle("slow", function () { });
            $(this).toggleClass("show");
        });
    }

    else {
        $(".info-hide").show();
    }
    //code

}

function resizeGroupJob() {
    var padMar;
    var titleGroup;
    if ($(document).width() <= 420) {
        padMar = 88;
        titleGroup = $(".header-work .pull-right").outerHeight();
        $(".group-job .item-group").css({ "min-width": $(window).width() - 56 });
        $(".group-job .item-group").css({ "min-height": 330 });
        $(".box-job").css({ "min-height": 300 });
    } else {
        if ($(document).width() < 768) {
            padMar = 88;
            titleGroup = $(".header-work .pull-right").outerHeight();
        } else {
            padMar = 88;
            titleGroup = $(".group-title").outerHeight();
        }
    }
    setHeightGroup(padMar, titleGroup);
}
//set height for item-group {menu project}
function setHeightGroup(padMar, titleGroup) {
    var page = $(document).height();
    var header = $(".header").outerHeight();
    var breadcrumb = $(".group-breadcrumb").outerHeight();
    var groupTitle = $(".group-title").outerHeight();
    var heightGroup = page - (header + breadcrumb + titleGroup + padMar);
    $(".group-job .item-group").css({ "height": heightGroup });

    var titleGroup = $(".title-group").outerHeight();
    var heightItemGroup = heightGroup - (titleGroup + 52);
}
//set height for item-group mobile {menu project}
function setHeightGroupMobile() {
    $(document).on('swipeleft', '[data-role="page"]', function (event) {
        if (event.handled !== true) // This will prevent event triggering more then once
        {
            var nextpage = $(this).next('[data-role="page"]');
            // swipe using id of next page if exists
            if (nextpage.length > 0) {
                $.mobile.changePage(nextpage, { transition: "slide", reverse: false }, true, true);
            }
            event.handled = true;
        }
        return false;
    });

    $(document).on('swiperight', '[data-role="page"]', function (event) {
        if (event.handled !== true) // This will prevent event triggering more then once
        {
            var prevpage = $(this).prev('[data-role="page"]');
            if (prevpage.length > 0) {
                $.mobile.changePage(prevpage, { transition: "slide", reverse: true }, true, true);
            }
            event.handled = true;
        }
        return false;
    });
}
//check Width Group Job {menu project}
function checkWidthGroupJob() {
    var widthG = $(".group-job").width();
    var count = $(".group-job").children(".item-group").length;
    if (count > 3) {
        $(".item-job").css({ "width": "100%" });
        setHeightGroup();
    }
    else {
        if (count > 2) {
            $(".item-job").css({ "width": "33.33%" });
        }
        else {
            if (count > 1) {
                $(".item-job").css({ "width": " calc(50% - 6px)", "width": " -o-calc(50% - 6px)", "width": " -ms-calc(50% - 6px)", "width": "-moz-calc(50% - 12px)", "width": "-webkit-calc(50% - 6px)" });

            }
            else {
                $(".item-job").css({ "width": 280 });
            }
        }

    }
}



$(".title-accordion").click(function () {
    $(this).parents().children(".content-accordion").toggle('hide');
    if ($(this).children("span").hasClass("fa-minus")) {
        $(this).children("span").removeClass("fa-minus").addClass("fa-plus");
    }
    else {
        $(this).children("span").removeClass("fa-plus").addClass("fa-minus");
    }
});




















// //resize Group Job
function resizeGroupJobFirst() {
    var count = $(".group-job").children(".item-group").length;
    if (count <= 1) {
        resizeGroupJob();
    }
}

//var a='abc';
//var b="asdfdsssdff"
//editGroupName(a, b);
// edit name group
function editGroupName(idGroup, nameGroup) {
    $(".group-job").find('li a[group-task-id = "' + idGroup + '"]').closest(".item-group").children(".title-group").text(nameGroup);
}
function deleteGroupTask(idGroup) {
    $(".group-job").find('li a[group-task-id = "' + idGroup + '"]').closest(".item-group").remove(".item-group");
}


$(function () {

    function table(process) {
        var wid = process + "%";
        $(".rTable #table-master .stt").css({ "width": "4%" });
        $(".rTable #table-master .name").css({ "width": "13%" });
        $(".rTable #table-master .unit").css({ "width": "6%" });
        $(".rTable #table-master .weight").css({ "width": "6%" });
        $(".rTable #table-master .price").css({ "width": "6%" });
        $(".rTable #table-master .money").css({ "width": "8%" });
        $(".rTable #table-master .time").css({ "width": "15%" });
        $(".rTable #table-master .manager").css({ "width": "11%" });
        $(".rTable #table-master .process").css({ "width": wid });
        $(".rTable #table-master .action").css({ "width": "12%" });
    }
    table(19);
    var Row = $(".rTableRow");
    function setWidth(Row) {
        var html = $("#table-master");

        var holdData = [];
        html.find('.rTableHead').each(function () {
            holdData.push($(this).outerWidth());
            // console.log(this);
        });
        $(".rTableRow").each(function (j, v) { // tr
            //data[j] = Array();
            $(this).children('.rTableCell').each(function (index, value) { // td
                var wid = holdData[index] + "%";
                $(this).css({ width: wid });
            });
        })
    }
    setWidth(Row);
    $(".rTable .parents .number a").click(function () {
        $(this).closest(".parents").children('.childs').toggleClass("open");
        $(this).toggleClass("show");
    });
    $("#table-master .time a").click(function () {
        $(".rTable").find(".money").toggleClass("hide");
        $(".rTable").find(".price").toggleClass("hide");
        if ($("#table-master .price ").hasClass("hide")) {
            $("#table-master .time a i").addClass("fa-angle-right");
            $("#table-master .time a i").removeClass("fa-angle-left");
            table(32); setWidth(Row);
        }
        else {
            $("#table-master .time a i").addClass("fa-angle-left");
            $("#table-master .time a i").removeClass("fa-angle-right");
            table(19); setWidth(Row);
        }
    });
    $(".btn.btn-success")
});