@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                  @if ( Session::has('user'))
                     <div class="panel-body">
                    CSDL đã chọn: <h2> <b>{!! Session::get('dbname')!!} </b></h2>
                    @if(Session::get('dbname') == "N/A")
                        Cannot access ERP System without database !
                        @endif
                </div>
                    @else
            </div>
              <a href="/login" class="btn btn-info"> You need to login to see</a>
            @endif
            </div>
        </div>
    </div>
</div>
@endsection
