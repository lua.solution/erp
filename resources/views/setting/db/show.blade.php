@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('admin.sidebar')

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">Post {{ $post['_id'] }}</div>
                <div class="panel-body">

                    <a href="{{ url('/profile/posts') }}" title="Back">
                        <button class="btn btn-warning btn-xs">
                            <i class="fa fa-arrow-left" aria-hidden="true">
                            </i> Back</button>
                        </a>
                        <a href="{{ url('/profile/posts/' . $post['_id'] . '/edit') }}" title="Edit Post">
                            <button class="btn btn-primary btn-xs">
                                <i class="fa fa-pencil-square-o" aria-hidden="true">    
                                </i> Select
                            </button>
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['profile/posts', $post['_id']],
                            'style' => 'display:inline'
                            ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Post',
                            'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                            {!! Form::close() !!}
                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <th>ID</th>
                                            <td>{{ $post['_id'] }}</td>
                                        </tr>
                                        <tr>
                                            <th> Title </th>
                                            <td> {{ $post['title'] }} </td>
                                        </tr>
                                        <tr>
                                            <th> Content </th>
                                            <td> {{ $post['content'] }} </td>
                                        </tr>
                                        <tr>
                                            <th> Category </th>
                                            <td> {{$post['category'] }} </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection