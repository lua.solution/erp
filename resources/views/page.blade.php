<html>
<head>
  <title>Ajax Example</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <script src="{{ asset ('jquery/jquery.min.js') }}"></script>
  <script>
   $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
 </script>
 
 <script type="text/javascript">
  $(document).ready(function()
  { 
    var submit   = $("button[type='submit']");
    
    submit.click(function()
    {
      var username = $("input[name='username']").val();

      if(username == ''){
        alert('Vui lòng nhập tài khoản');
        return false;
      }
      var data = $('form#form_login').serialize();
      $.ajax({
        type : 'POST',
        url  : 'ajax',
        data : data,
        success :  function(data)
        {            
          if(data == 'false')
          {
            alert('Sai tên hoặc mật khẩu');
          }else{
            $('#content').append(data);
            document.forms['form_login'].reset()
          }
        }
      });
      return false;
    });
  });
</script>
</head>
<body>
<table>
  <div id="content"> 
  <tr>
    <th>Company</th>
    <th>Contact</th>
  </tr>
  </div>
  </table>
  <form method="post" id="form_login">
    <table>
      <tr>
        <td>
          <input type="text" name="username" placeholder="Tài khoản" />
        </td>
      </tr>
      <tr>
        <td align="center">
         <button  type="submit" >Login</button>
       </td>
     </tr>
   </table>
   </form>
</body>
</html>    