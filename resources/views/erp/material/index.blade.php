@extends('erp.layouts.app')

@section('content')
    <div class="content-page document">
        <div class="form-search">
            <div class="pull-left">
                <h3 class="title-page">Danh sách vật tư</h3>
            </div>
            <div class="pull-right">
                <p class="btn-box btn-green">
                    <i class="fa fa-plus"></i>
                    <a href="#" data-toggle="modal" data-target="#createMaterial" class="createMaterial">Thêm</a>
                </p>
                <p class="btn-box btn-green">
                    <i class="fa fa-trash-o"></i>
                    <input class="btn-icon" type="button" value="Xóa" />
                </p>
            </div>
        </div>

        <table class="table table-striped nowrap tbl-web-search">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên vật tư</th>
                <th>Ngày nhập</th>
                <th>Đơn giá</th>
                <th>ĐVT</th>
                <th>Số lượng</th>
                <th>Còn lại</th>
                <th>Ghi chú</th>
                <th>Xử lý</th>
            </tr>

            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Tên vật tư</td>
                <td>Ngày nhập</td>
                <td>Đơn giá</td>
                <td>ĐVT</td>
                <td>Số lượng</td>
                <td>Còn lại</td>
                <td>Ghi chú</td>
                <td>
                    <p class="btn-box btn-trans">
                        <i class="fa fa-pencil"></i>
                        <a href="#" data-toggle="modal" data-target="#editMaterial"></a>
                    </p>
                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>Tên vật tư</td>
                <td>Ngày nhập</td>
                <td>Đơn giá</td>
                <td>ĐVT</td>
                <td>Số lượng</td>
                <td>Còn lại</td>
                <td>Ghi chú</td>
                <td>
                    <p class="btn-box btn-trans">
                        <i class="fa fa-pencil"></i>
                        <a href="#" data-toggle="modal" data-target="#editMaterial"></a>
                    </p>
                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>Tên vật tư</td>
                <td>Ngày nhập</td>
                <td>Đơn giá</td>
                <td>ĐVT</td>
                <td>Số lượng</td>
                <td>Còn lại</td>
                <td>Ghi chú</td>
                <td>
                    <p class="btn-box btn-trans">
                        <i class="fa fa-pencil"></i>
                        <a href="#" data-toggle="modal" data-target="#editMaterial"></a>
                    </p>
                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
            <tr>
                <td>1</td>
                <td>Tên vật tư</td>
                <td>Ngày nhập</td>
                <td>Đơn giá</td>
                <td>ĐVT</td>
                <td>Số lượng</td>
                <td>Còn lại</td>
                <td>Ghi chú</td>
                <td>
                    <p class="btn-box btn-trans">
                        <i class="fa fa-pencil"></i>
                        <a href="#" data-toggle="modal" data-target="#editMaterial"></a>
                    </p>
                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="editMaterial" tabindex="-1" role="dialog" aria-labelledby="myEditMaterial">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-editMaterial">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myEditMaterial">Sửa vật tư</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên vật tư:</label>
                                <input type="text" class="form-control" name="editNameMar" id="editNameMar" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Ngày nhập:</label>
                                <input data-provide="datepicker" class="form-control " name="" id=""  placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Đơn giá:</label>
                                <input type="text" class="form-control" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>ĐVT:</label>
                                <input type="text" class="form-control " name="" id="" placeholder="Nhập..."/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số lượng</label>
                                <input type="text" class="form-control" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Còn lại</label>
                                <input type="text" class="form-control" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control" rows="3" placeholder="Nhập..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="createMaterial" tabindex="-1" role="dialog" aria-labelledby="myCreateMaterial">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-createMaterial">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateMaterial">Thêm vật tư</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên vật tư:</label>
                                <input type="text" class="form-control" name="nameMaterial" id="nameMaterial" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Ngày nhập:</label>
                                <input data-provide="datepicker" class="form-control " name="" id=""  placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Đơn giá:</label>
                                <input type="text" class="form-control" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>ĐVT:</label>
                                <input type="text" class="form-control " name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số lượng</label>
                                <input type="text" class="form-control" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Còn lại</label>
                                <input type="text" class="form-control" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control" rows="3" placeholder="Nhập..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('js/staff.js') }}"></script>
@endsection