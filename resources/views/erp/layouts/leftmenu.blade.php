<?php
if(Session::has('idProject')) {
	$idProject = Session::get('idProject');
}
?>
<div class="sidebar-collapse">
        <ul class="nav nav-tabs menu-sidebar" role="tablist">
            <li>
                <a href="#" id="toogle-sidebar"><span>Show/Hide</span><i class="fa fa-angle-left" aria-hidden="true"></i></a>
            </li>

            <li role="presentation" >
               
                <a href="{{ url('erp/project/info/'. $idProject) }}" >
                    <i class="fa fa-desktop "></i>
                    <span>Thông tin</span>
                </a>
            </li>
            <li role="presentation">
                <a href="{{ url('erp/project/work/'. $idProject) }}" >
                    <i class="fa fa-table "></i>
                    <span>Công việc</span>
                </a>
            </li>
            <li role="presentation" >
                <a href="{{ url('erp/project/staff/'. $idProject) }}" >
                    <i class="fa fa-users "></i>
                    <span>Nhân sự</span>
                </a>
            </li>
            <li role="presentation" >
                <a href="#devices" aria-controls="devices" role="tab" data-toggle="tab">
                    <i class="fa fa-qrcode "></i>
                    <span>Thiết bị</span>
                </a>
            </li>
            <li role="presentation" >
                <a href="#material" aria-controls="material" role="tab" data-toggle="tab">
                    <i class="fa fa-deviantart"></i>
                    <span>Vật tư</span>
                </a>
            </li>
            <li role="presentation" >
                <a href="#report" aria-controls="report" role="tab" data-toggle="tab">
                    <i class="fa fa-bar-chart-o "></i>
                    <span>Báo cáo</span>
                </a>
            </li>
            <li role="presentation" >
                <a href="#cost" aria-controls="cost" role="tab" data-toggle="tab">
                    <i class="fa fa-money "></i>
                    <span>Chi phí</span>
                </a>
            </li>
        </ul>
    </div>