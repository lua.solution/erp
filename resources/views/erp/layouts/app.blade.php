﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv='cache-control' content='no-cache'>
    <meta http-equiv='expires' content='0'>
    <meta http-equiv='pragma' content='no-cache'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="icon" type="image/x-icon" href="{{ asset('images/logo_web.png') }}" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/build.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/chosen.css') }}" rel="stylesheet" />

    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />

    <!--[if lt IE 9 ]>
        <script type="text/javascript">
            var html5Elem = ['header', 'nav', 'menu','video', 'section', 'article', 'aside', 'main', 'footer'];
            for (var i = 0; i < html5Elem.length; i++){
                document.createElement(html5Elem[i]);
            }
        </script>
    <![endif]-->


    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
</head>
<body>
    <header class="header">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Đây là menu mobile</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://polarisvietnam.com" target="_blank">
                    <img src="{{ asset('images/logo_polaris_white.png') }}" width="115" title="login Polaris" alt="login Polaris" />
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
                <ul class="nav navbar-nav">
                    <li class=" dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dự án<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <!--<li><a href="#">Tạo dự án</a></li>-->
                            <li><a href="{{ route('erp.project') }}">Danh sách dự án</a></li>
                            <li><a href="{{ route('erp.employer.index') }}">Báo cáo thống kê</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Nhân sự<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('erp.employer.index') }}">Danh sách nhân viên</a></li>
                            <li><a href="{{ route('erp.department.index') }}">Phòng ban</a></li>
                            <li><a href="{{ url('erp/eml/manage') }}">Quản lý nhân sự</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('erp.machine.index') }}">Thiết bị</a></li>
                    <li><a href="{{ route('erp.material.index') }}">Vật tư</a></li>
                    <li><a href="{{ route('erp.document.index') }}">Công văn</a></li>
                    <li><a href="#">Báo cáo</a></li>
                </ul>
                
            </div>

            <!--/.nav-collapse -->
        </nav>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown notifications-menu ">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-bell-o"></i>
                    <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Bạn có thông báo</li>
                    <li>
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;">
                            <ul class="menu" style="overflow: hidden; width: 100%; height: 200px;">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-aqua"></i>5 new members joined today
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-warning text-yellow"></i>Very long description here that may not fit into the
                                        page and may cause design problems
                                    </a>
                                </li>
                            </ul>
                            <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 195.122px;"></div>
                            <div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                        </div>
                    </li>
                    <li class="footer"><a href="#">View all</a></li>
                </ul>
            </li>
            <li class="">
                <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <p class="img-emp" style="background-image: url({{ asset('images/ic_profile.jpg') }}")></p>
                    {!! Session::get('user')['username'] !!}  <span class=" fa fa-angle-down"></span>
                </a>

                <div class="dropdown-menu dropdown-usermenu pull-right">
                    <div class="user-detail">
                        <div class="img-user">
                            <a href="#" class="img-1b" title="anh nhan vien">anh nhan vien</a>
                        </div>

                        <div class="content-user">
                            
                            <a href="#" class="name" data-toggle="modal" data-target="#detailEmp">
                               {!! Session::get('user')['username'] !!} <span class="caret"></span>
                           </a>
                           <p class="level"> </p>
                       </div>

                       <a href="#" class="link-login changepass" data-toggle="modal" data-target="#changePass"><span class="fa fa-pencil"></span>Đổi mật khẩu</a>
                       
                       <a class="link-login" href="{{ route('logout') }}">
                       <span class="fa fa-trash-o"></span>Đăng xuất
                   </a>
               </div>
           </div>

       </li>
   </ul>
</header>
<!--Change Password-->
    <div class="modal fade" id="changePass" tabindex="-1" role="dialog" aria-labelledby="myChangePass">
        <div class="modal-dialog" role="document">
            <form class="modal-content" action="{{ route('changePass') }}" method="post" role="form" id="form-changePass">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myChangePass">Đổi mật khẩu</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div id="msg"></div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Mật khẩu cũ:<span class="note">(*)</span></label>
                                <input type="password" class="form-control" name="pass" id="pass" placeholder="" />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Mật khẩu mới:<span class="note">(*)</span></label>
                                <input type="password" class="form-control" name="newpass" id="newpass" placeholder="" />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Nhập lại mật khẩu mới:<span class="note">(*)</span></label>
                                <input type="password" class="form-control" name="repass" id="repass" placeholder="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>

@yield('content')

<div id="mask"></div>
<a href="#" class="scrollup"><span class="fa fa-arrow-circle-up"></span></a>


    <script type="text/javascript">
        //scroll page
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        });
        $('.scrollup').click(function () {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
        //datepicker
        $(function () {
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',
            });
        });
        //chosen
        $(function () {
            $('.chosen-select').chosen();
            $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
            $('.chosen-select').on('change', function (evt, params) {
                $(this).closest('.form-group').children('.error').css('display', 'none');
            });
        });
        var f = $('form#form-changePass');
        attackSubmit('form#form-changePass');

        function attackSubmit( form){
            
            var f = $(form);
            if( f === undefined ){
                return;
            }
            f.submit(function(e){
                e.preventDefault();
                var url = $(this).attr('action');
                if( url === undefined){
                    url = window.location.href;
                }
                var data = $(this).serialize();
                $.ajax({
                    type : 'POST',
                    url  : url,
                    data : data,
                    success :  function(dataGet)
                    {
                        $('#msg').html(dataGet);
                        $('#pass').val('');
                        $('#newpass').val('');
                        $('#repass').val('');
                    }
                });
            });
        }
    </script>


</body>

<!--  JS -->

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/chosen.jquery.js') }}"></script>
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>

{{--<script src="{{ asset('js/jquery.validate.min.js') }}"></script>--}}
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script src="{{ asset('js/web.js') }}"></script>

</html>
