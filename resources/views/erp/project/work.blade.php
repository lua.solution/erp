@extends('erp.layouts.app')

@section('content')
    <?php
    if(Session::has('idProject')) {
        $idProject = Session::get('idProject');
    }
    ?>
<div class="group-breadcrumb">
    <ol class="breadcrumb pull-left">
        <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
        <li id="menueml">Dự án</li>
        <li class="active">Thông tin công việc</li>
    </ol>
</div>
    <div class="content-page-project">
        @include('erp.layouts.leftmenu')
        <div class="tab-content project-right " >
            <div role="tabpanel" class="tab-pane active" id="work">
                <div class="header-work">
                    <div class="group-title">
                        <h3 class="title-page">Thông tin công việc</h3>
                        <h5 class="des-page"></h5>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <p class="btn-box btn-green">
                                <i class="fa fa-plus"></i>
                                <a href="#" data-toggle="modal" data-target="#createGroup">Thêm nhóm</a>
                            </p>
                            <p class="btn-box btn-green upload-file">
                                <i class="fa fa-upload"></i>
                                <input class="btn-icon choose-file" type="file" value="Upload"/>
                            </p>
                            <p class="btn-box btn-green">
                                <i class="fa fa-download"></i>
                                <input class="btn-icon" type="button" value="Download"
                                       onclick="location.href = '';"/>
                            </p>
                        </div>
                        <p class="input-search">
                            <i class="fa fa-search"></i>
                            <input type="text" class="form-control" placeholder="Nhập thông tin tìm kiếm..." />
                            <input class="btn-icon" type="submit" value="" />
                        </p>
                        <ul class="nav nav-tabs tab-work">
                            <li ><a href="{{ url('erp/project/work/'. $idProject) }}" id="link-view1"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
                            <li ><a href="{{ url('erp/project/worklist/'. $idProject) }}" id="link-view2"><i class="fa fa-th-list" aria-hidden="true"></i></a></li>
                            <li ><a href="{{ url('erp/project/workstaff/'. $idProject) }}" id="link-view3"><i class="fa fa-user"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content">
                    <div id="view1" class="tab-pane fade in active">
                        <div class="group-job" id="my-slide">
                            <div class="item-group item-group-clone hide" id="" ondrop="drop(event)" ondragover="allowDrop(event)">
                                <h3 class="title-group"></h3>
                                <div class="dropdown">
                                    <button id="" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#" class="create-task-to-group" project-id="" group-task-id="" data-toggle="modal" data-target="#createJob"><i class="fa fa-plus "></i><span>Thêm công việc</span></a></li>
                                        <li><a href="#" class="group-task" group-task-name="" group-task-id="" data-toggle="modal" data-target="#eidtGroup"><i class="fa fa-pencil "></i><span>Sửa</span></a></li>
                                        <li><a href="#" class="onClickDelete" group-task-id="" url-ajax="{{ URL::route('ajax.erp.project.work.postDeleteGroupTask') }}"><i class="fa fa-trash-o "></i><span>Xóa</span></a></li>
                                    </ul>
                                </div>
                                <div class="box-job">
                                    <div class="item-job clone-job hide" draggable="true" ondragstart="drag(event)">
                                        <div class="pro-content">
                                            <a href="javascript:void(0)" class="name"></a>
                                            <p>- Thời hạn: <b class="end"></b> </p>
                                            <div class="info-hide">
                                                <p>- Phụ trách chính: <b class="manager-name"></b></p>
                                                <div class="group-member">
                                                    <a href="#" class="img-member" style="background-image: url(images/img.jpg)"></a>
                                                    <a href="#" class="img-member" style="background-image: url(images/img2.jpg)"></a>
                                                    <a href="#" class="img-member" style="background-image: url(images/img.jpg)"></a>
                                                    <a href="#" class="img-member" style="background-image: url(images/img2.jpg)" title="" data-placement="bottom" data-toggle="tooltip" data-original-title=""></a>
                                                </div>
                                                <p>- Tiến trình công việc:</p>
                                                <div class="progress-group">
                                                    <span class="progress-text">
                                                        <span class="start"></span> <span class="end"></span>
                                                    </span>
                                                    <span class="progress-number">100%</span>

                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-done" style="width: 100%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="job-status done"></div>
                                        </div>
                                        <div class="dropdown">
                                            <button id="" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="#" class="onClickEditJob" task-id="" data-toggle="modal" data-target="#editJob"><i class="fa fa-pencil "></i><span>Sửa</span></a></li>
                                                <li><a href="#" class="onClickDeleteJob" url-ajax="{{ URL::route('ajax.erp.project.work.postDeleteTaskProject') }}"><i class="fa fa-trash-o "></i><span>Xóa</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(isset($dataGroupTask))
                            @foreach($dataGroupTask as $key => $groupTask)
                            <div class="item-group" id="item-group-{{ $groupTask['_id'] }}" ondrop="drop(event)" ondragover="allowDrop(event)">
                                <h3 class="title-group">{{ $groupTask['name'] }}</h3>
                                <div class="dropdown">
                                    <button id="" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="#" class="create-task-to-group" project-id="{{ $idProject }}" group-task-id="{{ $groupTask['_id'] }}" data-toggle="modal" data-target="#createJob"><i class="fa fa-plus "></i><span>Thêm công việc</span></a></li>
                                        <li><a href="#" class="group-task" group-task-name="{{ $groupTask['name'] }}" group-task-id="{{ $groupTask['_id'] }}" data-toggle="modal" data-target="#eidtGroup"><i class="fa fa-pencil "></i><span>Sửa</span></a></li>
                                        <li><a href="#" class="onClickDelete" group-task-id="{{ $groupTask['_id'] }}" url-ajax="{{ URL::route('ajax.erp.project.work.postDeleteGroupTask') }}"><i class="fa fa-trash-o "></i><span>Xóa</span></a></li>
                                    </ul>
                                </div>
                                <div class="box-job">
                                    @if(isset($dataTaskGetByGroupId))
                                    @foreach($dataTaskGetByGroupId[$key] as $key => $task)
                                    <div class="item-job" draggable="true" ondragstart="drag(event)" id="group-{{ $groupTask['_id'] }}-job-{{ $task['_id'] }}">
                                        <div class="pro-content">
                                            <a href="javascript:void(0)" class="name">{{ $task['name'] }}</a>
                                            <p>- Thời hạn: <b>{{ $task['end'] }}</b> </p>
                                            <div class="info-hide">
                                                <p>- Phụ trách chính: <b>{{ $leaderTask[0]['leader'] or " " }}</b></p>
                                                <div class="group-member">
	                                                <?php $count = 0; ?>
                                                    @if(isset($task['task_staff']))
                                                        @foreach($task['task_staff'] as $staff)
                                                            @if($count == 4)
                                                                @break;
                                                            @endif
			                                                <?php
			                                                $imgurl = 'img_empl/' . Session::get('dbname') . '/' . $staff['id'] . '.jpg';
			                                                $count++;
			                                                ?>
                                                            <a href="#"><img class="img-member" src="{{ asset($imgurl) }}" onerror="javascript:imgError(this)" /></a>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <p>- Tiến trình công việc:</p>
                                                <div class="progress-group">
                                                    <span class="progress-text">
                                                        <span class="start">{{ $task['start'] }}</span> <span class="end">{{ $task['end'] }}</span>
                                                    </span>
                                                    <span class="progress-number">100%</span>

                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-done" style="width: 100%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="job-status done"></div>
                                        </div>
                                        <div class="dropdown">
                                            <button id="" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                <li><a href="javascript:void(0)" class="onClickEditJob" task-id="{{ $task['_id'] }}" url-ajax="{{ URL::route('ajax.erp.project.work.postEditTask') }}" data-toggle="modal" data-target="#editJob"><i class="fa fa-pencil "></i><span>Sửa</span></a></li>
                                                <li><a href="javascript:void(0)" class="onClickDeleteJob" task-id="{{ $task['_id'] }}" url-ajax="{{ URL::route('ajax.erp.project.work.postDeleteTaskProject') }}"><i class="fa fa-trash-o "></i><span>Xóa</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div id="view2" class="tab-pane fade">
                        <table id="tbl-work" class="table table-striped nowrap tbl-work" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th rowspan="2"></th>
                                <th rowspan="2">Tên công việc</th>
                                <th colspan="2">Thời gian</th>
                                <th rowspan="2">ĐVT</th>
                                <th rowspan="2">Khối lượng</th>
                                <th rowspan="2">Phụ trách</th>
                                <th rowspan="2">Tiến trình</th>
                                <th rowspan="2"></th>
                            </tr>
                            <tr>
                                <th>Bắt đầu</th>
                                <th>Kết thúc</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="add-data">
                                <td>
                                    <span class="number">1</span>
                                </td>
                                <td>
                                    <input type="text" class="" value="Công việc 1" />
                                </td>
                                <td>
                                    <input data-provide="datepicker" name="start" id="start" class="" placeholder="02/24/2017" />
                                </td>
                                <td>
                                    <input data-provide="datepicker" name="end" id="end" class="" placeholder="03/24/2017" />
                                </td>
                                <td>
                                    <input type="text" class="" value="ABC" />
                                </td>
                                <td>
                                    <input type="text" class="" value="ABC" />
                                </td>
                                <td>
                                    <select data-placeholder="--chọn--" class="" multiple name="manageEdit" id="manageEdit" style="display: none;">
                                        <option value=""></option>
                                        <option value="United States">United States</option>
                                    </select>
                                    <a href="#" class="img-member" style="background-image: url(images/img.jpg)"></a><a href="#" class="img-member" style="background-image: url(images/img.jpg)"></a>
                                </td>
                                <td class="process">
                                    <div class="progress-group">
                                            <span class="progress-text">
                                                <span class="start">02/24/2017</span> <span class="end">03/24/2017</span>
                                            </span>
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button type="submit" class="add-emp"><i class="fa fa-plus-circle"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="number">2</span>
                                </td>
                                <td>
                                    <input type="text" class="" value="Công việc 1" />
                                </td>
                                <td>
                                    <input data-provide="datepicker" name="start" id="start" class="" placeholder="02/24/2017" />
                                </td>
                                <td>
                                    <input data-provide="datepicker" name="end" id="end" class="" placeholder="03/24/2017" />
                                </td>
                                <td>
                                    <input type="text" class="" value="ABC" />
                                </td>
                                <td>
                                    <input type="text" class="" value="ABC" />
                                </td>
                                <td>
                                    <select data-placeholder="--chọn--" class="" multiple name="manageEdit" id="manageEdit" style="display: none;">
                                        <option value=""></option>
                                        <option value="United States">United States</option>
                                    </select>
                                    <a href="#" class="img-member" style="background-image: url(images/img.jpg)"></a><a href="#" class="img-member" style="background-image: url(images/img.jpg)"></a>
                                </td>
                                <td class="process">
                                    <div class="progress-group">
                                            <span class="progress-text">
                                                <span class="start">02/24/2017</span> <span class="end">03/24/2017</span>
                                            </span>
                                        <span class="progress-number">80%</span>

                                        <div class="progress sm">
                                            <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button type="button" class="delete-emp"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="edit-emp"><i class="fa fa-pencil"></i></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="view3" class="tab-pane fade">
                        <ul class="accordion">
                            <li class="item-accordion">
                                <a class="title-accordion">Nguyễn Văn A<span class="fa fa-minus"></span></a>
                                <div class="content-accordion">
                                    <table class="table table-striped nowrap tbl-work" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th rowspan="2"></th>
                                            <th rowspan="2">Tên công việc</th>
                                            <th colspan="2">Thời gian</th>
                                            <th rowspan="2">ĐVT</th>
                                            <th rowspan="2">Khối lượng</th>
                                            <th rowspan="2">Tiến trình</th>
                                            <th rowspan="2"></th>
                                        </tr>
                                        <tr>
                                            <th>Bắt đầu</th>
                                            <th>Kết thúc</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="add-data">
                                            <td>
                                                <input type="checkbox" class="cbx-select" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="Công việc 1" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="start" id="start" class="" placeholder="02/24/2017" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="end" id="end" class="" placeholder="03/24/2017" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td class="process">
                                                <div class="progress-group">
                                                        <span class="progress-text">
                                                            <span class="start">02/24/2017</span> <span class="end">03/24/2017</span>
                                                        </span>
                                                    <span class="progress-number">80%</span>

                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="submit" class="add-emp"><i class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="cbx-select" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="Công việc 1" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="start" id="start" class="" placeholder="02/24/2017" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="end" id="end" class="" placeholder="03/24/2017" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td class="process">
                                                <div class="progress-group">
                                                        <span class="progress-text">
                                                            <span class="start">02/24/2017</span> <span class="end">03/24/2017</span>
                                                        </span>
                                                    <span class="progress-number">80%</span>

                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" class="delete-emp"><i class="fa fa-trash-o"></i></button>
                                                <button type="button" class="edit-emp"><i class="fa fa-pencil"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </li>
                            <li class="item-accordion">
                                <a class="title-accordion">Nguyễn Văn B<span class="fa fa-minus"></span></a>
                                <div class="content-accordion">
                                    <table class="table table-striped nowrap tbl-work" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th rowspan="2"></th>
                                            <th rowspan="2">Tên công việc</th>
                                            <th colspan="2">Thời gian</th>
                                            <th rowspan="2">ĐVT</th>
                                            <th rowspan="2">Khối lượng</th>
                                            <th rowspan="2">Tiến trình</th>
                                            <th rowspan="2"></th>
                                        </tr>
                                        <tr>
                                            <th>Bắt đầu</th>
                                            <th>Kết thúc</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="add-data">
                                            <td>
                                                <input type="checkbox" class="cbx-select" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="Công việc 1" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="start" id="start" class="" placeholder="02/24/2017" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="end" id="end" class="" placeholder="03/24/2017" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td class="process">
                                                <div class="progress-group">
                                                        <span class="progress-text">
                                                            <span class="start">02/24/2017</span> <span class="end">03/24/2017</span>
                                                        </span>
                                                    <span class="progress-number">80%</span>

                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="submit" class="add-emp"><i class="fa fa-plus-circle"></i></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="cbx-select" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="Công việc 1" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="start" id="start" class="" placeholder="02/24/2017" />
                                            </td>
                                            <td>
                                                <input data-provide="datepicker" name="end" id="end" class="" placeholder="03/24/2017" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td>
                                                <input type="text" class="" value="ABC" />
                                            </td>
                                            <td class="process">
                                                <div class="progress-group">
                                                        <span class="progress-text">
                                                            <span class="start">02/24/2017</span> <span class="end">03/24/2017</span>
                                                        </span>
                                                    <span class="progress-number">80%</span>

                                                    <div class="progress sm">
                                                        <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <button type="button" class="delete-emp"><i class="fa fa-trash-o"></i></button>
                                                <button type="button" class="edit-emp"><i class="fa fa-pencil"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- edit group -->
    <div class="modal fade" id="eidtGroup" tabindex="-1" role="dialog" aria-labelledby="myEditGroup">
        <div class="modal-dialog" role="document" style="width: 300px">
            <form class="modal-content" method="post" role="form" id="form-editGroup" url-ajax="{{ URL::route('ajax.erp.project.work.postEditGroupTask') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myEditGroup">Sửa tên nhóm</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Tên nhóm:<span class="note">(*)</span></label>
                                <input type="text" class="form-control" name="editGroup" id="txt_editGroup" placeholder="Tên nhóm" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <!-- create group -->
    <div class="modal fade" id="createGroup" tabindex="-1" role="dialog" aria-labelledby="myCreateGroup">
        <div class="modal-dialog" role="document" style="width: 300px">
            <form class="modal-content" method="post" role="form" id="form-createGroup" url-ajax="{{ URL::route('ajax.erp.project.work.addGroupTask') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="idProject" value="{{ Session::get('idProject') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateGroup">Thêm nhóm</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Tên nhóm:<span class="note">(*)</span></label>
                                <input type="text" class="form-control" name="groupTask" id="group" placeholder="Tên nhóm" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <!-- edit job -->
    <div class="modal fade" id="editJob" tabindex="-1" role="dialog" aria-labelledby="myEditJob">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-editJob" url-ajax="{{ URL::route('ajax.erp.project.work.postEditTaskProject') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="taskId" value="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myEditJob">Sửa công việc</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên công việc:</label>
                                <input type="text" class="form-control" name="nameEditJob" id="nameEditJob" placeholder="Nhập tên công việc..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Nhóm công việc:</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian bắt đầu:</label>
                                <input data-provide="datepicker" class="form-control " name="editStartDateJob" id="editStartDateJob" placeholder="mm/dd/yyyy" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian kết thúc:</label>
                                <input data-provide="datepicker" class="form-control " name="editEndDateJob" id="editEndDateJob" placeholder="mm/dd/yyyy" />
                            </div>
                        </div>
                        @if(isset($dataStaff))
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Phụ trách chính:</label>
                                    <select name="managerTask" id="managerTask" data-placeholder="--chọn--" class="chosen-select form-control">
                                        <option value=""></option>
                                        @foreach($dataStaff as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="">
                                    <label class="lbl-form">Thành viên:</label>
                                    <select id="memberTask" name="memberTask" data-placeholder="Choose" multiple class="chosen-select form-control">
                                        <option value=""></option>
                                        @foreach($dataStaff as $value)
                                            <option value="{{ $value['_id'] }}">{{ $value['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endif
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control" rows="3" placeholder="Nhập mô tả..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <!-- create job -->
    <div class="modal fade" id="createJob" tabindex="-1" role="dialog" aria-labelledby="myCreateJob">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-createJob" url-ajax="{{ URL::route('ajax.erp.project.work.postAddTaskToGroupProject') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateJob">Thêm công việc</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên công việc:</label>
                                <input type="text" class="form-control" name="nameJob" id="nameJob" placeholder="Nhập tên công việc..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Nhóm công việc:</label>
                                <input type="text" class="form-control" value="nhom abc" disabled="disabled" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian bắt đầu:</label>
                                <input type="text" data-provide="datepicker" class="form-control " name="startDateJob" id="startDateJob" placeholder="mm/dd/yyyy" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Thời gian kết thúc:</label>
                                <input type="text" data-provide="datepicker" class="form-control " name="endDateJob" id="endDateJob" placeholder="mm/dd/yyyy" />
                            </div>
                        </div>
                        @if(isset($dataStaff))
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Phụ trách chính:</label>
                                <select name="managerTask" id="managerTask" data-placeholder="--chọn--" class="chosen-select form-control">
                                    <option value=""></option>
                                    @foreach($dataStaff as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Thành viên:</label>
                                <select id="memberTask" name="memberTask" data-placeholder="Choose" multiple class="chosen-select form-control">
                                    <option value=""></option>
                                    @foreach($dataStaff as $value)
                                    <option value="{{ $value['_id'] }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea name="description" class="form-control" rows="3" placeholder="Nhập mô tả..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <!--////////////////////////////////////////////////////////// -->
    <script src="{{ asset('js/web.js') }}"></script>
    <script src="{{ asset('js/staff.js') }}"></script>
    <script src="{{ asset('js/group.staff.js') }}"></script>
    <script>
        function imgError(img) {
            img.error = "";
            img.src =window.location.origin + '/img_empl/default.jpg';
        }
    </script>
@endsection