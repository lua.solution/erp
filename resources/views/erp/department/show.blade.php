@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-default">
                    <div class="panel-heading">Post {{ $department['_id'] }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/erp/department/list') }}" title="Back">
                            <button class="btn btn-warning btn-xs">
                                <i class="fa fa-arrow-left" aria-hidden="true">
                                </i> Back</button>
                        </a>
                        <a href="{{ url('/erp/department/' . $department['_id'] . '/edit') }}" title="Edit Post">
                            <button class="btn btn-primary btn-xs">
                                <i class="fa fa-pencil-square-o" aria-hidden="true">
                                </i> Edit
                            </button>
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['erp/department/delete', $department['_id']],
                            'style' => 'display:inline'
                            ]) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete Department',
                            'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $department['_id'] }}</td>
                                </tr>
                                <tr>
                                    <th> Code </th>
                                    <td> {{ $department['code'] }} </td>
                                </tr>
                                <tr>
                                    <th> Name </th>
                                    <td> {{ $department['name'] }} </td>
                                </tr>
                                <tr>
                                    <th> Parent </th>
                                    <td> {{$department['parent_id'] }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection