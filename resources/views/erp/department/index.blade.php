@extends('erp.layouts.app')

@section('content')
    <div class="group-breadcrumb">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <li id="menueml">Phòng ban</li>
            <li class="active">Danh sách phòng ban</li>
        </ol>
        <ol class="breadcrumb pull-right">
            <li>
                <form action="{{ route("erp.department.index") }}" method="get">
                    <p class="input-search">
                        <i class="fa fa-search"></i>
                        <input type="text" id="search" name="search" class="form-control"
                               placeholder="Nhập thông tin tìm kiếm..."/>
                        <input class="btn-icon" type="submit" value=""/>
                    </p>
                </form>
            </li>
            <li><a href="#" data-toggle="modal" data-target="#createProject"><i class="fa fa-plus" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-th-large" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-th-list" aria-hidden="true"></i></a></li>
            <li class="active"><i class="fa fa-user"></i></li>
        </ol>
    </div>
    <div class="content-page">
        <div class="group-box" style="border-bottom: 1px solid #fafafa; box-shadow: 0 1px 0 #d8d8d8;">
            <div class="group-btn pull-left">

                <h3 class="title-page">Danh sách phòng ban</h3>
                <h5 class="des-page"> </h5>
            </div>
            <div class="pull-right">
                <p class="btn-box btn-green">
                    <i class="fa fa-plus"></i>
                    <a href="#" data-toggle="modal" data-target="#createDepart">Thêm</a>
                </p>
                <p class="btn-box btn-green">
                    <i class="fa fa-trash-o"></i>
                    <input id="btn-delete-dept" class="btn-icon" type="button" value="Xóa" />
                </p>
            </div>
        </div>
        <div class="group-employee group-department">
        @if(count($departments)>0)
            @foreach($departments as $dept)
            <div class="employee-item"  data-id="{{$dept['_id']}}">
                <div class="content">
                    <a href="#" class="name">{{ $dept['name'] or "" }}</a>
                    <p>- Trưởng phòng: {{ $dept['truongphong'] or "" }}</p>
                    <p>- Số nhân viên: {{ $dept['count_staff'] or 0 }}</p>

                    <a href="{{ url('erp/employer/department?depid='.$dept['_id']) }}"></a>
                   @if($dept['count_staff'] > 0)
                    <p class="btn-box btn-gray">
                        <a href="{{ url('erp/employer?depid='.$dept['_id']) }}">Danh sách nhân viên</a>

                    </p>
                       @else
                        <p class="btn-box btn-gray">
                            <a >Chưa có nhân viên</a>
                        </p>
                    @endif
                </div>
                <div class="checkbox checkbox-success ">
                    <input id="checkbox-{{$dept['_id']}}" class="chkdelete" type="checkbox" name="_id[]" value="{{$dept['_id']}}"/>
                    <label for="checkbox-{{$dept['_id']}}"></label>
                </div>
                <a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#editDepart" onclick="editDepartment('{{ $dept['_id'] }}')"><i class="fa fa-pencil "></i></a>
            </div>
            @endforeach
        @endif

        </div>
    </div>
<!-- Create dep-->
    <div class="modal fade" id="createDepart" tabindex="-1" role="dialog" aria-labelledby="myCreateDepart">
        <div class="modal-dialog" role="document" style="width:300px">
            <form class="modal-content" method="post" role="form" id="form-createDepart" action="{{url('erp/department/create')}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateDepart">Thêm mới phòng ban</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="background-color: plum">
                        <div class="error-form">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Tên phòng ban:<span class="note">(*)</span></label>
                                <input type="text" class="form-control" name="name" id="department_name" placeholder="Tên phòng ban" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <!-- Edit dep-->
    <div class="modal fade" id="editDepart" tabindex="-1" role="dialog" aria-labelledby="myeditDepart">
        <div class="modal-dialog" role="document" style="width:300px">
            <form class="modal-content" method="post" role="form" id="form-editDepart" action="{{url('erp/department/update')}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myeditDepart">Chỉnh sửa phòng ban</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="background-color: plum">
                        <div class="error-form">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="form-group">
                                <label>Tên phòng ban:<span class="note">(*)</span></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Tên phòng ban" />
                                <input type="hidden" class="name edit-test" name="_id" id="_id" placeholder=""
                                       value=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        //validate create new department{menu personnel}
        $(function () {
            var formSubmit = $("#form-createDepart");
            formSubmit.validate({
                ignore: ":hidden:not(select)",
                rules: {
                    name: "required",
                },
                messages: {
                    name: "Vui lòng nhập tên phòng ban",
                },
                submitHandler: function (form,event) {
                    //code
                    event.preventDefault();
                    var url = formSubmit.attr('action');
//                    var modalDep = $("#createDepart");
//                    var url = modalDep.attr('ajax-url');

                    var data = new FormData($("#form-createDepart")[0]);
                    $.ajax({
                        type : 'POST',
                        url  : url,
                        data : data,
                        processData: false,
                        contentType: false,
                        success :  function(getdata)
                        {
                            console.log(getdata);

                            var employerGroup = $(".group-department");
                            if(getdata.result) {
                                var id = ( ( getdata.data._id.$oid===null  ) ? '' : getdata.data._id.$oid);
                                var name = ( ( getdata.data.name===null  ) ? '' : getdata.data.name);
                                var count_staff = ( ( getdata.data.count_staff===null  ) ? 0 : getdata.data.count_staff);

                                var html = '<div class="employee-item" data-id="'+id+'">'
                                    +'<div class="content">'
                                    +'<a href="#" class="name">'+name+'</a>'
                                    +'<p>- Trưởng phòng: </p>'
                                    +'<p>- Số nhân viên: '+count_staff+'</p>'
                                    +'<a href="#"></a>'
                                    +'<p class="btn-box btn-gray">'
                                    +'<a>Chưa có nhân viên</a>'
                                    +'</p>'
                                    +'</div>'
                                    +'<div class="checkbox checkbox-success ">'
                                    +'<input id="checkbox-'+id+'" class="chkdelete" type="checkbox" name="_id[]" value="'+id+'">'
                                    +'<label for="checkbox-'+id+'"></label>'
                                    +'</div>'
                                    +'<a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#editDepart" onclick="editDepartment('+"'"+id+"'"+')"><i class="fa fa-pencil "></i></a>'
                                    +'</div>'
                                employerGroup.append(html);
                                formSubmit.trigger("reset");
                                $('#createDepart').modal('hide');
                            }
                            else {
                                console.log(getdata.error);
                                $(".error-form").text("");
                                $.each(getdata.error, function (key, value) {
                                    $(".error-form").append("<p>" + value + "</p>");
                                });

                            }
                        }
                    });
                    return false;
//                    $('#createDepart').modal('hide');
                }
            });
            var formSubmitEdit = $("#form-editDepart");
            formSubmitEdit.validate({
                ignore: ":hidden:not(select)",
                rules: {
                    name: "required",
                },
                messages: {
                    name: "Vui lòng nhập tên phòng ban",
                },
                submitHandler: function (form,event) {
                    //code
                    event.preventDefault();
                    var url = formSubmitEdit.attr('action');
//                    var modalDep = $("#createDepart");
//                    var url = modalDep.attr('ajax-url');

                    var data = new FormData($("#form-editDepart")[0]);
                    $.ajax({
                        type : 'POST',
                        url  : url,
                        data : data,
                        processData: false,
                        contentType: false,
                        success :  function(getdata)
                        {
                            console.log(getdata);
                            var employerGroup = $(".group-department");
                            if(getdata.result){
                                var id = ( ( getdata.data._id.$oid===null  ) ? '' : getdata.data._id.$oid);
                                var name = ( ( getdata.data.name===null  ) ? '' : getdata.data.name);
                                var count_staff = ( ( getdata.data.count_staff===null  ) ? 0 : getdata.data.count_staff);
                                var truongphong = ( ( getdata.data.truongphong===null  ) ? 0 : getdata.data.truongphong);
                                var depUpdate = $("[data-id="+id+"]");
                                depUpdate.text('');
                                var link = '<a>Chưa có nhân viên</a>';
                                if(count_staff > 0 )
                                    link = "<a href='employer?depid=" + id + "'>Danh sách nhân viên</a>";
                                var html =
                                    '<div class="content">'
                                    +'<a href="#" class="name">'+name+'</a>'
                                    +'<p>- Trưởng phòng: '+truongphong+'</p>'
                                    +'<p>- Số nhân viên: '+count_staff+'</p>'
                                    +'<a href="#"></a>'
                                    +'<p class="btn-box btn-gray">'
                                    + link
                                    +'</p>'
                                    +'</div>'
                                    +'<div class="checkbox checkbox-success ">'
                                    +'<input id="checkbox-'+id+'" class="chkdelete" type="checkbox" name="_id[]" value="'+id+'">'
                                    +'<label for="checkbox-'+id+'"></label>'
                                    +'</div>'
                                    +'<a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#editDepart" onclick="editDepartment('+"'"+id+"'"+')"><i class="fa fa-pencil "></i></a>'

                                depUpdate.append(html);

                            }
                            else {
                                console.log(getdata.error);
                                $(".error-form").text("");
                                $.each(getdata.error, function (key, value) {
                                    $(".error-form").append("<p>" + value + "</p>");
                                });
                                return;
                            }
                            $('#editDepart').modal('hide');
                        }
                    });
                    return false;
                }

            });

            $( "#btn-delete-dept" ).click(function() {
                var selected = [];
                $(".group-department .employee-item").each(function () {
                    var chkdelete = $(this).find(".chkdelete");
                    if ($(chkdelete).is(":checked")) {
                        selected.push(chkdelete.val());
                    }
                });
                if (selected.length == 0) {
                    alert("chưa chọn phòng ban cần xóa");
                    return;
                }

                var token = $("meta[name='csrf-token']").attr("content");

                var data = {
                    "_token": token,
                    "_id": selected
                };
                $.ajax({
                    type: 'POST',
                    url: '/erp/department/destroy',
                    data: data,
                    success: function (data) {
                        console.log(data);
                        if (data.result) {
                            for (i = 0; i < selected.length; i++) {
                                $("[data-id=" + selected[i] + "]").css('display', 'none');
                            }
                        }
                    }
                });
                return false;
            });
        });
    </script>
    <script>
        $(document).ready(function ( ) {
            $('#createDepart').on('hidden.bs.modal', function () {
                $(".error-form").text("");
            })
            $('#editDepart').on('hidden.bs.modal', function () {
                $(".error-form").text("");
            })
//            $(document).on('click', '#createDepart .btn-success', function(){
//
//            });
        });
        function editDepartment(id)
        {


            $.ajax({
                type : 'GET',
                dataType: "json",
                url  : '/erp/department/'+id,
                success :  function(data)
                {
//                    console.log(data);
                    if(data.success){
                        var formdetail = $("#form-editDepart");

                        $.each( data, function( key, value ) {
                            console.log(key);
                            if(value!==null){
                                if(key=="_id"){
                                    formdetail.find("#" + key).val(id);
                                }

                                else{
                                    formdetail.find("#"+key).val(value);
                                }
                            }
                        })
                    }
                }
            })
            return false;
        }
    </script>
@endsection
