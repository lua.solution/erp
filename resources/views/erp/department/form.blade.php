<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    @if(isset($department['_id']))
        <input type="hidden" name="_id" value="{{ $department['_id']}}">
    @endif
    <label for="code" class="col-md-4 control-label">Code</label>
    <div class="col-md-6">
        <input name="code" type="text" class="form-control" value="{{ $department['code'] or old('code') }}">
        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-md-4 control-label">Name</label>
    <div class="col-md-6">
        <input name="name" type="text" class="form-control"  value="{{ $department['name'] or old('name') }}">
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('deparment_id') ? 'has-error' : ''}}">
    <label for="departmentparent" class="col-md-4 control-label">Department Parent</label>
    <div class="col-md-6">
        <select name="parent_id" class="form-control">
            <option value="0">No parent</option>
            @foreach($departmentParent as $value)
                @if(isset($department['parent_id']))
                    <option value="{{$value['_id']}}"{{$department['parent_id']== $value['_id'] ? 'selected' : ''}}>{{$value['name']}}</option>
                @else
                    <option value="{{$value['_id']}}">{{$value['name']}}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
