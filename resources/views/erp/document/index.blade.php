@extends('erp.layouts.app')

@section('content')
    <div class="content-page">
        <div class="document">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#doc1" aria-controls="doc1" role="tab" data-toggle="tab">
                        <span class="num">214</span>
                        <span class="des">Công văn đến</span></a></li>
                <li role="presentation"><a href="#doc2" aria-controls="doc2" role="tab" data-toggle="tab">
                        <span class="num">24</span>
                        <span class="des">Công văn đi</span></a></li>
                <li role="presentation"><a href="#doc3" aria-controls="doc3" role="tab" data-toggle="tab">
                        <span class="num">414</span>
                        <span class="des">Công văn nội bộ</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="doc1">

                    <div class="form-search">
                        <div class="pull-left">
                            <h3 class="title-page">Danh sách công văn đến</h3>
                        </div>
                        <div class="pull-right">
                            <p class="btn-box btn-green">
                                <i class="fa fa-plus"></i>
                                <a href="#" data-toggle="modal" data-target="#createDocument" class="createDocument">Thêm</a>
                            </p>
                            <p class="btn-box btn-green">
                                <i class="fa fa-trash-o"></i>
                                <input class="btn-icon" type="button" value="Xóa" />
                            </p>
                        </div>
                    </div>
                    <div class="content-doc">
                        <table class="table table-striped nowrap tbl-web-search">
                            <thead>
                            <tr class="hide">
                                <td data-name="index">1</td>
                                <td data-name="titleDoc">1</td>
                                <td data-name="numDoc">0</td>
                                <td data-name="dateDoc"></td>
                                <td data-name="to"></td>
                                <td data-name="desc"></td>
                                <td data-name="project"></td>
                                <td data-name="department">PB liên quan</td>
                                <td data-name="file">file</td>
                                <td data-name="note">0</td>
                                <td data-name="btn">
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Số CV</th>
                                <th>Ngày thêm</th>
                                <th>Nơi đến</th>
                                <th>Trích yếu</th>
                                <th>Dự án</th>
                                <th>PB liên quan</th>
                                <th>File</th>
                                <th>Ghi chú</th>
                                <th>Xử lý</th>
                            </tr>

                            </thead>
                            <tbody>
                            @foreach($documents_to as $doc)
                                <?php
                                $url_file = '';
                                if(isset($doc['url_file']) && !empty($doc['url_file'])){
                                    $url_file = '<a href="'.$doc['url_file'].'">File</a>';
                                }
                                ?>
                                <tr data-id="{{ $doc['_id'] or "" }}">
                                    <td>{{$loop->index+1}}</td>
                                    <td>{{ $doc['name'] or "" }}</td>
                                    <td>{{ $doc['code'] or "" }}</td>
                                    <td>{{ $doc['date'] or "" }}</td>
                                    <td>{{ $doc['to'] or "" }}</td>
                                    <td>{{ $doc['desc'] or "" }}</td>
                                    <td>{{ $doc['project'] or "" }}</td>
                                    <td>{{ $doc['department_rela'] or "" }}</td>
                                    <td>{!! $url_file !!}</td>
                                    <td>{{ $doc['note'] or "" }}</td>
                                    <td>
                                        <p class="btn-box btn-trans">
                                            <i class="fa fa-pencil"></i>
                                            <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                        </p>
                                        <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                        <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            {{--<tr>--}}
                            {{--<td>1</td>--}}
                            {{--<td>Tiêu đề</td>--}}
                            {{--<td>Số CV</td>--}}
                            {{--<td>10/02/2017</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>sdfsdf</td>--}}
                            {{--<td>sdfsdfsdfsdf</td>--}}
                            {{--<td>--}}
                            {{--<p class="btn-box btn-trans">--}}
                            {{--<i class="fa fa-pencil"></i>--}}
                            {{--<a href="#" data-toggle="modal" data-target="#editDocument"></a>--}}
                            {{--</p>--}}
                            {{--<button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>--}}
                            {{--<button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>--}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                            {{--<td>1</td>--}}
                            {{--<td>Tiêu đề</td>--}}
                            {{--<td>Số CV</td>--}}
                            {{--<td>10/02/2017</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>sdfsdf</td>--}}
                            {{--<td>sdfsdfsdfsdf</td>--}}
                            {{--<td>--}}
                            {{--<p class="btn-box btn-trans">--}}
                            {{--<i class="fa fa-pencil"></i>--}}
                            {{--<a href="#" data-toggle="modal" data-target="#editDocument"></a>--}}
                            {{--</p>--}}
                            {{--<button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>--}}
                            {{--<button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>--}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                            {{--<td>1</td>--}}
                            {{--<td>Tiêu đề</td>--}}
                            {{--<td>Số CV</td>--}}
                            {{--<td>10/02/2017</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>ABC</td>--}}
                            {{--<td>sdfsdf</td>--}}
                            {{--<td>sdfsdfsdfsdf</td>--}}
                            {{--<td>--}}
                            {{--<p class="btn-box btn-trans">--}}
                            {{--<i class="fa fa-pencil"></i>--}}
                            {{--<a href="#" data-toggle="modal" data-target="#editDocument"></a>--}}
                            {{--</p>--}}
                            {{--<button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>--}}
                            {{--<button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>--}}
                            {{--</td>--}}
                            {{--</tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="doc2">
                    <div class="form-search">
                        <div class="pull-left">
                            <h3 class="title-page">Danh sách công văn đi</h3>
                        </div>
                        <div class="pull-right">
                            <p class="btn-box btn-green">
                                <i class="fa fa-plus"></i>
                                <a href="#" data-toggle="modal" data-target="#createDocument" class="createDocument">Thêm</a>
                            </p>
                            <p class="btn-box btn-green">
                                <i class="fa fa-trash-o"></i>
                                <input class="btn-icon" type="button" value="Xóa" />
                            </p>
                        </div>
                    </div>
                    <div class="content-doc">
                        <table class="table table-striped nowrap tbl-web-search">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Số CV</th>
                                <th>Ngày thêm</th>
                                <th>Nơi đi</th>
                                <th>Trích yếu</th>
                                <th>Dự án</th>
                                <th>PB liên quan</th>
                                <th>File</th>
                                <th>Ghi chú</th>
                                <th>Xử lý</th>
                            </tr>

                            </thead>
                            <tbody>
                            <tr class="clone hide">
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="doc3">
                    <div class="form-search">
                        <div class="pull-left">
                            <h3 class="title-page">Danh sách công văn nội bộ</h3>
                        </div>
                        <div class="pull-right">
                            <p class="btn-box btn-green">
                                <i class="fa fa-plus"></i>
                                <a href="#" data-toggle="modal" data-target="#createDocument" class="createDocument">Thêm</a>
                            </p>
                            <p class="btn-box btn-green">
                                <i class="fa fa-trash-o"></i>
                                <input class="btn-icon" type="button" value="Xóa" />
                            </p>
                        </div>
                    </div>
                    <div class="content-doc">
                        <table class="table table-striped nowrap tbl-web-search">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Số CV</th>
                                <th>Ngày thêm</th>
                                <th>Trích yếu</th>
                                <th>Dự án</th>
                                <th>PB liên quan</th>
                                <th>File</th>
                                <th>Ghi chú</th>
                                <th>Xử lý</th>
                            </tr>

                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Tiêu đề</td>
                                <td>Số CV</td>
                                <td>10/02/2017</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>ABC</td>
                                <td>sdfsdf</td>
                                <td>sdfsdfsdfsdf</td>
                                <td>
                                    <p class="btn-box btn-trans">
                                        <i class="fa fa-pencil"></i>
                                        <a href="#" data-toggle="modal" data-target="#editDocument"></a>
                                    </p>
                                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                                    <button type="button" class="uploadDoc btn-trans"><i class="fa fa-download"></i></button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editDocument" tabindex="-1" role="dialog" aria-labelledby="myEditDocument">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-editDocument">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myEditDocument">Sửa công văn</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tiêu đề:</label>
                                <input type="text" class="form-control" name="editTitleDoc" id="editTitleDoc" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số công văn:</label>
                                <input type="text" class="form-control" disabled="disabled" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Ngày:</label>
                                <input data-provide="datepicker" class="form-control " disabled="disabled" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Nơi đến:</label>
                                <input type="text" class="form-control place" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Trích yếu:</label>
                                <input type="text" class="form-control" name="" id="" placeholder="" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">Dự án:</label>
                                <select data-placeholder="Choose" class="chosen-select form-control">
                                    <option value=""></option>
                                    <option value="United States">United States</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">Phòng ban liên quan:</label>
                                <select data-placeholder="Choose" class="chosen-select form-control">
                                    <option value=""></option>
                                    <option value="United States">United States</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">File:</label>
                                <input class="form-control" type="file" value="Upload" name="editFile" id="editFile" />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control" rows="3" placeholder="Nhập mô tả..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="createDocument" tabindex="-1" role="dialog" aria-labelledby="myCreateDocument">

        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-createDocument" action="{{ route('erp.document.create.post') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateDocument">Thêm công văn</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tiêu đề:</label>
                                <input type="hidden" class="form-control  form-data" name="group_name" id="group_name" placeholder="group_name" value="0"/>
                                <input type="text" class="form-control  form-data" name="titleDoc" id="titleDoc" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số công văn:</label>
                                <input type="text" class="form-control  form-data" name="numDoc" id="numDoc" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Ngày:</label>
                                <input data-provide="datepicker" class="form-control form-data" name="dateDoc" id="dateDoc" placeholder="Nhập..." />

                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Nơi đến:</label>
                                <input type="text" class="form-control place form-data" name="to" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12" style="display: none">
                            <div class="form-group">
                                <label>Nơi đi:</label>
                                <input type="text" class="form-control place-from form-data" name="from" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Trích yếu:</label>
                                <input type="text" class="form-control  form-data" name="desc" id="desc" placeholder="" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">Dự án:</label>
                                <select data-placeholder="Choose" class="chosen-select form-control form-data" name="projects">
                                    <option value=""></option>
                                    @foreach($projects as $p)
                                        <option value="{{$p['_id']}}">{{$p['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">Phòng ban liên quan:</label>
                                <select data-placeholder="Choose" multiple class="chosen-select form-control  form-data" name="departments[]">
                                    <option value=""></option>
                                    @foreach($departments as $dep)

                                        <option value="{{$dep['_id']}}">{{$dep['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label class="lbl-form">File:</label>
                                <input class="form-control form-data" type="file" value="Upload" name="createFile" id="createFile" />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control" rows="3" placeholder="Nhập mô tả..." name="note"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('js/staff.js') }}"></script>
    <script src="{{ asset('js/document.js') }}"></script>
@endsection