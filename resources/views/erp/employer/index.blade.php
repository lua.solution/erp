@extends('erp.layouts.app')
@section('content')
    <div class="group-breadcrumb">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <li id="menueml">
                @if(!isset($_GET['depid']))
                    Nhân sự
                @else
                    <a href="{{route('erp.department.index')}}">Phòng ban</a>
                @endif
            </li>
            <li class="active">{{ $depid['name'] or "Danh sách nhân viên" }}</li>
        </ol>
        <ol class="breadcrumb pull-right">
            <li>
                <form action="{{ route("erp.employer.index") }}" method="get">
                    <p class="input-search">
                        <i class="fa fa-search"></i>
                        <input type="text" id="search" name="search" class="form-control"
                               placeholder="Nhập thông tin tìm kiếm..."/>
                        <input class="btn-icon" type="submit" value=""/>
                    </p>
                </form>
            </li>

        </ol>
    </div>
    <!--list employee-->
    <div class="content-page ">
        <div class="group-box" style="border-bottom: 1px solid #fafafa; box-shadow: 0 1px 0 #d8d8d8;">
            <div class="group-btn pull-left">
                <h3 class="title-page">Danh sách nhân viên</h3>
                <h5 class="des-page"></h5>
            </div>
            <div class="pull-right">
                <p class="btn-box btn-green">
                    <i class="fa fa-plus"></i>
                    <a href="#" data-toggle="modal" data-target="#createEmp">Thêm</a>
                </p>
                <p class="btn-box btn-green">
                    <i class="fa fa-trash-o"></i>
                    <input class="btn-icon btn-remove-employer" type="button" value="Xóa" ajax-url="{{url('/erp/employer/destroy')}}" />
                </p>
                <p class="btn-box btn-green upload-file">
                    <i class="fa fa-upload"></i>
                    <input class="btn-icon choose-file" type="file" value="Upload"/>
                </p>
                <p class="btn-box btn-green">
                    <i class="fa fa-download"></i>
                    <input class="btn-icon" type="button" value="Download"
                           onclick="location.href = '{{ route('erp.employer.download') }}';"/>
                </p>
            </div>
        </div>
        <div class="group-employee">
            @foreach($staffs as $st)
                <div class="employee-item" data-id="{{$st['_id']}}">
                    <?php
                    if (!isset($st['image']))
                        $imgurl = 'img_empl/default.jpg';
                    else
                        $imgurl = 'img_empl/' . Session::get('dbname') . '/' . $st['_id'] . '.jpg';
                    ?>
                    <div class="img" id="{{$st['_id']}}" style="background-image: url({{ asset($imgurl) }});"></div>
                    <div class="content">
                        <a employer-id="{{ $st['_id'] }}" href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name btn-edit"
                        >{{ $st['name'] or "" }}</a>
                        <p>- Ngày sinh: {{ $st['ngaysinh'] or "" }}</p>
                        <p>- Phòng ban: {{ $st['dept'] or "" }}</p>
                        <p>- Chức vụ: {{ $st['res'] or "" }}</p>
                    </div>
                    <div class="checkbox checkbox-success ">
                        <input id="checkbox-{{$st['_id']}}" class="chkdelete" type="checkbox" name="_id[]"
                               value="{{$st['_id']}}"/>
                        <label for="checkbox-{{$st['_id']}}"></label>
                    </div>
                    <a href="javascript:void(0)" class="link-edit btn-edit" employer-id="{{ $st['_id'] }}" data-toggle="modal" data-target="#detailEmp"
                    ><i class="fa fa-pencil "></i></a>
                </div>
            @endforeach
        </div>
    </div>
    <!--Detail Employee-->
    <div class="modal fade" id="detailEmp" tabindex="-1" role="dialog" aria-labelledby="myDetailEmp"
         ajax-url="{{url('erp/employer/update')}}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{url('erp/employer/update')}}" method="post" role="form" id="form-detailEmp" ajax-check= "{{url('/erp/employer/check')}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myDetailEmp">Thông tin</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" style="background-color: plum">
                            <div class="error-form">
                            </div>
                        </div>
                        <div class="pull-right">
                            <p class="btn-box btn-black ">
                                <i class="fa fa-pencil "></i>
                                <button type="button" class="btn-icon btn-edit">Sửa</button>
                            </p>
                        </div>
                        <div class="employee-item">
                            {{--<img class="upload-file-container" id="preview" src={{ asset('images/img2.jpg') }} />--}}
                            {{--<input type="file" name="photo" class="employee-image-edit" />--}}

                            <div class="upload-file-container " style="">
                                <input type="file" name="photo" class="employee-image-edit"/>
                            </div>
                            {{--<div class="upload-file-container " style="">--}}
                            {{--<input type="file" name="photo" class="employee-image-edit" />--}}
                            {{--</div>--}}

                            <div class="content">
                                <input class="name edit-test" name="name" id="name" placeholder=""
                                       value="Nguyen Van A"/>
                                <input type="hidden" class="name edit-test" name="_id" id="_id" placeholder=""
                                       value=""/>
                                <p>
                                    <span>- Ngày sinh:   </span>
                                    <input data-provide="datepicker" name="ngaysinh" id="ngaysinh"
                                           class="form-control edit-test" placeholder="" value="20/10/1992" disabled="disabled"/>
                                </p>
                                <p {{ (isset($depid ) && $depid != "" ) ? 'style=' . 'display:none' :'' }}>
                                    <span>- Phòng ban:   </span>
                                    <select class="form-control edit-test " name="departments_id" id="departments_id"
                                            data-placeholder="--chọn--" disabled="disabled">
                                        <option value=""></option>
                                        @foreach($departments as $d)
                                            <option value="{{$d['_id']}}">{{$d['name']}}</option>
                                        @endforeach
                                    </select>
                                </p>
                                <p>
                                    <span>- Chức vụ:</span>
                                    <select class="form-control edit-test " name="res" id="res"
                                            data-placeholder="--chọn--" disabled="disabled">
                                        <option value="">-- Chọn --</option>
                                        <option value="manager">Manager</option>
                                        <option value="employer">Employer</option>
                                        <option value="designer">Designer</option>
                                        <option value="trưởng phòng">Trưởng Phòng</option>
                                    </select>
                                </p>
                            </div>
                        </div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#basic" aria-controls="basic" role="tab"
                                                                      data-toggle="tab">Thông tin public</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"
                                                       data-toggle="tab">TT cá nhân</a></li>
                            <li role="presentation"><a href="#exper" aria-controls="exper" role="tab" data-toggle="tab">Kinh
                                    nghiệm</a></li>
                            <li role="presentation"><a href="#user" aria-controls="user" role="tab" data-toggle="tab">Tài
                                    khoản</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="basic">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Giới tính:</label>
                                        <select class="form-control  edit-test" name="gender" id="gender"
                                                data-placeholder="--chọn--" disabled="disabled">
                                            <option></option>
                                            <option selected>Nam</option>
                                            <option>Nữ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Email</label>
                                        <input class="form-control edit-test" name="email" id="email" placeholder=""
                                               value="test@abc.com" disabled="disabled">
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Số điện thoại</label>
                                        <input class="form-control edit-test" name="phone" id="phone" placeholder=""
                                               value="09214568542" disabled="disabled"/>
                                    </div>
                                </div>

                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Hợp đồng</label>
                                        <select class="form-control edit-test" name="contracts" id="contracts"
                                                data-placeholder="--chọn--" disabled="disabled">
                                            <option></option>
                                            <option>Chính thức</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- CMND</label>
                                        <input class="form-control edit-test" name="personal_id" id="personal_id"
                                               placeholder="" value="290945932" disabled="disabled"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Số bảo hiểm</label>
                                        <input class="form-control edit-test" name="assuren" id="assuren" placeholder=""
                                               value="09214568542" disabled="disabled"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Quan hệ</label>
                                        <select class="form-control edit-test" name="relation" id="relation"
                                                data-placeholder="--chọn--" disabled="disabled">
                                            <option>Độc thân</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Số tài khoản</label>
                                        <input class="form-control edit-test" name="bank_acc" id="bank_acc"
                                               placeholder="" value="09214568542" disabled="disabled"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Địa chỉ</label>
                                        <input class="form-control edit-test" name="address" id="address" placeholder=""
                                               value="Số 123, p Tân Thới hiệp, quận 12, HCM" disabled="disabled"/>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Ngân hàng</label>
                                        <input class="form-control edit-test" name="bank_name" id="bank_name"
                                               placeholder="" value="Teckcombank" disabled="disabled"/>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="exper">
                                <div class="pull-left" style="margin-bottom: 10px;">
                                    <p class="btn-box btn-blue" style="display:none">
                                        <i class="fa fa-plus"></i>
                                        <a href="javascript:void(0)" class="add-history">Thêm</a>
                                    </p>
                                </div>
                                <div class="group-company">
                                    <p class="btn-box btn-delete">
                                        <i class="fa fa-trash-o"></i>
                                        <input class="btn-icon edit-test" type="submit" value=""/>
                                    </p>
                                    <input name="company_name_his[]" class="lbl-company edit-test"
                                           value="1. Công ty TNHH ABC"/>
                                    <input name="time_work[]" class="des-time edit-test" placeholder=""
                                           value="Từ 02-2017 đến 12-2017" disabled="disabled"/>
                                    <label>- Nhiệm vụ</label>
                                    <textarea name="des_his[]" class="textarea edit-test edit-test">Some plugins and CSS components depend on other plugins. If you include plugins individually, make sure to check for these dependencies in the docs. Also note that all plugins depend on jQuery (this means jQuery must be included before the plugin files). Consult our bower.json to see which versions of jQuery are supported.</textarea>
                                </div>
                                <div class="group-company">
                                    <p class="btn-box btn-delete">
                                        <i class="fa fa-trash-o"></i>
                                        <input class="btn-icon edit-test" type="submit" value=""/>
                                    </p>
                                    <input name="company_name_his[]" class="lbl-company edit-test"
                                           value="2. Công ty TNHH ABC2222" disabled="disabled"/>
                                    <input name="time_work[]" class="des-time edit-test" placeholder=""
                                           value="Từ 02-2017 đến 12-2017" disabled="disabled"/>

                                    <label>- Nhiệm vụ</label>
                                    <textarea name="des_his[]" class="textarea edit-test edit-test" disabled="disabled">1111Some plugins and CSS components depend on other plugins. If you include plugins individually, make sure to check for these dependencies in the docs. Also note that all plugins depend on jQuery (this means jQuery must be included before the plugin files). Consult our bower.json to see which versions of jQuery are supported.</textarea>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="user">
                                <div class="col-md-12 col-xs-12">
                                    <div class="checkbox checkbox-success " id="checkbox-active">
                                        <input id="active" type="checkbox"/>
                                        <label for="active">Active tài khoản</label>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group">
                                        <label>- Tên đăng nhập</label>
                                        <input class="form-control edit-account" name="accountUser" id="accountUser"
                                               placeholder="" value="test@abc.com"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                        <button type="submit" class="btn btn-success">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--create employee-->
    <div class="modal fade" id="createEmp" tabindex="-1" role="dialog" aria-labelledby="myCreateEmp">
        {!! Form::open(['url' => 'erp/employer/create', 'class' => '', 'files' => true, 'id' => 'formCreateEmp','ajax-check' => url('/erp/employer/check')]) !!}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateEmp">Thêm nhân viên</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="background-color: plum">
                        <div class="error-form">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên nhân viên:<span class="note">(*)</span></label>
                                <input type="text" class="form-control" name="name" id="name"
                                       placeholder="Nguyen Van A"/>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Ngày sinh:</label>
                                <input data-provide="datepicker" class="form-control" placeholder="dd/mm/yyyy"
                                       name="ngaysinh" value=""/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Giới tính:</label>
                                <select class="form-control" name="gender">
                                    <option value="Nam" selected>Nam</option>
                                    <option value="Nữ">Nữ</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số điện thoại:</label>
                                <input type="text" class="form-control" placeholder="290945932" name="phone"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Email: <span class="note">(*)</span></label>
                                <input type="email" class="form-control" placeholder="abc@abc.com" name="email"
                                       id=emailCreate"/>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>CMND:<span class="note">(*)</span></label>
                                <input type="text" class="form-control" placeholder="290945932" name="personal_id"/>
                                <span class="error"></span>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Địa chỉ:</label>
                                <input type="text" class="form-control" placeholder="số 12, phường 2..."
                                       name="address"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số tài khoản:</label>
                                <input type="text" class="form-control" placeholder="025951..." name="bank_acc"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên ngân hàng:</label>
                                <input type="text" class="form-control" placeholder="Nhân hàng ABC, chi nhánh..."
                                       name="bank_name"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số bảo hiểm:</label>
                                <input type="text" class="form-control" placeholder="025951..." name="assuren"/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Loại hợp đồng:</label>
                                <select class="form-control" name="contracts">
                                    <option value="Chính thức" selected>Chính thức</option>
                                    <option value="Thử việc">Thử việc</option>
                                    <option value="Bán thời gian">Bán thời gian</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12" {{ (isset($depid ) && $depid != "" ) ? 'style=' . 'display:none' :'' }}>
                            <div class="form-group">
                                <label>Phòng ban:</label>
                                <select data-placeholder="Choose" name="departments_id"
                                        class="chosen-select form-control">
                                    <option value=""></option>
                                    @foreach($departments as $d)
                                        @if(isset($depid))
                                            @if($d['_id']==$depid['id'])
                                                <option value="{{$d['_id']}}" selected>{{$d['name']}}</option>
                                            @else
                                                <option value="{{$d['_id']}}">{{$d['name']}}</option>
                                            @endif
                                        @else
                                            <option value="{{$d['_id']}}">{{$d['name']}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Chức vụ:</label>
                                <select class="form-control" name="res">
                                    <option value="manager" selected>Manager</option>
                                    <option value="employer">Employer</option>
                                    <option value="designer">Designer</option>
                                    <option value="trưởng phòng">Trưởng Phòng</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div id="imagedisplay">
                                <input type="file" id="img" name="img">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Lưu</button>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    {{--<script>--}}
    {{--$(document).ready(function () {--}}
    {{--//                $('#detailEmp .edit-test').on('click', function () {--}}
    {{--//                    alert('addMore click event');--}}
    {{--//                });--}}
    {{--$("input.edit-test").attr("disabled", true);--}}
    {{--$('.upload-file-container input').css('display', 'none');--}}
    {{--$('#detailEmp .btn-edit').click(function () {--}}
    {{--$("input.edit-test").attr("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--$('.upload-file-container ').addClass('avatar');--}}
    {{--$('#exper .btn-box').css('display', 'block');--}}
    {{--});--}}
    {{--$('.group-employee .link-edit').click(function () {--}}
    {{--$(".group-company .btn-delete").css('display', 'block');--}}
    {{--$("input.edit-test").attr("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--});--}}
    {{--});--}}
    {{--$(document).on('click', '#detailEmp .btn-delete', function () {--}}
    {{--$(this).parent().remove();--}}
    {{--});--}}

    {{--function updateEmployer() {--}}
    {{--var modal = $("#detailEmp");--}}
    {{--var form = $('form#form-detailEmp');--}}
    {{--var employerGroup = $(".group-employee");--}}
    {{--var img = employerGroup.attr('img-default');--}}
    {{--var name = form.find("#name");--}}
    {{--if (name.val() === "") {--}}
    {{--alert("Chưa nhập tên nhân viên");--}}
    {{--return;--}}
    {{--}--}}
    {{--var url = modal.attr('ajax-url');--}}
    {{--var data = $('form#form-detailEmp').serialize();--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--url: url,--}}
    {{--data: data,--}}
    {{--dataType: "json",--}}
    {{--success: function (getdata) {--}}
    {{--console.log(getdata);--}}

    {{--if (getdata.result) {--}}

    {{--var id = ( ( getdata.data._id.$oid === null  ) ? '' : getdata.data._id.$oid);--}}
    {{--var name = ( ( getdata.data.name === null  ) ? '' : getdata.data.name);--}}
    {{--var ngaysinh = ( ( getdata.data.ngaysinh === null  ) ? '' : getdata.data.ngaysinh);--}}
    {{--var dept = ( ( getdata.data.dept === null  ) ? '' : getdata.data.dept);--}}
    {{--var res = ( ( getdata.data.res === null  ) ? '' : getdata.data.res);--}}
    {{--var employerUpdate = $("[data-id=" + id + "]");--}}
    {{--employerUpdate.text('');--}}
    {{--var html =--}}
    {{--'<div class="img" style="background-image: url(' + getdata.data.url_image + ');"></div>'--}}
    {{--+ '<div class="content">'--}}
    {{--+ '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name" onclick="editEmployer(' + "'" + id + "'" + ')">' + name + '</a>'--}}
    {{--+ '<p>- Ngày sinh: ' + ngaysinh + '</p>'--}}
    {{--+ '<p>- Phòng ban: ' + dept + '</p>'--}}
    {{--+ '<p>- Chức vụ: ' + res + '</p>'--}}
    {{--+ '</div>'--}}
    {{--+ '<div class="checkbox checkbox-success ">'--}}
    {{--+ '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'--}}
    {{--+ '<label for="checkbox-' + id + '"></label>'--}}
    {{--+ '</div>'--}}
    {{--+ '<a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#detailEmp" onclick="editEmployer(' + "'" + id + "'" + ')"><i class="fa fa-pencil "></i></a>'--}}

    {{--employerUpdate.append(html);--}}
    {{--//                        $('#formCreateEmp').trigger("reset");--}}
    {{--$('#detailEmp').modal('hide');--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}--}}

    {{--$(document).on('click', '#detailEmp .add-history', function () {--}}
    {{--var html = '<div class="group-company">'--}}
    {{--+ '<p class="btn-box btn-delete">'--}}
    {{--+ '<i class="fa fa-trash-o"></i>'--}}
    {{--+ '<input class="btn-icon edit-test" type="submit" value="">'--}}
    {{--+ '</p>'--}}
    {{--+ '<input name="company_name_his[]" class="lbl-company edit-test" value="" placeholder="Tên công ty"/>'--}}
    {{--+ '<input name="time_work[]" class="des-time edit-test" placeholder="Thời gian" value="" />'--}}
    {{--+ '<label>- Nhiệm vụ</label>'--}}
    {{--+ '<textarea name="des_his[]" class="textarea edit-test edit-test" placeholder="Mô tả công việc"></textarea>'--}}
    {{--+ '</div>'--}}
    {{--$('#exper').append(html);--}}

    {{--});--}}

    {{--function addEmployer() {--}}
    {{--var form = $("#createEmp");--}}
    {{--var employerGroup = $(".group-employee");--}}
    {{--var img = employerGroup.attr('img-default');--}}
    {{--var name = $("#name");--}}
    {{--if (name.val() === "") {--}}
    {{--name.next().text("Chưa nhập tên nhân viên");--}}
    {{--return;--}}
    {{--}--}}
    {{--var url = form.attr('ajax-url');--}}

    {{--//            var data = $('form#formCreateEmp').serialize();--}}

    {{--var data = new FormData($("#formCreateEmp")[0]);--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--url: url,--}}
    {{--data: data,--}}
    {{--processData: false,--}}
    {{--contentType: false,--}}
    {{--success: function (getdata) {--}}
    {{--//   $('#imagedisplay').html("<img src=" + data.url + "" + data.name + ">");--}}

    {{--if (getdata.result) {--}}
    {{--var id = ( ( getdata.data._id.$oid === null  ) ? '' : getdata.data._id.$oid);--}}
    {{--var ngaysinh = ( ( getdata.data.ngaysinh === null  ) ? '' : getdata.data.ngaysinh);--}}
    {{--var dept = ( ( getdata.data.dept === null  ) ? '' : getdata.data.dept);--}}
    {{--var name = ( ( getdata.data.name === null  ) ? '' : getdata.data.name);--}}
    {{--var res = ( ( getdata.data.res === null  ) ? '' : getdata.data.res);--}}
    {{--var html = '<div class="employee-item" data-id="' + getdata.data._id.$oid + '">'--}}
    {{--+ '<div class="img" style="background-image: url(' + getdata.data.url_image + ');"></div>'--}}
    {{--+ '<div class="content">'--}}
    {{--+ '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name" onclick="editEmployer(' + "'" + id + "'" + ')">' + name + '</a>'--}}
    {{--+ '<p>- Ngày sinh: ' + ngaysinh + '</p>'--}}
    {{--+ '<p>- Phòng ban: ' + dept + '</p>'--}}
    {{--+ '<p>- Chức vụ: ' + res + '</p>'--}}
    {{--+ '</div>'--}}
    {{--+ '<div class="checkbox checkbox-success ">'--}}
    {{--+ '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'--}}
    {{--+ '<label for="checkbox-' + id + '"></label>'--}}
    {{--+ '</div>'--}}
    {{--+ '<a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#detailEmp" onclick="editEmployer(' + "'" + id + "'" + ')"><i class="fa fa-pencil "></i></a>'--}}
    {{--+ '</div>';--}}
    {{--employerGroup.append(html);--}}
    {{--$('#formCreateEmp').trigger("reset");--}}
    {{--$('#createEmp').modal('hide');--}}
    {{--var data = new FormData($("#formCreateEmp")[0]);--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}--}}

    {{--function removeEmployer() {--}}
    {{--var selected = [];--}}
    {{--$(".group-employee .employee-item").each(function () {--}}
    {{--var chkdelete = $(this).find(".chkdelete");--}}
    {{--if ($(chkdelete).is(":checked")) {--}}
    {{--selected.push(chkdelete.val());--}}
    {{--}--}}
    {{--});--}}
    {{--if (selected.length == 0) {--}}
    {{--alert("chưa chọn nhân viên cần xóa");--}}
    {{--return;--}}
    {{--}--}}
    {{--var token = $("meta[name='csrf-token']").attr("content");--}}
    {{--var data = {--}}
    {{--"_token": token,--}}
    {{--"_id": selected--}}
    {{--};--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--url: '/erp/employer/destroy',--}}
    {{--data: data,--}}
    {{--success: function (data) {--}}
    {{--console.log(data);--}}
    {{--if (data.result) {--}}
    {{--for (i = 0; i < selected.length; i++) {--}}
    {{--$("[data-id=" + selected[i] + "]").css('display', 'none');--}}
    {{--}--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}--}}

    {{--function editEmployer(id) {--}}
    {{--$.ajax({--}}
    {{--type: 'GET',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/' + id,--}}

    {{--success: function (data) {--}}
    {{--//                    console.log(data.work_history);--}}
    {{--//                    console.log(data);--}}
    {{--if (data.success) {--}}

    {{--var formdetail = $("#form-detailEmp");--}}

    {{--if (!data.hasOwnProperty('work_history')) {--}}
    {{--$('#exper').find('.group-company').remove();--}}
    {{--}--}}
    {{--$.each(data, function (key, value) {--}}
    {{--if (value !== null) {--}}
    {{--if (key == "_id") {--}}
    {{--}--}}

    {{--else if (key == "url_image") {--}}
    {{--formdetail.find('.upload-file-container').attr("style", 'background-image: url(' + value + ');');--}}
    {{--}--}}
    {{--else if (key == "work_history") {--}}

    {{--$('#exper').find('.group-company').remove();--}}
    {{--var countWorkHistory = value.length--}}
    {{--for (i = 0; i < countWorkHistory; i++) {--}}
    {{--var company = ((value[i][0].company_name !== 'undefined') ? value[i][0].company_name : '');--}}
    {{--var time = ((value[i][0].time !== 'undefined') ? value[i][0].time : '');--}}
    {{--var des = ((value[i][0].des !== 'undefined') ? value[i][0].des : '');--}}
    {{--var html = '<div class="group-company">'--}}
    {{--+ '<p class="btn-box btn-delete">'--}}
    {{--+ '<i class="fa fa-trash-o"></i>'--}}
    {{--+ '<input class="btn-icon edit-test" type="submit" value="" />'--}}
    {{--+ '</p>'--}}
    {{--+ '<input name="company_name_his[]" class="lbl-company edit-test" value="' + company + '" />'--}}
    {{--+ '<input name="time_work[]" class="des-time edit-test" placeholder="" value="' + time + '" />'--}}
    {{--+ '<label>- Nhiệm vụ</label>'--}}
    {{--+ '<textarea name="des_his[]" class="textarea edit-test edit-test">' + des + '</textarea>'--}}
    {{--+ '</div>'--}}
    {{--$('#exper').append(html);--}}
    {{--}--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}--}}

    {{--function editEmployer(id) {--}}
    {{--var imgtag = document.getElementById("preview");--}}
    {{--$.ajax({--}}
    {{--type: 'GET',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/' + id,--}}
    {{--async: false,--}}
    {{--success: function (data) {--}}
    {{--console.log(data);--}}
    {{--if (data.success) {--}}
    {{--var formdetail = $("#form-detailEmp");--}}

    {{--if (!data.hasOwnProperty('work_history')) {--}}
    {{--$('#exper').find('.group-company').remove();--}}
    {{--}--}}
    {{--$.each(data, function (key, value) {--}}
    {{--if (value !== null) {--}}
    {{--if (key == "_id") {--}}
    {{--formdetail.find("#" + key).val(id);--}}
    {{--}--}}
    {{--else if (key == "url_image") {--}}
    {{--//                                        imgtag.src = value;--}}
    {{--formdetail.find('.upload-file-container').attr("style", 'background-image: url(' + value + ');');--}}
    {{--}--}}
    {{--else if (key == "work_history") {--}}
    {{--$('#exper').find('.group-company').remove();--}}
    {{--var countWorkHistory = value.length--}}
    {{--for (i = 0; i < countWorkHistory; i++) {--}}
    {{--var company = ((value[i][0].company_name !== 'undefined') ? value[i][0].company_name : '');--}}
    {{--var time = ((value[i][0].time !== 'undefined') ? value[i][0].time : '');--}}
    {{--var des = ((value[i][0].des !== 'undefined') ? value[i][0].des : '');--}}
    {{--var html = '<div class="group-company">'--}}
    {{--+ '<p class="btn-box btn-delete" style="display:none">'--}}
    {{--+ '<i class="fa fa-trash-o"></i>'--}}
    {{--+ '<input class="btn-icon edit-test" type="submit" value="" />'--}}
    {{--+ '</p>'--}}
    {{--+ '<input name="company_name_his[]" placeholder="Tên công ty" class="lbl-company edit-test" value="' + company + '" disabled="disabled"/>'--}}
    {{--+ '<input name="time_work[]" class="des-time edit-test" placeholder="Thời gian" value="' + time + '" disabled="disabled"/>'--}}
    {{--+ '<label>- Nhiệm vụ</label>'--}}
    {{--+ '<textarea name="des_his[]" class="textarea edit-test edit-test" placeholder="Mô tả công việc" disabled="disabled">' + des + '</textarea>'--}}
    {{--+ '</div>'--}}
    {{--$('#exper').append(html);--}}
    {{--}--}}
    {{--}--}}
    {{--else {--}}
    {{--formdetail.find("#" + key).val(value);--}}

    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}--}}

    {{--</script>--}}
    {{--<script>--}}
    {{--$(document).ready(function () {--}}

    {{--$("input.edit-test").attr("disabled", true);--}}
    {{--$('.upload-file-container input').css('display', 'none');--}}
    {{--$('#detailEmp .btn-edit').click(function () {--}}
    {{--$("input.edit-test").prop("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--$('.upload-file-container ').addClass('avatar');--}}


    {{--});--}}
    {{--$('.group-employee .link-edit').click(function () {--}}
    {{--$("input.edit-test").attr("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--$('#exper .btn-box').css('display', 'block');--}}
    {{--});--}}
    {{--return false;--}}
    {{--});--}}

    {{--$("#exper").delegate("click", ".group-company", function (event) {--}}
    {{--alert('111');--}}
    {{--});--}}
    {{--</script>--}}
    {{--<script>--}}
    {{--$(document).ready(function () {--}}
    {{--$("input.edit-test").attr("disabled", true);--}}
    {{--$('.upload-file-container input').css('display', 'none');--}}
    {{--$(".upload-file-container .employee-image-edit").change(function () {--}}
    {{--var fileExtension = ['jpg', 'png', 'gif'];--}}
    {{--if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {--}}
    {{--alert("Only formats are allowed : " + fileExtension.join(', '));--}}
    {{--return;--}}
    {{--}--}}
    {{--var id = $('#form-detailEmp').find('input[name="_id"]').val();--}}
    {{--var data = new FormData();--}}
    {{--data.append('img', $('.upload-file-container .employee-image-edit').prop('files')[0]);--}}
    {{--data.append('_id', id);--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/uploadimage',--}}
    {{--data: data,--}}
    {{--processData: false,--}}
    {{--contentType: false,--}}
    {{--success: function (data) {--}}
    {{--var img = data.data.url_image;--}}
    {{--$('#form-detailEmp').find('.upload-file-container').attr("style", '');--}}
    {{--//                            $('#form-detailEmp').find('.upload-file-container').attr("style", 'background-image: url('+img+');');--}}
    {{--if (data.result) {--}}
    {{--$('#form-detailEmp').find('.upload-file-container').attr("style", '');--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--});--}}
    {{--$(".upload-file .choose-file").change(function () {--}}
    {{--var fileExtension = ['xls', 'xlsx'];--}}
    {{--var empGroup = $(".group-employee");--}}
    {{--if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {--}}
    {{--alert("Only formats are allowed : " + fileExtension.join(', '));--}}
    {{--return;--}}
    {{--}--}}
    {{--var data = new FormData();--}}
    {{--data.append('file', $('.upload-file .choose-file').prop('files')[0]);--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/upload',--}}
    {{--data: data,--}}
    {{--processData: false,--}}
    {{--contentType: false,--}}
    {{--success: function (data) {--}}
    {{--empGroup.text('');--}}
    {{--for (i = 0; i < (data.data).length; i++) {--}}
    {{--console.log(data.data[i].address);--}}
    {{--var row = data.data[i]--}}
    {{--var id = ( ( row._id.$oid === null  ) ? '' : row._id.$oid);--}}
    {{--var ngaysinh = ( ( row.ngaysinh === null  ) ? '' : row.ngaysinh);--}}
    {{--var dept = ( ( row.dept === null  ) ? '' : row.dept);--}}
    {{--var name = ( ( row.name === null  ) ? '' : row.name);--}}
    {{--var res = ( ( row.res === undefined  ) ? '' : row.res);--}}
    {{--var image = ( ( row.url_image === null  ) ? '' : row.url_image);--}}
    {{--var html = '<div class="employee-item" data-id="' + id + '">'--}}
    {{--+ '<div class="img" style="background-image: url(' + image + ');"></div>'--}}
    {{--+ '<div class="content">'--}}
    {{--+ '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name" onclick="editEmployer(' + "'" + id + "'" + ')">' + name + '</a>'--}}
    {{--+ '<p>- Ngày sinh: ' + ngaysinh + '</p>'--}}
    {{--+ '<p>- Phòng ban: ' + dept + '</p>'--}}
    {{--+ '<p>- Chức vụ: ' + res + '</p>'--}}
    {{--+ '</div>'--}}
    {{--+ '<div class="checkbox checkbox-success ">'--}}
    {{--+ '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'--}}
    {{--+ '<label for="checkbox-' + id + '"></label>'--}}
    {{--+ '</div>'--}}
    {{--+ '<a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#detailEmp" onclick="editEmployer(' + "'" + id + "'" + ')"><i class="fa fa-pencil "></i></a>'--}}
    {{--+ '</div>'--}}
    {{--empGroup.append(html);--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--});--}}
    {{--$(".employee-image-edit").change(function () {--}}

    {{--var fileExtension = ['jpg', 'png', 'gif'];--}}
    {{--if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {--}}
    {{--alert("Only formats are allowed : " + fileExtension.join(', '));--}}
    {{--return;--}}
    {{--}--}}
    {{--var selectedFile = $('.employee-image-edit').prop('files')[0];--}}
    {{--var imgtag = document.getElementById("preview");--}}
    {{--var reader = new FileReader();--}}

    {{--var previewImage = $('.upload-file-container ');--}}


    {{--reader.readAsDataURL(selectedFile);--}}

    {{--var id = $('#form-detailEmp').find('input[name="_id"]').val();--}}
    {{--reader.onload = function (event) {--}}
    {{--//                        imgtag.src = event.target.result;--}}
    {{--//                        $('.upload-file-container').css('abc', 'display:none');--}}

    {{--$("[data-id=" + id + "] > .img").css('background-image', 'url(' + event.target.result + ')');--}}
    {{--};--}}
    {{--var employChild = $("[data-id=" + id + "] > .img");--}}

    {{--var data = new FormData();--}}

    {{--data.append('img', selectedFile);--}}

    {{--data.append('_id', id);--}}

    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/uploadimage',--}}
    {{--data: data,--}}
    {{--processData: false,--}}
    {{--contentType: false,--}}
    {{--success: function (data) {--}}
    {{--var img = data.data.url_image;--}}
    {{--img = img + "?hello=" + new Date().getTime();--}}
    {{--$(".upload-file-container").css('background-image', 'url(' + img + ')');--}}
    {{--//$(".upload-file-container").trigger("chosen:update");--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--});--}}
    {{--$('#detailEmp .btn-edit').click(function () {--}}
    {{--$("input.edit-test").attr("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--$('.upload-file-container ').addClass('avatar');--}}
    {{--});--}}
    {{--$('.group-employee .link-edit').click(function () {--}}
    {{--$("input.edit-test").attr("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--$('.upload-file-container ').addClass('avatar');--}}
    {{--});--}}
    {{--$('#detailEmp .btn-success').click(function () {--}}
    {{--var modal = $("#detailEmp");--}}
    {{--var form = $('form#form-detailEmp');--}}
    {{--var employerGroup = $(".group-employee");--}}
    {{--var img = employerGroup.attr('img-default');--}}
    {{--var name = form.find("#name");--}}
    {{--if (name.val() === "") {--}}
    {{--alert("Chưa nhập tên nhân viên");--}}
    {{--return;--}}
    {{--}--}}
    {{--var url = modal.attr('ajax-url');--}}
    {{--var data = $('form#form-detailEmp').serialize();--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--url: url,--}}
    {{--data: data,--}}
    {{--dataType: "json",--}}
    {{--success: function (getdata) {--}}
    {{--if (getdata.result) {--}}

    {{--var id = ( ( getdata.data._id.$oid === null  ) ? '' : getdata.data._id.$oid);--}}
    {{--var name = ( ( getdata.data.name === null  ) ? '' : getdata.data.name);--}}
    {{--var ngaysinh = ( ( getdata.data.ngaysinh === null  ) ? '' : getdata.data.ngaysinh);--}}
    {{--var dept = ( ( getdata.data.dept === null  ) ? '' : getdata.data.dept);--}}
    {{--var res = ( ( getdata.data.res === null  ) ? '' : getdata.data.res);--}}
    {{--var employerUpdate = $("[data-id=" + id + "]");--}}
    {{--employerUpdate.text('');--}}
    {{--var html =--}}
    {{--'<div class="img" style="background-image: url(' + getdata.data.url_image + ');"></div>'--}}
    {{--+ '<div class="content">'--}}
    {{--+ '<a href="javascript:void(0)" data-toggle="modal" data-target="#detailEmp" class="name" onclick="editEmployer(' + "'" + id + "'" + ')">' + name + '</a>'--}}
    {{--+ '<p>- Ngày sinh: ' + ngaysinh + '</p>'--}}
    {{--+ '<p>- Phòng ban: ' + dept + '</p>'--}}
    {{--+ '<p>- Chức vụ: ' + res + '</p>'--}}
    {{--+ '</div>'--}}
    {{--+ '<div class="checkbox checkbox-success ">'--}}
    {{--+ '<input id="checkbox-' + id + '" class="chkdelete" type="checkbox" name="_id[]" value="' + id + '">'--}}
    {{--+ '<label for="checkbox-' + id + '"></label>'--}}
    {{--+ '</div>'--}}
    {{--+ '<a href="javascript:void(0)" class="link-edit" data-toggle="modal" data-target="#detailEmp" onclick="editEmployer(' + "'" + id + "'" + ')"><i class="fa fa-pencil "></i></a>'--}}

    {{--employerUpdate.append(html);--}}
    {{--}--}}
    {{--else {--}}
    {{--console.log(getdata.error);--}}
    {{--$(".error-form").text("");--}}
    {{--$.each(getdata.error, function (key, value) {--}}
    {{--$(".error-form").append("<p>" + value + "</p>");--}}
    {{--});--}}

    {{--}--}}
    {{--$('#detailEmp').modal('hide');--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--});--}}
    {{--$('#detailEmp').on('hidden.bs.modal', function () {--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'block');--}}
    {{--$(".edit-test").prop("disabled", true);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'none');--}}
    {{--$('.upload-file-container ').removeClass('avatar');--}}
    {{--$('#exper .btn-box').css('display', 'none');--}}
    {{--})--}}
    {{--$("input[name=email]").focusout(function () {--}}
    {{--var email = $(this);--}}
    {{--if (email.val() == "") {--}}
    {{--email.next().text("");--}}
    {{--return;--}}
    {{--}--}}
    {{--if (!isValidEmailAddress(email.val())) {--}}
    {{--email.next().text("Email không hợp lệ");--}}
    {{--return;--}}
    {{--}--}}
    {{--$.ajax({--}}
    {{--type: 'GET',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/checkemail/' + email.val(),--}}
    {{--success: function (data) {--}}
    {{--if (data.email) {--}}
    {{--email.next().text("Email này đã được sử dụng");--}}
    {{--}--}}
    {{--else {--}}
    {{--email.next().text("");--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}).focusin(function () {--}}
    {{--var email = $(this);--}}
    {{--email.next().text("");--}}
    {{--});--}}
    {{--$("input[name=personal_id]").focusout(function () {--}}
    {{--var personal_id = $(this);--}}
    {{--if (personal_id.val() == "") {--}}
    {{--personal_id.next().text("");--}}
    {{--return;--}}
    {{--}--}}
    {{--$.ajax({--}}
    {{--type: 'GET',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/checkpersonal/' + personal_id.val(),--}}
    {{--success: function (data) {--}}
    {{--if (data.personal_id) {--}}
    {{--personal_id.next().text("CMND này đã được sử dụng");--}}
    {{--}--}}
    {{--else {--}}
    {{--personal_id.next().text("");--}}
    {{--}--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}).focusin(function () {--}}
    {{--var email = $(this);--}}
    {{--email.next().text("");--}}
    {{--});--}}
    {{--function isValidEmailAddress(emailAddress) {--}}
    {{--var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);--}}
    {{--return pattern.test(emailAddress);--}}
    {{--}--}}
    {{--});--}}
    {{--</script>--}}
    {{--<style>--}}
    {{--.error {--}}
    {{--position: absolute;--}}
    {{--}--}}
    {{--</style>--}}
    {{--<script type="text/javascript">--}}
    {{--$(function () {--}}
    {{--$("#form-detailEmp").validate({--}}
    {{--ignore: ":hidden:not(select)",--}}
    {{--rules: {--}}
    {{--name: "required",--}}
    {{--birthday: {--}}
    {{--required: true,--}}
    {{--},--}}
    {{--sex: {--}}
    {{--required: true,--}}
    {{--},--}}
    {{--phone: {--}}
    {{--digits: true,--}}
    {{--minlength: 10,--}}
    {{--},--}}
    {{--email: {--}}
    {{--required: true,--}}
    {{--email: true--}}
    {{--},--}}
    {{--cmnd: {--}}
    {{--required: true,--}}
    {{--digits: true,--}}
    {{--},--}}
    {{--},--}}
    {{--messages: {--}}
    {{--name: "Vui lòng nhập họ tên",--}}
    {{--birthday: "Vui lòng nhập ngày sinh",--}}
    {{--sex: "Vui lòng nhập giới tính",--}}
    {{--phone: {--}}
    {{--digits: "Giá trị phải là số ",--}}
    {{--minlength: "Số máy quý khách vừa nhập là số không có thực",--}}
    {{--},--}}
    {{--email: {--}}
    {{--required: "Vui lòng nhập Email",--}}
    {{--email: "Địa chỉ email không hợp lệ",--}}
    {{--},--}}
    {{--cmnd: {--}}
    {{--required: "Vui lòng nhập số chứng minh nhân dân",--}}
    {{--digits: "Giá trị phải là số ",--}}
    {{--},--}}
    {{--},--}}
    {{--submitHandler: function (form) {--}}
    {{--//code--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}
    {{--$(".edit-account").attr("disabled", true);--}}
    {{--$('#checkbox-active input:checkbox').change(function () {--}}
    {{--$(this).closest('.checkbox').children("input").attr('checked', true);--}}
    {{--if ($(this).closest('.checkbox').children("input").is(":checked")) {--}}
    {{--$(".edit-account").attr("disabled", false);--}}
    {{--$("#form-detailEmp").validate({--}}
    {{--ignore: ":hidden:not(select)",--}}
    {{--rules: {--}}
    {{--accountUser: {--}}
    {{--required: true,--}}
    {{--email: true--}}
    {{--},--}}
    {{--accountPass: {--}}
    {{--required: true,--}}
    {{--minlength: 8,--}}
    {{--},--}}
    {{--},--}}
    {{--messages: {--}}
    {{--accountUser: {--}}
    {{--required: "Vui lòng nhập Email",--}}
    {{--email: "Địa chỉ email không hợp lệ",--}}
    {{--},--}}
    {{--accountPass: {--}}
    {{--required: "Vui lòng nhập mật khẩu",--}}
    {{--minlength: "Số máy quý khách vừa nhập là số không có thực",--}}
    {{--},--}}
    {{--},--}}
    {{--submitHandler: function (form) {--}}
    {{--}--}}
    {{--});--}}
    {{--}--}}
    {{--else {--}}
    {{--$(".edit-account").attr("disabled", true);--}}
    {{--}--}}
    {{--});--}}
    {{--$(function () {--}}
    {{--$(".edit-test").attr("disabled", true);--}}
    {{--$("#checkbox-active input").attr("disabled", true);--}}
    {{--$('.upload-file-container input').css('visible', '0');--}}
    {{--$('#detailEmp .btn-edit').click(function () {--}}
    {{--$(".edit-test").attr("disabled", false);--}}
    {{--$("#checkbox-active input").attr("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--$('.upload-file-container ').addClass('avatar');--}}
    {{--});--}}
    {{--$('.group-employee .link-edit').click(function () {--}}
    {{--$(".edit-test").attr("disabled", false);--}}
    {{--$("#checkbox-active input").attr("disabled", false);--}}
    {{--$('#detailEmp .btn.btn-success').css('display', 'block');--}}
    {{--$('#detailEmp .pull-right .btn-box.btn-black ').css('display', 'none');--}}
    {{--$('.upload-file-container input').css('display', 'block');--}}
    {{--});--}}
    {{--if ((".upload-file .btn-icon").files.length > 0) {--}}
    {{--alert('Must Select any of your photo for upload!');--}}
    {{--}--}}
    {{--});--}}
    {{--// change image in detail box--}}
    {{--function getImg(evt) {--}}
    {{--var selectedFile = evt.target.files[0];--}}
    {{--var reader = new FileReader();--}}

    {{--var imgtag = document.getElementById("preview");--}}
    {{--imgtag.title = selectedFile.name;--}}

    {{--reader.readAsDataURL(selectedFile);--}}

    {{--var id = $('#form-detailEmp').find('input[name="_id"]').val();--}}
    {{--reader.onload = function (event) {--}}
    {{--imgtag.src = event.target.result;--}}
    {{--$("[data-id=" + id + "] > .img").css('background-image', 'url(' + event.target.result + ')');--}}
    {{--};--}}
    {{--var data = new FormData();--}}

    {{--data.append('img', evt.target.files[0]);--}}

    {{--data.append('_id', id);--}}
    {{--$.ajax({--}}
    {{--type: 'POST',--}}
    {{--dataType: "json",--}}
    {{--url: '/erp/employer/uploadimage',--}}
    {{--data: data,--}}
    {{--processData: false,--}}
    {{--contentType: false,--}}
    {{--success: function (data) {--}}
    {{--var img = data.data.url_image;--}}
    {{--}--}}
    {{--});--}}
    {{--return false;--}}
    {{--}--}}

    {{--</script>--}}
    <script src="{{ asset('js/employer.js') }}"></script>
    <style>
        .error {
            position: absolute;
        }
    </style>
@endsection