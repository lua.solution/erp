﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Login PITC Group</title>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1" />
    <link rel="icon" type="image/x-icon" href="images/logo_web.png" />

    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/login.style.css" rel="stylesheet" />
    <!--[if lt IE 9 ]>
        <script type="text/javascript">
            var html5Elem = ['header', 'nav', 'menu','video', 'section', 'article', 'aside', 'main', 'footer'];
            for (var i = 0; i < html5Elem.length; i++){
                document.createElement(html5Elem[i]);
            }
        </script>
        <![endif]-->

    </head>
    <body>
        <div class="main-wrapper">
            <main class="form-login">
                <h1 class="box-header">
                    <a href="http://polarisvietnam.com" title="website Polaris">
                        <img src="images/logo_polaris.png" title="login Polaris" alt="login Polaris" />
                    </a>
                </h1>
                <div class="box-content">
                <!--<p class="user">
                    <input type="text" name="login" id="login" placeholder="Tên đăng nhập...">
                </p>
                <p class="pass">
                    <input type="password" name="password" id="password" placeholder="Mật khẩu ..." />
                </p> -->
                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Tài khoản</label>

                        <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                            @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Mật khẩu</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn-login">
                                Đăng nhập
                            </button>

                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                Quên mật khẩu?
                            </a>
                        </div>
                    </div>
                </form>

                <!--form change password -->
                <div class="form-forget">
                    <div class="box-forget">
                        <h3 class="title-forget">Tìm tài khoản của bạn</h3>
                        <p class="forget">
                            <input type="text" name="forget" id="forget" placeholder="Vui lòng nhập email khôi phục mật khẩu..." />
                        </p>
                        <input type="submit" class="btn-login" name="login" id="login" value="Xác nhận" />
                    </div>
                </div>

            </div>
            <div id="mask-popup"></div>
        </main>
        <footer class="footer">Copyright @ 2017 by Polaris VietNam</footer>
    </div>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script>
        $(document).ready(function () {
            var height = ($('.main-wrapper').height() - 160) / 2;
            $('.form-forget').css({ 'top': height });

            $('a.forget-password').click(function () {
                $('.form-forget').fadeIn(400);
                $('#mask-popup').css({ 'display': 'block' });
            });
            $("#mask-popup").click(function () {
                $('.form-forget').fadeOut(100);
                $('#mask-popup').css({ 'display': 'none' });
            });
        });
        $(window).resize(function () {
            var height = ($('.main-wrapper').height() - 160) / 2;
            $('.form-forget').css({ 'top': height });
        });
    </script>
</body>

</html>
