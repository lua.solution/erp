<?php
/*
 * Link to mainpage polaris - update later
 */
Route::get('/', 'HomeController@index')->name('home');

/*
 * Authencation route for main DB - don't care about them
 */
Auth::routes();
Route::post('logout', 'Auth\LogoutController@logout')->name('logout');
Route::get('logout', 'Auth\LogoutController@logout');
Route::post('change', 'Auth\ResetPasswordController@changePass')->name('changePass');

/*
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
*/
/* Testing route, don't care it - NAD
*/
Route::post('ajax', 'MongoController@index');
Route::get("ajax", function () {
    return view("page");
});
/*
 * Setting route for multi tenancy
 */
Route::group(['prefix' => 'setting'], function () {
    Route::get('', 'SettingController@index')->name('setting');
});
/*
 * All route for ERP system, place all here
 */
Route::group(['prefix' => 'erp', 'middleware' => 'logged'], function () {
    Route::get('', 'ERP\ErpController@index')->name('erp');
    // Departments
    Route::group(['prefix' => 'department'], function () {
        Route::get('', 'ERP\Department\DepartmentController@index')->name('erp.department.index');
        Route::post('create', 'ERP\Department\DepartmentController@store')->name('erp.department.create.post');
        Route::post('update', 'ERP\Department\DepartmentController@update')->name('erp.department.edit.post');
        Route::post('destroy', 'ERP\Department\DepartmentController@destroy')->name('erp.department.destroy');
        Route::get('{id}', 'ERP\Department\DepartmentController@show')->name('erp.department.show');
        Route::delete('delete/{id}', 'ERP\Department\DepartmentController@destroy')->name('erp.department.destroy');
    });
    // Employee
    Route::group(['prefix' => 'employer'], function () {
        Route::get('checkemail/{email}', 'ERP\Employer\EmployerController@checkEmail')->name('erp.employer.checkemail');
        Route::get('checkpersonal/{personal_id}', 'ERP\Employer\EmployerController@checkPersonalId')->name('erp.employer.personalid');
        Route::get('download', 'ERP\Employer\EmployerController@download')->name('erp.employer.download');
        Route::get('', 'ERP\Employer\EmployerController@index')->name('erp.employer.index');
        Route::get('create', 'ERP\Employer\EmployerController@create')->name('erp.employer.create');
        Route::post('createstaffout', 'ERP\Employer\EmployerController@createStaffOut')->name('erp.employer.create.staffout');
        Route::post('create', 'ERP\Employer\EmployerController@store')->name('erp.employer.create.post');
        Route::post('update', 'ERP\Employer\EmployerController@update')->name('erp.employer.edit.post');
        Route::post('upload', 'ERP\Employer\EmployerController@upload')->name('erp.employer.upload');
        Route::post('uploadimage', 'ERP\Employer\EmployerController@uploadImage')->name('erp.employer.uploadimage');
        Route::post('check', 'ERP\Employer\EmployerController@checkEmployer')->name('erp.employer.check');
        Route::post('getemployer', 'ERP\Employer\EmployerController@getEmployer')->name('erp.employer.getemployer');
        Route::get('{id}', 'ERP\Employer\EmployerController@show')->name('erp.employer.show');
        Route::post('destroy', 'ERP\Employer\EmployerController@destroy')->name('erp.employer.destroy');
    });
    //Machine
    Route::group(['prefix' => 'machine'], function () {
        Route::get('', 'ERP\Machine\MachineController@index')->name('erp.machine.index');
        Route::post('create', 'ERP\Machine\MachineController@store')->name('erp.machine.create.post');
    });
    //Material
    Route::group(['prefix' => 'material'], function () {
        Route::get('', 'ERP\Material\MaterialController@index')->name('erp.material.index');
        Route::post('create', 'ERP\Material\MaterialController@store')->name('erp.material.create.post');
    });
    //Document
    Route::group(['prefix' => 'document'], function () {
        Route::get('', 'ERP\Document\DocumentController@index')->name('erp.document.index');
        Route::post('create', 'ERP\Document\DocumentController@store')->name('erp.document.create.post');
    });
    /* Project */
    Route::get('projects', 'ERP\Project\ProjectsController@getRenderProject')->name('erp.project');
    Route::post('project/create', 'ERP\Project\ProjectsController@postCreateProject')->name('erp.project.createProject');
    Route::get('project/delete/{id}', 'ERP\Project\ProjectsController@getMoveToTrashProject')->name('erp.project.deleteProject');
    Route::get('project/search', 'ERP\Project\ProjectsController@getSearchProject')->name('erp.project.searchProject');

    // Project Info
    Route::get('project/info/{id}', 'ERP\Project\ProjectInfoController@getRenderInfoProject')->name('erp.project.getRenderInfoProject');
    // Project Work
    Route::get('project/work/{id}', 'ERP\Project\ProjectWorkController@getRenderWorkProject')->name('erp.project.getRenderWorkProject');
    Route::post("project/work/add-group-task", 'ERP\Project\ProjectWorkController@postAddGroupTask')->name('erp.project.postAddGroupTask');
    // Project Work List
    Route::get('project/worklist/{id}', 'ERP\Project\ProjectWorkListController@getRenderWorkListProject')->name('erp.project.getRenderWorkListProject');
    Route::post('project/worklist/more', 'ERP\Project\ProjectWorkListController@getMoreWorkListProject')->name('erp.project.getMoreWorkListProject');
    Route::post('project/worklist/upload', 'ERP\Project\ProjectWorkListController@workListUpload')->name('erp.project.workListUpload');
    Route::get('project/worklist/download/{id}', 'ERP\Project\ProjectWorkListController@workListDownload')->name('erp.project.workListDownload');
    Route::post('project/worklist/addchild', 'ERP\Project\ProjectWorkListController@workListAddChild')->name('erp.project.workListAddChild');

    Route::get('project/workstaff/{id}', 'ERP\Project\ProjectWorkStaffController@getRenderWorkStaffProject')->name('erp.project.getRenderWorkStaffProject');
    Route::post('project/workstaff/upload', 'ERP\Project\ProjectWorkStaffController@workStaffUpload')->name('erp.project.workStaffUpload');
    Route::get('project/workstaff/download/{id}', 'ERP\Project\ProjectWorkStaffController@workStaffDownload')->name('erp.project.workStaffDownload');
    // Project Staff
    Route::get('project/staff/{id}', 'ERP\Project\ProjectStaffController@getRenderStaffProject')->name('erp.project.getRenderStaffProject');
    Route::post('project/staff/add', 'ERP\Project\ProjectStaffController@addStaffToProject')->name('erp.project.addStaffToProject');
    Route::post('project/staff/update', 'ERP\Project\ProjectStaffController@updateStaffToProject')->name('erp.project.updateStaffToProject');
    Route::post('project/staff/delete', 'ERP\Project\ProjectStaffController@deleteStaff')->name('erp.project.deleteStaff');


    Route::get('project/liststaff', 'ERP\Project\ProjectStaffController@comboboxStaffProject')->name('erp.project.liststaff');
    /* End Project */


    Route::get("eml/manage", function () {
        return view("erp.employer.manage");
    });

    /**
     * Ajax Route
     */
    Route::post('/ajax/project/edit', 'ERP\Project\AjaxController@getEditProject')->name('ajax.erp.project.editProject');
    Route::post('/ajax/project/work/add-group-task', 'ERP\Project\AjaxController@postAddGroupTaskProject')->name('ajax.erp.project.work.addGroupTask');
    Route::post('/ajax/project/work/edit-group-task', 'ERP\Project\AjaxController@postEditGroupTaskProject')->name('ajax.erp.project.work.postEditGroupTask');
    Route::post('/ajax/project/work/delete-group-task', 'ERP\Project\AjaxController@postDeleteGroupTaskProject')->name('ajax.erp.project.work.postDeleteGroupTask');
    Route::post('/ajax/project/work/add-task-to-group', 'ERP\Project\AjaxController@postAddTaskToGroupProject')->name('ajax.erp.project.work.postAddTaskToGroupProject');
    Route::post('/ajax/project/work/delete-task', 'ERP\Project\AjaxController@postDeleteTaskProject')->name('ajax.erp.project.work.postDeleteTaskProject');
    Route::post('/ajax/project/work/request-edit-task', 'ERP\Project\AjaxController@postRequestEditTask')->name('ajax.erp.project.work.postEditTask');
    Route::post('/ajax/project/work/edit-task', 'ERP\Project\AjaxController@postEditTaskProject')->name('ajax.erp.project.work.postEditTaskProject');

});