<?php

namespace App\ERPModels;


use App\Helpers\Helper;
use DB;
use Jenssegers\Mongodb\Eloquent\Model;

use Session;
class ProjectStaffs extends Model
{
    //
	protected $collection = 'project_staffs';
	protected $connection = 'mongodb';

	protected $fillable = [
		'staffs_id',
		'projects_id',
		'staffs_name',
		'staffs_email',
		'staffs_image',
		'staffs_phone',
		'int',
		'out',
		'groups',
		'tasks',
		'reports',
		'status',
	];
    public function __construct()
    {
        $dbname = Session::get('dbname');
        $this->connection = $dbname;
    }

    /**
     * @author Long
     * @description getProjectStaff
     * @param array $data
     * @return array
     */
    public function __getProjectStaff($data = [])
    {
        $id = isset($data['_id']) ? $data['_id'] : '';
        return DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->pluck('project_staff');
    }

    /**
     * @author Long
     * @description them nhan su vao du an
     * @param array $data
     * @return array
     */
    public function __addStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';

        $project_staff = [];
        $findByIdProject = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->first();
        if (!$findByIdProject) {

            return [];
        } else {

            if (isset($data['addName'])) {

                $project_staff['id'] = isset($data['addName']) ? $data['addName'] : '';
                $project_staff['in'] = isset($data['addStartDate']) ? $data['addStartDate'] : '';
                $project_staff['out'] = isset($data['addEndDate']) ? $data['addEndDate'] : '';
                $project_staff['action'] = 1;
                $project_staff['res'] = isset($data['addJob']) ? $data['addJob'] : '';
                $project_staff['note'] = isset($data['addDes']) ? $data['addDes'] : '';

            }

        }
        DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->push('project_staff', $project_staff);
    }

    /**
     * @author Long
     * @description them nhan su vao du an
     * @param array $data
     * @return array
     */
    public function __updateStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';

        $project_staff = [];
        $findByIdProject = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->first();
        if (!$findByIdProject) {

            return [];
        } else {
            DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->update(['project_staff' => $data['project_staff']]);
        }

//            ->update([['project_staff.id' => $project_staff['id']],'set' => ['project_staff.note' => '1111']]);
//        DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->update(array('project_staff.id' => $project_staff['id']), array('project_staff.note' => '327483274897'));
    }
    public function __checkProjectStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';
        $staff_id = isset($data['addName']) ? $data['addName'] : '';

        return DB::connection($data['dbname'])->collection('projects')->find(array('_id','project_staff' => array('id' => $staff_id)));
    }
    public function __deleteStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';
        $staff_id = isset($data['staff_id']) ? $data['staff_id'] : '';
//        $project_staff = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->find('project_staff.');
//        dd($project_staff);
        return DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->pull('project_staff', ['id' => $staff_id]);

    }

    public function __updateDataStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';

        $project_staff = [];
        $findByIdProject = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->first();
        if (!$findByIdProject) {

            return [];
        } else {
            $project_staff['id'] = isset($data['addName']) ? $data['addName'] : '';
            $project_staff['in'] = isset($data['addStartDate']) ? $data['addStartDate'] : '';
            $project_staff['out'] = isset($data['addEndDate']) ? $data['addEndDate'] : '';
            $project_staff['action'] = 1;
            $project_staff['res'] = isset($data['addJob']) ? $data['addJob'] : '';
            $project_staff['note'] = isset($data['addDes']) ? $data['addDes'] : '';
            $array_staff = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->pluck('project_staff')->toArray();


            $index = '';
            $count = 0;
            foreach ($array_staff[0] as $key => $value){

                $count++;
                if($value['id']==$project_staff['id']){
                    $index = $key;
                }
            }
            $project_staff['action'] = $index==0 ? 0 : 1;

            DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->update(array('$set' => array('project_staff.'.$index => $project_staff)));
        }

//            ->update([['project_staff.id' => $project_staff['id']],'set' => ['project_staff.note' => '1111']]);
//        DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->update(array('project_staff.id' => $project_staff['id']), array('project_staff.note' => '327483274897'));
    }
}
