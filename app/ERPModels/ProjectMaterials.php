<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMaterials extends Model
{
    //
	protected $collection = 'project_materials';
	protected $connection = 'mongodb';

	protected $fillable = [
		'materials_id',
		'projects_id',
		'materials_name',
		'int',
		'out',
		'groups',
	    'tasks',
	    'reports',
	    'status',
	];
}
