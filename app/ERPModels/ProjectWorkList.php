<?php

namespace App\ERPModels;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use DB;
use Session;

class ProjectWorkList extends Model
{
    public function create($data = [])
    {
        $data['status'] = isset($data['status']) ? $data['status'] : 0;

        return DB::connection(Session::get('dbname'))
            ->collection('tasks')
            ->insertGetId($data);
    }

    public function getTasklist($data = [])
    {
        $dbname = $data['dbname'];
        $projectId = $data['idProject'];

        return DB::connection($dbname)
            ->collection('tasks')
            ->where('project_id', $projectId)
            ->get();
    }

    public function getTasklistByStaff($data = [])
    {
        $dbname = $data['dbname'];
        $idStaff = $data['idStaff'];

        $res = DB::connection($dbname)
            ->collection('tasks')
            ->where('task_staff.staff_id', $idStaff)
            ->select('name', 'start', 'end', 'unit', 'quantity')
            ->get()->toArray();
        return $res;
    }

    public function getNameById($id) {
        return DB::connection(Session::get('dbname'))->collection('projects')->where('_id', $id)->pluck('name')->first();
    }
}