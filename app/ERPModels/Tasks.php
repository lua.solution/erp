<?php

namespace App\ERPModels;

use App\Helpers\Helper;
use DB;
use Jenssegers\Mongodb\Eloquent\Model;
use Session;

class Tasks extends Model
{
	//
	protected $collection = 'tasks';
	protected $connection = 'mongodb';

	protected $fillable = [
		'project_id',
		'group_task_id',
		'task_staff',
		'name',
		'start',
		'end',
		'unit',
		'quantity',
		'unit_price',
		'parent_id',
		'task_function',
		'desc',
		'status',
	];

	public function __construct()
	{
		$dbname = Session::get('dbname');
		$this->connection = $dbname;
	}

	public function __createTask($data = [])
	{
		$data['status'] = isset($data['status']) ? $data['status'] : 0;
		if (!isset($data['idProject']) && !isset($data['idGroupTask'])) {
			return [];
		} else {
			$idProject = $data['idProject'];
			$idGroupTask = $data['idGroupTask'];
			$data['desc'] = isset($data['desc']) ? $data['desc'] : '';
			if (isset($data['nameJob'])) {
				$nameJob = $data['nameJob'];
				$this->name = $nameJob;
			}
			if (isset($data['startDateJob'])) {
				$startDateJob = $data['startDateJob'];
				$this->start = $startDateJob;
			}
			if (isset($data['endDateJob'])) {
				$endDateJob = $data['endDateJob'];
				$this->end = $endDateJob;
			}
			if (isset($data['unit'])) {
				$unit = $data['unit'];
				$this->unit = $unit;
			} else {
				$this->unit = '';
			}
			if (isset($data['quantity'])) {
				$quantity = $data['quantity'];
				$this->quantity = $quantity;
			} else {
				$this->quantity = [];
			}
			if (isset($data['unit_price'])) {
				$unit_price = $data['unit_price'];
				$this->unit_price = $unit_price;
			} else {
				$this->unit_price = 0;
			}
			if (isset($data['parent_id'])) {
				$parent_id = $data['parent_id'];
				$this->parent_id = $parent_id;
			} else {
				$this->parent_id = '';
			}
			if (isset($data['task_function'])) {
				$task_function = $data['task_function'];
				$this->task_function = $task_function;
			} else {
				$this->task_function = '';
			}
			if (isset($data['taskStaff'])) {
				$taskStaff = $data['taskStaff'];
				$this->task_staff = $taskStaff;
			} else {
				$this->task_staff = [];
			}

			$this->project_id = $idProject;
			$this->group_task_id = $idGroupTask;
			$this->desc = $data['desc'];
			$this->status = $data['status'];

			return $this->save();
		}
	}

	public function __updateTask($data = [])
	{
		$id = isset($data['_id']) ? $data['_id'] : '';
		$find = $this::find($id);
		if (!$find) {
			return [];
		} else {
			$data['status'] = isset($data['status']) ? $data['status'] : 0;
			if (isset($data['idProject'])) {
				$idProject = $data['idProject'];
				$find->project_id = $idProject;
			}
			if (isset($data['idGroupTask'])) {
				$idGroupTask = $data['idGroupTask'];
				$find->group_task_id = $idGroupTask;
			}
			$data['desc'] = isset($data['desc']) ? $data['desc'] : '';
			if (isset($data['nameJob'])) {
				$nameJob = $data['nameJob'];
				$find->name = $nameJob;
			}
			if (isset($data['startDateJob'])) {
				$startDateJob = $data['startDateJob'];
				$find->start = $startDateJob;
			}
			if (isset($data['endDateJob'])) {
				$endDateJob = $data['endDateJob'];
				$find->end = $endDateJob;
			}
			if (isset($data['unit'])) {
				$unit = $data['unit'];
				$find->unit = $unit;
			} else {
				$find->unit = '';
			}
			if (isset($data['quantity'])) {
				$quantity = $data['quantity'];
				$find->quantity = $quantity;
			} else {
				$find->quantity = [];
			}
			if (isset($data['unit_price'])) {
				$unit_price = $data['unit_price'];
				$find->unit_price = $unit_price;
			} else {
				$find->unit_price = 0;
			}
			if (isset($data['parent_id'])) {
				$parent_id = $data['parent_id'];
				$find->parent_id = $parent_id;
			} else {
				$find->parent_id = '';
			}
			if (isset($data['task_function'])) {
				$task_function = $data['task_function'];
				$find->task_function = $task_function;
			} else {
				$find->task_function = '';
			}
			if (isset($data['taskStaff'])) {
				$taskStaff = $data['taskStaff'];
				$find->task_staff = $taskStaff;
			} else {
				$find->task_staff = [];
			}

			$find->desc = $data['desc'];
			$find->status = $data['status'];

			return $find->save();
		}
	}

	public function __deleteTempTask($data = [])
	{
		$dbname = Session::get('dbname');
		$id = isset($data['_id']) ? $data['_id'] : '';

		return DB::connection($dbname)->collection('tasks')->where('_id', $id)->update(['status' => 1]);
	}
}
