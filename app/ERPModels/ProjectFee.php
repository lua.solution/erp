<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectFee extends Model
{
    //
	protected $collection = 'project_fee';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'project_id',
		'start',
		'finish',
		'name',
		'with',
		'unit',
		'unit_price',
		'qua',
		'group',
		'status',
	];
}
