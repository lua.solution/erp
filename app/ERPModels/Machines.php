<?php

namespace App\ERPModels;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Session;
use DB;

class Machines extends Model
{
    //
	protected $collection = 'machines';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'name',
		'unit',
		'quantity',
		'unit_price',
		'buy_date',
		'used',
		'status',
        'note'
	];

    public function __construct()
    {
        $dbname = Session::get('dbname');
        $this->connection = $dbname;
    }

    /*
     * @Author: Bao Long
     * @Description: danh sach machine
     * @var array data
     */
    public function __getList($data = []){
        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        if(!empty($keyword)){
            return DB::connection($data['dbname'])->collection('machines')->where($arr)->where('name', 'LIKE', "%$keyword%")->get();
        }
        return DB::connection($data['dbname'])->collection('machines')->where($arr)->get();
    }

    /*
     * @Author: Bao Long
     * @Description: them 1 machine
     * @var array data
     */
    public function __create($data = []){
        $data['status'] = isset($data['status']) ? $data['status'] : 1;
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }


        return DB::connection($data['dbname'])->collection('machines')->insertGetId($arr);
    }

    /*
     * @Author: Bao Long
     * @Description: update 1 machine
     * @var array data
     */
    public function __update($data = []){
        $id = isset($data['_id']) ? $data['_id'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        return DB::connection($data['dbname'])->collection('machines')->where('_id',$id)->update($arr);

    }

    /*
     * @Author: Bao Long
     * @Description: lay 1 machine
     * @var array data
     */
    public function getMachine($data =[]){

        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        if(isset($data['_id'])){
            return DB::connection($data['dbname'])->collection('machines')->where($arr)->first();
        }
    }
}
