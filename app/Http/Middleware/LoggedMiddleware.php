<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Config;
use Illuminate\Support\Facades\DB;

class LoggedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::has('dbname') || Session::get('dbname') == "N/A")
            return redirect()->route("home");

        $dbname = Session::get('dbname');
        $nameKey = $dbname;
        Config::set('database.connections.' . $nameKey, array(
        'driver'    => 'mongodb',
        'host'      => '192.168.0.116',
        'port'      => 27017,   
        'database'  => $dbname,
        ));
        DB::setDefaultConnection($nameKey);
        return $next($request);
    }
}
