<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Schema;
use Config;
use Illuminate\Support\Facades\DB;

class DBController extends Controller
{
     public function index(Request $request)
    {
    	$posts = DB::table('admins')->where('_id', Session::get('user')['_id'])->pluck('dbname');
        return view('setting.db.index', compact('posts'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('setting.db.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $dbname = Session::get('user')['username'].'-'.$request->dbname;

        Config::set('database.connections.' . $dbname, array(
        'driver'    => 'mongodb',
        'host'      => '127.0.0.1',
        'port'      => 27017,   
        'database'  => $dbname,
        ));

        Schema::connection($dbname)->create('users', function($collection) {		});
        
        DB::collection('admins')
            ->where('_id', Session::get('user')['_id'])
            ->push('dbname', array($dbname), true);
        
        return redirect()->route('db.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        
        $post = DB::table('posts')->where('_id', $id)->first();
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        Session::put('dbname', $id);    
        return view('sldb');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $post = Post::findOrFail($id);
        $post->update($requestData);

        Session::flash('flash_message', 'Post updated!');

        return redirect('profile/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Post::destroy($id);

        Session::flash('flash_message', 'Post deleted!');

        return redirect('profile/posts');
    }
}
