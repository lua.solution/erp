<?php

namespace App\Http\Controllers\ERP\Document;


use Session;
use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ERPModels\Documents;
use App\ERPModels\Project;
use App\Department;

class DocumentController extends Controller
{
    //
    public function index(Request $request)
    {
        $dbname = Session::get('dbname');
        $data = [];
        $modelDocument = new Documents;
        $modelProject = new Project;
        $modelDepartment = new Department;

        $argPro = [
            'dbname' => $dbname,
            'status' => 0
        ];

        $projects = $modelProject->__getList($argPro)->toArray();
        $data['projects'] = $projects;
        $argDep = [
            'dbname' => $dbname,
            'status' => 1
        ];

        $departments = $modelDepartment->getList($argDep)->toArray();
        $data['departments'] = $departments;

        $argDocTo = [
            'dbname' => $dbname,
            'status' => 0,
            'group_name' => "0"
        ];
        $documents_to = $modelDocument->__getList($argDocTo)->toArray();
        $new_documents_to = [];
        foreach($documents_to as $key => $value){
            $new_documents_to[$key] = $value;
            if(isset($value['file'])){
                $new_documents_to[$key]['url_file'] = !empty($value['file']) ? url($value['file']) : '';
            }
            if(!empty($value['project_id'])){
                $project_name = $modelProject->getNameById($value['project_id']);
                $new_documents_to[$key]['project'] = $project_name ? $project_name : '';
            }
            else{
                $new_documents_to[$key]['project'] = '';
            }
            $department_name = '';
            if(count($value['departments'])>0){
                foreach($value['departments'] as $dep){
                    $department = $modelDepartment->getById(['dbname' => $dbname,'_id'=> $dep]);
                    if($department){

                        $department_name .= $department['name'] . ',';
                    }
                }

            }
            $department_name =trim($department_name,',');
            $new_documents_to[$key]['department_rela'] = $department_name;
        }
        $data['documents_to'] = $new_documents_to;

        $argDocFrom = [
            'dbname' => $dbname,
            'status' => 0,
            'group_name' => "1"
        ];

        $documents_from = $modelDocument->__getList($argDocFrom)->toArray();
        $data['documents_from'] = $documents_from;
        $argDoc = [
            'dbname' => $dbname,
            'status' => 0,
            'group_name' => "2"
        ];

        $documents = $modelDocument->__getList($argDoc)->toArray();
        $data['documents'] = $documents;
        $data['departments'] = $departments;
        return view('erp.document.index',$data);
    }

    public function store(Request $request)
    {

        $file_url = '';
        if($request->hasFile("createFile")){
            $file= $request->file("createFile");
            $filename = $file->getClientOriginalName();
            $filename = str_random(12).$filename;
            $extension = $file->getClientOriginalExtension();
            $destinationPath = ('file/'.Session::get('dbname').'/');
            $request->file("createFile")->move($destinationPath,$filename);
            $file_url = $destinationPath.$filename;

        }
        $requestData = $request->all();
        $result = [
            'success' => true,
            'data' => ''
        ];
        $dbname = Session::get('dbname');
        $modelDocument = new Documents;
        $modelDepartment = new Department;
        $modelProject = new Project;
        $requestData = $request->all();
//        dd($requestData);
        $requestData['dbname'] = $dbname;

        $rules = [
            'titleDoc' => 'required',
            'numDoc' => 'required|unique:'.$dbname.'.documents,code',

        ];
        $validator = \Validator::make($requestData, $rules);
        if ($validator->fails()) {
            return response()->json(array('result' => false,'error' => $validator->errors()),200);

        }
        $data = [];
        $data['file'] = $file_url;
        $data['code'] = isset($requestData['numDoc']) ? $requestData['numDoc'] : '';
        $data['project_id'] = isset($requestData['projects']) ? $requestData['projects'] : '';
        $data['name'] = isset($requestData['titleDoc']) ? $requestData['titleDoc'] : '';
        $data['date'] = isset($requestData['dateDoc']) ? $requestData['dateDoc'] : '';
        $data['departments'] = isset($requestData['departments']) ? $requestData['departments'] : [];
        $data['desc'] = isset($requestData['desc']) ? $requestData['desc'] : '';
        $data['note'] = isset($requestData['note']) ? $requestData['note'] : '';
        $data['group_name'] = isset($requestData['group_name']) ? $requestData['group_name'] : "0";
        $department_name = '';
        if(count($data['departments'])>0){
            foreach($data['departments'] as $dep){
                $department = $modelDepartment->getById(['dbname' => $dbname,'_id'=> $dep]);
                if($department){

                    $department_name .= $department['name'] . ',';
                }
            }

        }
        $data['department_name'] = $department_name;
        if(!empty($data['project_id'])){
            $project_name = $modelProject->getNameById($data['project_id']);
            $data['project'] = $project_name ? $project_name : '';
        }
        else{
            $data['project'] = '';
        }

        switch ($data['group_name']){
            case "0":
                $data['to'] = isset($requestData['to']) ? $requestData['to'] : '';
                break;
            case "1":
                $data['from'] = isset($requestData['from']) ? $requestData['from'] : '';
                break;

        }
        $data['dbname'] = $dbname;
        $result = $modelDocument->__create($data);
        if(!empty($file_url)){
            $data['file_url'] = url($file_url);
        }
        $data['_id'] = $result;


//        return $result;
        return response()->json(array('result' => true,'data' => $data),200);
    }

}