<?php

namespace App\Http\Controllers\ERP\Employer;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Staff;
use App\Department;
use Illuminate\Http\Request;
use Session;
use Config;
use App;
use PHPExcel;
use PHPExcel_IOFactory;

class EmployerController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {

        $dbname = Session::get('user')['dbname'];
        $nameKey = $dbname;

        $modelDepartment = new Department;
        $modelStaff = new Staff;
        $keyword = $request->get('search');
        $depid = $request->get('depid');

        $args = [
            'dbname' => $dbname,
            'keyword' => $keyword,
            'status' => 1,
            'is_out' => 0,
//            'departments_id' => $depid
        ];
        if(!empty($depid)) {
            $args['departments_id'] = $depid;
            $data['depid'] = [
                'id' => $depid,
                'name' => $modelDepartment->getDepartment(['dbname' => $dbname, '_id' => $depid])['name']
            ];

        }
        $staffs = $modelStaff->getList($args);
        $staffs = $staffs->toArray();
        $arr = array();
        foreach ($staffs as $key => $value){
            $arr[$key]= $value;
            $argDept = [
                '_id' => isset($value['departments_id']) ? $value['departments_id'] : "",
                'dbname' => $dbname
            ];
            $dept = $modelDepartment->getById($argDept);
            $arr[$key]['dept'] = $dept ? $dept['name'] : "";
        }

        $staffs = $arr;
        $data['staffs'] = $staffs;


        $departments = $modelDepartment->getList($args);
        $data['departments'] = $departments;

        return view('erp.employer.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $dbname = Session::get('dbname');
        $nameKey = $dbname;
        $modelDepartment = new Department;
        $args['dbname'] = $dbname;
        $args['parent_id'] = "0";
        $department = $modelDepartment->getParent($args);
        $data['department'] = department;
        return View('erp.employer.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $result = [
            'success' => true,
            'data' => ''
        ];
//        return response()->json($result);
        $dbname = Session::get('dbname');
        $nameKey = $dbname;

        $modelDepartment = new Department;
        $modelStaff = new Staff();

        $requestData = $request->all();

//        if(!empty($requestData['ngaysinh'])){
//            $date = new \DateTime($requestData['ngaysinh']);
//            $requestData['ngaysinh'] = $date->format('d/m/Y');
//        }
//        return $requestData;
        $requestData['dbname'] = $dbname;

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:'.$dbname.'.staffs,email',
            'personal_id' => 'required|unique:'.$dbname.'.staffs,personal_id',
            'img' => 'mimes:jpeg,jpg,png,gif'
        ];
        $validator = \Validator::make($requestData, $rules);



        if ($validator->fails()) {
            $error = [];
//            foreach ($validator->errors()->toArray() as $key => $value){
//                $error[$key] = $value[0];
//            }
            return response()->json(array('result' => false,'error' => $validator->errors()),200);
//            $data= ['error' => $validator->errors()];
//            return response()->json($data);
//            return redirect()->back()->withInput()->withErrors($validator->errors());
        }

        if($request->hasFile("img")){
            $requestData['image'] = true;
        }
        $requestData['is_out'] = 0;
        $result = $modelStaff->create($requestData);
//        $data = [];
//        $data = [
//            '_id' => $result,
//            'data'=> $requestData
//        ];

        $requestData['_id'] = $result;
        if($request->hasFile('img')) $img = $request->file("img")->move('img_empl/'.Session::get('dbname'), $result.'.jpg');
        $argDept = [
            '_id' => $requestData['departments_id'],
            'dbname' => $dbname
        ];
        $dept = $modelDepartment->getById($argDept);
        $requestData['dept'] = $dept ? $dept['name'] : "";
        $requestData['url_image'] = isset($requestData['image']) ? url('img_empl/'.Session::get('dbname').'/'.$result.'.jpg'): url('img_empl/default.jpg');
        $requestData['result'] = true;
//        return $result;
        return response()->json(array('result' => true,'data' => $requestData),200);
        if($result){
            Session::flash('flash_message', 'Employer added!');
        }

        return redirect('erp/employer/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $modelStaff = new Staff;
        $dbname = Session::get('dbname');
        $modelDepartment = new Department;
        $argDep = [
            '_id' => $id,
            'dbname' => $dbname
        ];
        $staff = $modelStaff->getById($argDep);

        if(!$staff){
            return response()->json(['success' => false]);
        }
        $staff['success'] = true;
        $staff['url_image'] = isset($staff['image']) ? url('img_empl/'.Session::get('dbname').'/'.$staff['_id'].'.jpg'): url('img_empl/default.jpg');
        return response()->json($staff,200);
//        return view('erp.department.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $dbname = Session::get('dbname');
        $nameKey = $dbname;

        $modelStaff = new Staff;

        $modelDepartment = new Department;
        $args['dbname'] = $dbname;
        $args['parent_id'] = "0";
        $departmentParent = $modelDepartment->getParent($args);
        $data['departmentParent'] = $departmentParent;

        $argDep = [
            '_id' => $id,
            'dbname' => $dbname
        ];
        $data['staff'] = $modelStaff->getById($argDep);

        return view('erp.department.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {

        $requestData = $request->all();
        $dbname = Session::get('dbname');
//        if(!empty($requestData['ngaysinh'])){
//            $date = new \DateTime($requestData['ngaysinh']);
//            $requestData['ngaysinh'] = $date->format('d/m/Y');
//        }
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:'.$dbname.'.staffs,email,'.$requestData['_id'].',_id',
            'personal_id' => 'required|unique:'.$dbname.'.staffs,personal_id,'.$requestData['_id'].',_id',
        ];


//        return $rules;
        $validator = \Validator::make($requestData, $rules);

        if ($validator->fails()) {
            return response()->json(array('result' => false,'error' => $validator->errors()),200);
        }

        $modelStaff = new Staff;
        $modelDepartment = new Department;
        $args = $request->all();

        $args['dbname'] = $dbname;
        $staff = $modelStaff->getById($args);

        if(!$staff){
            return response()->json(array('result' => false),200);
        }
        $requestData['dbname'] = $dbname;

        $modelStaff->updateData($requestData);

        $update = $modelStaff->updateData($requestData);
//        dd($update);
//        if(!$update){
//            return response()->json(array('result' => false),200);
//        }
        $data = $modelStaff->getById($args);
        $argDept = [
            '_id' => $data['departments_id'],
            'dbname' => $dbname
        ];
        $dept = $modelDepartment->getById($argDept);
        $data['dept'] = $dept ? $dept['name'] : "";
        $data['url_image'] = isset($data['image']) ? url('img_empl/'.Session::get('dbname').'/'.$data['_id'].'.jpg'): url('img_empl/default.jpg');
        return response()->json(array('result' => true, 'data' => $data),200);


//        Session::flash('flash_message', 'Department updated!');
//
//        return redirect('erp/department/list');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $dbname = Session::get('user')['dbname'];
        $nameKey = $dbname;

        $modelStaff = new Staff;
        $dataRequest = $request->all();
        $idArr = $dataRequest['_id'];
        foreach($idArr as $value){
            $args['_id'] = $value;
            $args['dbname'] = $dbname;
            $modelStaff->movetotrash($args);
        }
        return response()->json(array('result' => true),200);
    }
    public function checkEmail($email)
    {
        $dbname = Session::get('user')['dbname'];
        $nameKey = $dbname;

        $modelStaff = new Staff;

        $args['dbname'] = $dbname;
        $args['email'] = $email;
        $checkEmail = $modelStaff->ajaxGetEmail($args);
        if($checkEmail){
            return response()->json(array('email' =>true),200);

        }
        return response()->json(array('email' =>false),200);
    }
    public function checkPersonalId($personal_id)
    {
        $dbname = Session::get('user')['dbname'];
        $nameKey = $dbname;

        $modelStaff = new Staff;

        $args['dbname'] = $dbname;
        $args['personal_id'] = $personal_id;
        $checkEmail = $modelStaff->ajaxGetPersonaId($args);
        if($checkEmail){
            return response()->json(array('personal_id' =>true),200);

        }
        return response()->json(array('personal_id' =>false),200);
    }
    public function download()
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Họ và tên')
            ->setCellValue('C1', 'Ngày sinh')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Phone')
            ->setCellValue('F1', 'CMND')
            ->setCellValue('G1', 'Số tài khoản')
            ->setCellValue('H1', 'Ngân hàng')
            ->setCellValue('I1', 'Sổ bảo hiểm')
            ->setCellValue('J1', 'Địa chỉ');

        $dbname = Session::get('dbname');

        $nameKey = $dbname;

        $modelDepartment = new Department;
        $modelStaff = new Staff;
        $args = [
            'dbname' => $dbname,
            'status' => 1,
            'is_out' => 0
        ];
        $staffs = $modelStaff->getList($args);
        $staffs = $staffs->toArray();

        $i = 2;
        foreach ($staffs as $key => $value){
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$i, $key+1)
                ->setCellValue('B'.$i, isset($value['name']) ? $value['name'] : '')
                ->setCellValue('C'.$i, isset($value['ngaysinh']) ? $value['ngaysinh'] : '')
                ->setCellValue('D'.$i, isset($value['email'])? $value['email'] : '')
                ->setCellValue('E'.$i, isset($value['phone'])? $value['phone'] : '')
                ->setCellValue('F'.$i, isset($value['personal_id'])? $value['personal_id'] : '')
                ->setCellValue('G'.$i, isset($value['bank_acc'])? $value['bank_acc'] : '')
                ->setCellValue('H'.$i, isset($value['bank_name'])? $value['bank_name'] : '')
                ->setCellValue('I'.$i, isset($value['assuren'])? $value['assuren'] : '')
                ->setCellValue('J'.$i, isset($value['address'])? $value['address'] : '');
            $i++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="userList.xlsx"');
        $objWriter->save('php://output');
    }
    public function upload(Request $request)
    {
        $modelStaff = new Staff;
        $modelDepartment = new Department;
        $dbname = Session::get('dbname');

        $filename = $request->file("file");
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objReader->setReadDataOnly(true);

        /**  Load $inputFileName to a PHPExcel Object  **/
        $objPHPExcel = $objReader->load("$filename");

        $total_sheets=$objPHPExcel->getSheetCount();

        $allSheetName=$objPHPExcel->getSheetNames();
        $objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow    = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
        $arraydata = array();
        for ($row = 2; $row <= $highestRow;++$row)
        {
            for ($col = 0; $col <$highestColumnIndex;++$col)
            {
                $value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                $arraydata[$row-2][$col]=$value;
            }
        }

        foreach ($arraydata as $user) {
            $argData = [];
            $name = (string)$user[1];
            $dob = $user[2];
            $email = $user[3];
            $sdt = (string)$user[4];
            $cmnd = (string)$user[5];
            $stk = (string)$user[6];
            $bankname = $user[7];
            $sobh = (string)$user[8];
            $diachi = $user[9];
            //check cmnd, email trung da co
            $argData = [
                'name'        => $name,
                'ngaysinh'    => $dob,
                'email'       => $email,
                'phone'       => $sdt,
                'personal_id' => $cmnd,
                'bank_acc'    => $stk,
                'bank_name'   => $bankname,
                'assuren'     => $sobh,
                'address'     => $diachi
            ];
            $argCheck = [
                'email'       => $email,
                'personal_id' => $cmnd,
                'dbname'      => $dbname
            ];
            $argData['dbname'] = $dbname;
            $argData['is_out'] = 0;
            $check = $modelStaff->checkEmalOrPersonal($argCheck);
            if ($check) {
                $modelStaff->processDataUpdateFromFile($argData);
            } else {
                $modelStaff->create($argData);
            }
        }

        $args = [
            'dbname' => $dbname,
            'status' => 1
        ];
        $staffs = $modelStaff->getList($args);
        $staffs = $staffs->toArray();
        $arr = array();
        foreach ($staffs as $key => $value){
            $arr[$key]= $value;
            $argDept = [
                '_id' => isset($value['departments_id']) ? $value['departments_id'] : "",
                'dbname' => $dbname
            ];
            $dept = $modelDepartment->getById($argDept);
            $arr[$key]['dept'] = $dept ? $dept['name'] : "";
            $arr[$key]['url_image'] = isset($value['image']) ? url('img_empl/'.Session::get('dbname').'/'.$value['_id'].'.jpg'): url('img_empl/default.jpg');
        }


        $staffs = $arr;
        return response()->json(array('result' => true,'data' => $staffs),200);


    }
    public function uploadImage(Request $request)
    {

        $dbname = Session::get('dbname');
        $modelStaff = new Staff;
        $args = $request->all();

        $args['dbname'] = $dbname;
        $staff = $modelStaff->getById($args);
        if(!$staff){
            return response()->json(array('result' => false),200);
        }
        if($request->hasFile('img')) $img = $request->file("img")->move('img_empl/'.Session::get('dbname'), $args['_id'].'.jpg');
        $args['image'] = true;
        $modelStaff->updateData($args);
        $staff['url_image'] =  ($request->hasFile('img')) ? url('img_empl/'.Session::get('dbname').'/'.$args['_id'].'.jpg'): url('img_empl/default.jpg');
        return response()->json(array('result' => false,'data' => $staff),200);
    }

    public function checkEmployer(Request $request)
    {
        $valid = "true";
        $dbname = Session::get('dbname');
        $requestData = $request->all();

        $modelStaff = new Staff;
        $requestData['dbname'] = $dbname;

        $checkEmail = $modelStaff->ajaxCheck($requestData);

        if($checkEmail){

            $valid = "false";


        }

        return $valid;
    }
    public function getEmployer(Request $request)
    {
        $dbname = Session::get('dbname');
        $requestData = $request->all();

        $modelStaff = new Staff;
        $requestData['dbname'] = $dbname;


        $staff = $modelStaff->getById($requestData);

        return $staff;


    }

    public function createStaffOut(Request $request)
    {

        $dbname = Session::get('dbname');


        $modelStaff = new Staff();

        $requestData = $request->all();
        $requestData['dbname'] = $dbname;
        $requestData['is_out'] = 1;

        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:'.$dbname.'.staffs,email',
            'phone' => 'required'
        ];
        $validator = \Validator::make($requestData, $rules);



        if ($validator->fails()) {
            return response()->json(array('success' => false,'error' => $validator->errors()),200);
        }



        $result = $modelStaff->create($requestData);
        return response()->json(array('success' => true,'id' => $result),200);

    }
   }
