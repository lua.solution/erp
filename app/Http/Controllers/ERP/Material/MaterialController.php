<?php

namespace App\Http\Controllers\ERP\Material;


use Session;
use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\ERPModels\Machines;

class MaterialController extends Controller
{
    //
    public function index(Request $request)
    {
        $dbname = Session::get('dbname');

        return view('erp.material.index');
    }

}