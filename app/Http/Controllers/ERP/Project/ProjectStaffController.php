<?php

namespace App\Http\Controllers\ERP\Project;

use Session;
use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\ERPModels\ProjectStaffs;
use App\ERPModels\Project;
use App\Staff;

class ProjectStaffController extends Controller
{
    //
	public function getRenderStaffProject(Request $request)
	{

        $dbname = Session::get('dbname');
        $modelStaff = new Staff;
        $modelProject = new Project;
        $args = [
            'dbname' => $dbname,
            '_id' => Session::get('idProject'),
        ];
//        $staffs = $modelStaff->getList(
//            [
//                'dbname' => $dbname,
//                'status' => 1
//            ]
//        );

        $staffs = $this->comboboxStaffProject();

        $projectStaff = $modelProject->__getProjectStaff($args);
        $data['staffs'] = $staffs;
        $data['projectStaff'] = $projectStaff[0];
        $arr = [];
        $emai = [];
        foreach ($data['projectStaff']  as $key => $value){
            $arr[$key] = $value;


            $argStaff = [
                '_id' => isset($value['id']) ? $value['id'] : "",
                'dbname' => $dbname
            ];
            $staff = $modelStaff->getUser($argStaff);
            $arr[$key]['staff'] = $staff ? $staff : '';
            $arr[$key]['email'] = $staff ? $staff['email'] : '';
            $email[] = $staff['email'];
            $arr[$key]['phone'] = $staff ? $staff['phone'] : '';
        }
        $data['projectStaff'] = $arr;

//        dd($data['projectStaff']);
		return view('erp.project.staff',$data);
	}
	public function addStaffToProject(Request $request){
	    $id='';
        if(Session::has('idProject')) {
//            dd(Session::get('idProject'));
            $id=Session::get('idProject');
        }
        $dbname = Session::get('dbname');
        $requestData = $request->all();
//        dd($requestData);
        $requestData['_id'] = $id;
        $requestData['dbname'] = $dbname;
        $modelProject= new Project;
        $argPs = [
            '_id' => $id,
            'dbname' => $dbname
        ];
        $projectStaff = $modelProject->__getProjectStaff($argPs);
        $check = $this->checkExistStaff($projectStaff[0],$requestData['addName']);
        if($check){
            $header = array (
                'Content-Type' => 'application/json; charset=UTF-8',
                'charset' => 'utf-8'
            );
            return response()->json(array('success' => false,'error' => 'Nhân sự này đã có trong dự án'),200);
        }

        $modelProject->__addStaff($requestData);
        return response()->json(array('success' => true),200);
        return redirect()->route('erp.project.getRenderStaffProject',$id);
    }
    public function updateStaffToProject(Request $request){
        $dbname = Session::get('dbname');
	    $id='';
        $modelProject = new Project;
        $modelStaff = new Staff;
        if(Session::has('idProject')) {
            $id=Session::get('idProject');
        } else {
            return redirect()->route('erp.project');
        }
        $requestData = $request->all();
        $requestData['_id'] = $id;
        $requestData['dbname'] = $dbname;

        $argPs = [
            '_id' => $id,
            'dbname' => $dbname
        ];
        $arrStaff = [
            'dbname' => $dbname,
            '_id' => $requestData['staff_id'],

        ];
        $checkIsOut = $modelStaff->ajaxCheck($arrStaff);

        if($checkIsOut && isset($checkIsOut['is_out']) && $checkIsOut['is_out']==1){

            $rules = [
                'email' => 'required|email|unique:'.$dbname.'.staffs,email,'.$requestData['staff_id'].',_id',
            ];

//        return $rules;
            $validator = \Validator::make(['email' => $requestData['addEmail']], $rules);

            if ($validator->fails()) {
                $validator = $validator->errors()->toArray();
                return response()->json(array('result' => false,'error' => $validator['email']),200);
            }

            $modelStaff->updateData(['dbname' => $dbname ,'_id' => $requestData['staff_id'],'email' => $requestData['addEmail'],'phone' => $requestData['addPhone'],'name' => $requestData['addName']]);

        }



        $modelProject->__updateDataStaff($requestData);
        return response()->json(array('success' => true),200);
//        if($requestData['btnSubmit']=="edit"){
//            $countStaff = count($requestData['addName']);
//            $arr[] = '';
//            for($i = 0; $i< $countStaff ;$i++){
//                $arr[$i] = [
//                    "id" => isset($requestData['addName'][$i]) ? $requestData['addName'][$i] : '',
//                    "in" => isset($requestData['addStartDate'][$i]) ? $requestData['addStartDate'][$i] : '',
//                    "out" => isset($requestData['addEndDate'][$i]) ? $requestData['addEndDate'][$i] : '',
//                    "action" => $i == 0 ? 0 : 1,
//                    "res" => isset($requestData['addJob'][$i]) ? $requestData['addJob'][$i] : '',
//                    "note" => isset($requestData['addDes'][$i]) ? $requestData['addDes'][$i] : '',
//                ];
//            }
//
//            $data = [
//                'dbname' => Session::get('dbname'),
//                '_id' => $id,
//                'project_staff' => $arr
//            ];
//            $modelProjectStaff->__updateStaff($data);
//            return redirect()->route('erp.project.getRenderStaffProject',$id);
//        }
//        else{
//            return 1;
//        }
    }


    function comboboxStaffProject(){
        $modelProject = new Project;
        $modelStaff = new Staff;

        $dbname = Session::get('dbname');

        $staffs = $modelStaff->getList(
            [
                'dbname' => $dbname,
                'status' => 1
            ]
        );

        $staffs = $staffs->toArray();
        $args = [
            'dbname' => $dbname,
            '_id' => Session::get('idProject'),
        ];
        $projectStaff = $modelProject->__getProjectStaff($args);
        $projectStaff = $projectStaff[0];
        $arr = [];
        foreach ($staffs as $st){
            $check = $this->checkExistStaff($projectStaff,$st['_id']);
            if(!$check){
                $arr[] = $st;
            }
        }

        return $arr;
    }
    function deleteStaff(Request $request){
        $id='';
        $dbname = Session::get('dbname');
        $modelProject = new Project;


        if(Session::has('idProject')) {
            $id=Session::get('idProject');
        } else {
            return redirect()->route('erp.project');
        }
        $requestData = $request->all();
        $requestData['_id'] = $id;
        $requestData['dbname'] = $dbname;
        return $modelProject->__deleteStaff($requestData);
    }
    public function checkExistStaff($array , $search){

        foreach($array as $arr){
              if($search != ""){
                if($arr['id'] == $search){
                    return true;
                }
            }
        }

        return false;
    }
}
