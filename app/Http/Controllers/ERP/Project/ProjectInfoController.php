<?php

namespace App\Http\Controllers\ERP\Project;

use Session;
use Config;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProjectInfoController extends Controller
{
	//
	public function getRenderInfoProject(Request $request)
	{
		$idProject = $request->id;
		if (empty($idProject)) {
			return redirect()->route('erp.project')->withErrors('ID Project Not Found');
		} else {
			Session::put('idProject', $idProject);
		}

		return view('erp.project.info', ['idProject' => $idProject]);
	}
}
