<?php

namespace App\Http\Controllers\ERP\Project;

use function emptyArray;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\ERPModels\ProjectWorkList;
use App\Staff;
use App\ERPModels\Project;

class ProjectWorkStaffController extends Controller
{
    public function getRenderWorkStaffProject(Request $request)
    {
        $dbname = Session::get('dbname');
        $idProject = '';
        if (Session::has('idProject'))
            $idProject = Session::get('idProject');
        else
            return redirect()->route('erp.project');
        $projectStaff = (new Project())->getStaffList($idProject);
        $projectStaff = $projectStaff[0];
        $data = [];
        $i = 0;
        foreach ($projectStaff as $staff) {
            $data[$i]['staff_id'] = $staff['id'];
            $data[$i]['staff_name'] = (new Staff())->getNameById([
                'dbname' => $dbname,
                '_id'    => $staff['id']
            ]);

            $data[$i]['task_staff'] = (new ProjectWorkList())->getTasklistByStaff([
                'dbname'  => $dbname,
                'idStaff' => $data[$i]['staff_id']
            ]);
            $j = 0;
            foreach ($data[$i]['task_staff'] as $task) {
                $totalQuantity = 0;
                if (emptyArray($task['quantity'])) {
                    foreach ($task['quantity'] as $quantity) {
                        $totalQuantity += $quantity['quantity'];
                    }
                }
                $data[$i]['task_staff'][$j]['total'] = $totalQuantity;
                $j++;
            }
            $i++;
        }

        $tasklist = (new ProjectWorkList())->getTasklist([
            'dbname'    => $dbname,
            'idProject' => $idProject
        ]);
        $data2 = [];
        $i = 0;
        foreach ($tasklist as $task) {
            if (emptyArray($task['task_staff'])) {
                $data2[$i] = $task;
                $total = 0;
                foreach ($task['quantity'] as $quantity) {
                    $total += $quantity['quantity'];
                }
                $data2[$i]['total'] = $total;
            }
            $i++;
        }
        return view('erp.project.workstaff')->with('data', $data)->with('workqueue', $data2);
    }

    public function workListUpload(Request $request)
    {
        $dbname = '';
        $projectId = '';
        if (Session::has('dbname') || Session::has('idProject')) {
            $dbname = Session::get('dbname');
            $projectId = Session::get('idProject');
        } else {
            return;
        }
        $filename = $request->file("file");
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objReader->setReadDataOnly(true);

        /**  Load $inputFileName to a PHPExcel Object  **/
        $objPHPExcel = $objReader->load("$filename");

        $total_sheets = $objPHPExcel->getSheetCount();

        $allSheetName = $objPHPExcel->getSheetNames();
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
        $arraydata = array();
        for ($row = 2; $row <= $highestRow; ++$row) {
            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                $arraydata[$row - 2][$col] = $value;
            }
        }
        foreach ($arraydata as $work) {
            $workName = $work[1];
            $startDate = $work[2];
            $endDate = $work[3];
            $unit = $work[4];
            $quantity = $work[5];
            $desc = $work[6];
            $arr = [
                'project_id' => $projectId,
                'task_staff' => [],
                'name'       => $workName,
                'start'  => $startDate,
                'end'    => $endDate,
                'unit'       => $unit,
                'quantity'   => [
                    [
                        'quantity' => $quantity,
                        'date'     => $startDate,]
                ],
                'unit_price' => '',
                'desc'       => $desc,
                'parent_id'  => '',
                'task_staff' => []
            ];
            $task = new ProjectWorkList();
            $res = $task->create($arr);
        }

        return response()->json(array('result' => true), 200);
    }

    public function workListDownload(Request $request)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Tên công việc')
            ->setCellValue('C1', 'Thời gian bắt đầu')
            ->setCellValue('D1', 'Thời gian kết thúc')
            ->setCellValue('E1', 'Người phụ trách')
            ->setCellValue('F1', 'Người thực hiện')
            ->setCellValue('G1', 'Đơn vị tính')
            ->setCellValue('H1', 'Tổng khối lượng')
            ->setCellValue('I1', 'Ghi chú')
            ->setCellValue('J1', 'Tiến độ (%)')
            ->setCellValue('K1', 'Trạng thái');

        $dbname = Session::get('dbname');
        $idProject = '';
        if (Session::has('idProject'))
            $idProject = Session::get('idProject');
        else
            return redirect()->route('erp.project');

        $projectName = (new Project())->getNameById($idProject);

        $taskList = (new ProjectWorkList())->getTasklist([
            'dbname'    => $dbname,
            'idProject' => $idProject
        ]);
        $i = 2;
        foreach ($taskList as $task) {
            $progress = $this->progressCalcToPercent($task['start'], $task['endDate']);
            $devList = '';
            $master = '';
            if (!isset($task['task_staff']) || $task['task_staff'] != null) {
                foreach ($task['task_staff'] as $staff) {
                    $id = $staff['staff_id'];
                    if ($staff['action'] == 0)
                        $master = $id;
                    $name = (new Staff())->getNameById([
                        'dbname' => $dbname,
                        '_id'    => $id
                    ]);
                    $devList .= $name . ', ';
                }
            }
            $master = (new Staff())->getNameById([
                'dbname' => $dbname,
                '_id'    => $master
            ]);
            $totalQuantity = 0;
            if (!isset($task['quantity']) || $task['quantity'] != null) {
                foreach ($task['quantity_list'] as $quantity) {
                    $totalQuantity += $quantity['quantity'];
                }
            }
            $status = 'Pending';
            if ($task['status'] == 0) $status = 'Pending';
            else if ($task['status'] == 1) $status = 'In progress';
            else if ($task['status'] == 2) $status = 'Delaying';
            else if ($task['status'] == 3) $status = 'Closed';

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $i - 1)
                ->setCellValue('B' . $i, isset($task['name']) ? $task['name'] : '')// ten cong viec
                ->setCellValue('C' . $i, isset($task['start']) ? $task['start'] : '')// start date
                ->setCellValue('D' . $i, isset($task['end']) ? $task['end'] : '')// end date
                ->setCellValue('E' . $i, $master)// master
                ->setCellValue('F' . $i, $devList)// dev list
                ->setCellValue('G' . $i, isset($task['unit']) ? $task['unit'] : '')// unit
                ->setCellValue('H' . $i, $totalQuantity)// total quantity
                ->setCellValue('I' . $i, isset($task['desc']) ? $task['desc'] : '')// Note
                ->setCellValue('J' . $i, $progress)// Progress
                ->setCellValue('K' . $i, $status); // status
            $i++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Project (' . $projectName . ').xlsx"');
        $objWriter->save('php://output');
    }
}
