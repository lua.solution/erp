<?php

namespace App\Http\Controllers\ERP\Project;

use App\ERPModels\Staffs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Session;
use Config;
use Illuminate\Support\Facades\DB;
use App\ERPModels\Project;

class ProjectsController extends Controller
{
	//
	public function getRenderProject()
	{
		$dbname = Session::get('dbname');
		$staffs = DB::connection($dbname)
			->collection('staffs')
			->get();

		$project = DB::connection($dbname)
			->collection('projects')
			->where('status', '=', 0)
			->get();

		$idLeader = null;
		$holdData = [];

		foreach ($project as $subProject) {
			foreach ($subProject['project_staff'] as $user) {
				if (isset($user['action'])) {
					if ($user['action'] == 0) {
						$idLeader = $user['id'];
						break;
					}
				}
			}
			$item = $user;
			if ($idLeader) {
				$item['leader'] = DB::connection($dbname)
					->collection('staffs')
					->where('_id', $idLeader)
					->pluck('name')
					->first();
			} else {
				$item['leader'] = null;
			}

			$holdData[] = $item;
		}
		$leader = $holdData;
		unset($holdData);
		$data = [
			'staffs'  => $staffs,
			'leader'  => $leader,
			'project' => $project,
		];

		return view('erp.project.project', $data);
	}

	public function postCreateProject(Request $request)
	{
		$dbname = Session::get('dbname');
		$requestData = $request->all();
		$requestData['dbname'] = $dbname;
		$rules = array(
			'name'  => 'required|min:3|max:255',
			'start' => 'required',
			'end'   => 'required',
		);

		$validator = \Validator::make($requestData, $rules);
		if ($validator->fails()) {
			return response()->json(array('result' => false), 200);
		}

		$modelProject = new Project();
		$result = $modelProject->__createProject($requestData);

		if ($result) {
			return redirect()->route('erp.project')->with('message', 'Project added!');
		} else {
			return redirect()->route('erp.project')->with('message', 'Project not added!');
		}
	}

	public function getMoveToTrashProject(Request $request)
	{
		$dbName = Session::get('dbname');
		$idProject = $request->id;
		if (empty($idProject)) {
			return redirect()->route('erp.project.createProject')->withErrors('message', 'Project ID not found');
		} else {
			$modelProject = new Project();
			$agrs = array(
				'_id'    => $idProject,
				'dbname' => $dbName,
			);

			$excute = $modelProject->__moveToTrash($agrs);
			if ($excute) {
				return redirect()->route('erp.project')->with('message', 'The project has been moved to trash!');
			} else {
				return redirect()->route('erp.project')->withErrors('message', 'The move to the trash failed!');
			}
		}
	}

	public function getSearchProject(Request $request)
	{
		$nameProject = $request->nameProject;
		$dbname = Session::get('dbname');
		$staffs = DB::connection($dbname)->collection('staffs')->get();

		$project = DB::connection($dbname)
			->collection('projects')
			->where('name', 'like', "%{$nameProject}%")
			->where('status', '=', 0)
			->get();

		$idLeader = null;
		$holdData = [];

		foreach ($project as $subProject) {
			foreach ($subProject['project_staff'] as $user) {
				if ($user['action'] == 0) {
					$idLeader = $user['id'];
					break;
				}
			}
			$item = $user;
			if ($idLeader) {
				$item['leader'] = DB::connection($dbname)
					->collection('staffs')
					->where('_id', $idLeader)
					->pluck('name')
					->first();
			} else {
				$item['leader'] = null;
			}

			$holdData[] = $item;
		}
		$leader = $holdData;
		unset($holdData);
		$data = [
			'staffs'  => $staffs,
			'leader'  => $leader,
			'project' => $project,
		];

		return view('erp.project.project', $data);
	}

	public function postEditProject(Request $request)
	{
		$idProject = $request->idProject;
		if (empty($idProject)) {
			return redirect()->route('erp.project')->withErrors('ID project not found!!!');
		} else {
			$staff_id = $request->manage;
			$nameEdit = $request->nameEdit;
			$startEdit = $request->startEdit;
			$endEdit = $request->endEdit;
			$descriptionEdit = $request->descriptionEdit;
			$data = [
				'_id'         => $idProject,
				'staff_id'    => $staff_id,
				'name'        => $nameEdit,
				'start'       => $startEdit,
				'end'         => $endEdit,
				'description' => $descriptionEdit,
			];
			$modelProject = new Project();
			$excute = $modelProject->__update($data);
			if ($excute) {
				return redirect()->route('erp.project')->with('message', 'Update success!!!');
			} else {
				return redirect()->route('erp.project')->withErrors('Update failed!!!');
			}
		}
	}

	public function getRenderProjectById(Request $request)
	{
		$idProject = $request->id;
		if(empty($idProject)) {
			return redirect()->route('erp.project')->withErrors('ID project not found!!!');
		} else {
			return view('erp.project.projectStaff', ['idProject' => $idProject]);
		}
	}

	public function postAddGroupTask(Request $request)
	{
		$idProject = $request->idProject;
		$groupTask = $request->groupTask;
		$data = [
			'_id' => $idProject,
		    'group_task' => $groupTask,
		];

		$modelProject = new Project();
		$excute = $modelProject->__update($data);
		$excute = $modelProject->__updateFillable($data);
		if($excute) {
			return redirect()->route('erp.project.getRenderByProjectId', ['id' => $idProject])->with('message', 'Update success!!!');
		} else {
			return redirect()->route('erp.project.getRenderByProjectId', ['id' => $idProject])->withErrors('Update failed!!!');
		}
	}
}
