<?php

namespace App\Http\Controllers\ERP\Project;

use function emptyArray;
use Illuminate\Support\Facades\DB;
use App\ERPModels\ProjectWorkList;
use App\Staff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PHPExcel;
use PHPExcel_IOFactory;
use Session;

class ProjectWorkListController extends Controller
{
    public function getRenderWorkListProject(Request $request)
    {
        return view('erp.project.worklist');
    }

    public static function getChild($id)
    {
        return DB::connection(Session::get('dbname'))->collection('tasks')->where('parent_id', $id)->get();
    }

    public static function veryStupidFunction($id, $prev)
    {
        $res = self::getChild($id);
        if (empty($res)) return;

        $i = 1;
        foreach ($res as $task) {
            $totalQuantity = 0;
            if (!isset($task['quantity']) || $task['quantity'] != null) {
                foreach ($task['quantity_list'] as $quantity) {
                    $totalQuantity += $quantity['quantity'];
                }
            }
            $masterID = '';
            foreach($task['task_staff'] as $staff) {
                if($staff['action'] == 0) {
                    $masterID = $staff['id'];
                    break;
                }
            }
            $url = asset("img_empl/default.jpg");
            if($masterID != '' && (new Staff())->checkHaveImg($masterID)) {
                $url = asset('img_empl/'. Session::get('dbname').'/'.$masterID.'.jpg');
            }
            echo '<div class="rTableRow"><div class="rTableCell">';
            echo '<span class="number"><a href="#"></a>' . $prev . '.' . $i . '</span></div>';
            echo '<div class="rTableCell">' . $task['name'] . '</div>';
            echo '<div class="rTableCell">' . $task['unit'] . '</div>
                                        <div class="rTableCell">'. $totalQuantity.'</div>
                                        <div class="rTableCell price">' . $task['unit_price'] . '</div>
                                        <div class="rTableCell money">Thành tiền</div>
                                        <div class="rTableCell">
                                            <div class="start-time">' . $task['start'] . '</div>
                                            <div class="end-time">' . $task['end'] . '</div>
                                        </div>
                                        <div class="rTableCell">
                                            <a href="#" class="img-member" style="background-image: url('. $url .')"></a>
                                        </div>
                                        <div class="rTableCell">
                                            <div class="progress-group">
                                            <span class="progress-text">
                                                <span class="start">' . $task['start'] . '</span> <span
                                                        class="end">' . $task['end'] . '</span>
                                            </span>
                                                <span class="progress-number">'. $totalQuantity.'</span>

                                                <div class="progress sm">
                                                    <div class="progress-bar progress-bar-aqua"
                                                         style="width: 80%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rTableCell">
                                            <p class="btn-box btn-trans">
                                                <i class="fa fa-plus"></i>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#createJobChild" 
                                                onclick="addEmployer(\''.(string)$task['_id'].'\', \''. $task['name'].'\')"></a>
                                            </p>
                                            <p class="btn-box btn-trans">
                                                <i class="fa fa-pencil"></i>
                                                <a href="#" data-toggle="modal" data-target="#editJobChild"
                                                onclick="editEmployer(\''.(string)$task['_id'].'\', \''. $task['name'].'\')"></a>
                                            </p>
                                            <button type="button" class="delete-emp btn-trans"><i
                                                        class="fa fa-trash-o"></i></button>
                                        </div>
                                    </div>';
            self::veryStupidFunction((string)$task['_id'], $prev . '.' . $i);
            $i++;
        }
    }

    public static function render()
    {
        $parent = DB::connection(Session::get('dbname'))
            ->collection('tasks')
            ->where('project_id', Session::get('idProject'))
            ->where('parent_id', "")
            ->get();

        foreach ($parent as $task) {
            $level = 1;
            $totalQuantity = 0;
            if (!isset($task['quantity']) || $task['quantity'] != null) {
                foreach ($task['quantity_list'] as $quantity) {
                    $totalQuantity += $quantity['quantity'];
                }
            }
            $masterID = '';
            foreach($task['task_staff'] as $staff) {
                if($staff['action'] == 0) {
                    $masterID = $staff['id'];
                    break;
                }
            }
            $url = asset("img_empl/default.jpg");
            if($masterID != '' && (new Staff())->checkHaveImg($masterID)) {
                $url = asset('img_empl/'. Session::get('dbname').'/'.$masterID.'.jpg');
            }
            echo '<div class=" parents"><div class="rTableRow">';
            echo '<div class="rTableCell"><span class="number"><a href="#"></a>' . $level . '</span></div>';
            echo '<div class="rTableCell">' . $task['name'] . '</div>';
            echo '<div class="rTableCell">' . $task['unit'] . '</div>
                                    <div class="rTableCell">'. $totalQuantity .'</div>
                                    <div class="rTableCell price">' . $task['unit_price'] . '</div>
                                    <div class="rTableCell money">Thành tiền</div>
                                    <div class="rTableCell">
                                        <div class="start-time">' . $task['start'] . '</div>
                                        <div class="end-time">' . $task['end'] . '</div>
                                    </div>
                                    <div class="rTableCell">
                                        <a href="#" class="img-member" style="background-image: url('. $url .')"></a>
                                    </div>
                                    <div class="rTableCell">
                                        <div class="progress-group">
                                            <span class="progress-text">
                                                <span class="start">' . $task['start'] . '</span> <span
                                                        class="end">' . $task['end'] . '</span>
                                            </span>
                                            <span class="progress-number">'. $totalQuantity .'</span>

                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-aqua" style="width: 80%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rTableCell">
                                        <p class="btn-box btn-trans">
                                            <i class="fa fa-plus"></i>
                                            <a href="#" data-toggle="modal" data-target="#createJobChild" 
                                                onclick="addEmployer(\''.(string)$task['_id'].'\', \''. $task['name'].'\')"></a>
                                        </p>
                                        <p class="btn-box btn-trans">
                                            <i class="fa fa-pencil"></i>
                                            <a href="#" data-toggle="modal" data-target="#editJobChild"
                                            onclick="editEmployer(\''.(string)$task['_id'].'\', \''. $task['name'].'\')"></a>
                                        </p>
                                        <button type="button" class="delete-emp btn-trans"><i class="fa fa-trash-o"></i>
                                        </button>
                                    </div>
                                </div>
                                <!-- child --->
                                <div class="childs">';
            self::veryStupidFunction((string)$task['_id'], $level);
            echo '</div><!-- end child --></div><!-- endforeach -->';
        }
    }

    public function workListUpload(Request $request)
    {
        $dbname = '';
        $projectId = '';
        if (Session::has('dbname') || Session::has('idProject')) {
            $dbname = Session::get('dbname');
            $projectId = Session::get('idProject');
        } else {
            return;
        }
        $filename = $request->file("file");
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);

        $objReader->setReadDataOnly(true);

        /**  Load $inputFileName to a PHPExcel Object  **/
        $objPHPExcel = $objReader->load("$filename");

        $total_sheets = $objPHPExcel->getSheetCount();

        $allSheetName = $objPHPExcel->getSheetNames();
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();
        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);
        $arraydata = array();
        for ($row = 2; $row <= $highestRow; ++$row) {
            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                $arraydata[$row - 2][$col] = $value;
            }
        }
        foreach ($arraydata as $work) {
            $workName = $work[1];
            $startDate = $work[2];
            $endDate = $work[3];
            $unit = $work[4];
            $quantity = $work[5];
            $desc = $work[6];
            $arr = [
                'project_id'    => $projectId,
                'task_staff'    => [],
                'name'          => $workName,
                'start'         => $startDate,
                'end'           => $endDate,
                'unit'          => $unit,
                'quantity'      => [
                    [
                        'quantity' => $quantity,
                        'date'     => $startDate,]
                ],
                'unit_price'    => '',
                'desc'          => $desc,
                'parent_id'     => '',
                'group_task_id' => '',
                'task_staff'    => []
            ];
            $task = new ProjectWorkList();
            $res = $task->create($arr);
        }

        return response()->json(array('result' => true), 200);
    }

    public function workListDownload(Request $request)
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'STT')
            ->setCellValue('B1', 'Tên công việc')
            ->setCellValue('C1', 'Thời gian bắt đầu')
            ->setCellValue('D1', 'Thời gian kết thúc')
            ->setCellValue('E1', 'Người phụ trách')
            ->setCellValue('F1', 'Người thực hiện')
            ->setCellValue('G1', 'Đơn vị tính')
            ->setCellValue('H1', 'Tổng khối lượng')
            ->setCellValue('I1', 'Ghi chú')
            ->setCellValue('J1', 'Tiến độ (%)')
            ->setCellValue('K1', 'Trạng thái');

        $dbname = Session::get('dbname');
        $idProject = '';
        if (Session::has('idProject'))
            $idProject = Session::get('idProject');
        else
            return redirect()->route('erp.project');

        $projectName = (new ProjectWorkList())->getNameById($idProject);

        $taskList = (new ProjectWorkList())->getTasklist([
            'dbname'    => $dbname,
            'idProject' => $idProject
        ]);
        $i = 2;
        foreach ($taskList as $task) {
            $progress = $this->progressCalcToPercent($task['start'], $task['end']);
            $devList = '';
            $master = '';
            if (!isset($task['task_staff']) || $task['task_staff'] != null) {
                foreach ($task['task_staff'] as $staff) {
                    $id = $staff['id'];
                    if ($staff['action'] == 0)
                        $master = $id;
                    $name = (new Staff())->getNameById([
                        'dbname' => $dbname,
                        '_id'    => $id
                    ]);
                    $devList .= $name . ', ';
                }
            }
            $master = (new Staff())->getNameById([
                'dbname' => $dbname,
                '_id'    => $master
            ]);
            $totalQuantity = 0;
//            if (!isset($task['quantity']) || $task['quantity'] != null) {
//                foreach ($task['quantity'] as $quantity) {
//                    $totalQuantity += $quantity['quantity'];
//                }
//            }
            $status = 'Pending';
            if ($task['status'] == 0) $status = 'Pending';
            else if ($task['status'] == 1) $status = 'In progress';
            else if ($task['status'] == 2) $status = 'Delaying';
            else if ($task['status'] == 3) $status = 'Closed';

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $i - 1)
                ->setCellValue('B' . $i, isset($task['name']) ? $task['name'] : '')
                ->setCellValue('C' . $i, isset($task['start']) ? $task['start'] : '')
                ->setCellValue('D' . $i, isset($task['end']) ? $task['end'] : '')
                ->setCellValue('E' . $i, isset($master) ? $master : '')
                ->setCellValue('F' . $i, $devList)
                ->setCellValue('G' . $i, isset($task['unit']) ? $task['unit'] : '')
                ->setCellValue('H' . $i, $totalQuantity)
                ->setCellValue('I' . $i, isset($task['desc']) ? $task['desc'] : '')
                ->setCellValue('J' . $i, $progress)
                ->setCellValue('K' . $i, $status);
            $i++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        ob_end_clean();
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Project (' . $projectName . ').xlsx"');
        $objWriter->save('php://output');
    }

    private function progressCalcToPercent($start, $end)
    {
        $startDate = $start[0] . $start[1] . '/' . $start[3] . $start[4] . '/' . $start[6] . $start[7] . $start[8] . $start[9];
        $endDate = $end[0] . $end[1] . '/' . $end[3] . $end[4] . '/' . $end[6] . $end[7] . $end[8] . $end[9];

        $now = date("d/m/Y");

        return 'calculating';
    }

    public function getMoreWorkListProject(Request $request)
    {
        $id = $_POST[0];

        return response()->json($id, 200);
    }

    public function workListAddChild(Request $request) {
        dd($request->all());
    }
}
