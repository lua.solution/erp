<?php

namespace App\Http\Controllers\ERP\Department;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Department;
use App\Staff;
use Illuminate\Http\Request;
use Session;
use Config;
use App;

class DepartmentController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $requestData = $request->all();
        $dbname = Session::get('dbname');
        $modelDepartment = new Department;
        $modelStaff = new Staff;

        $keyword = $request->get('search');
        $args = [
            'dbname' => $dbname,
            'keyword' => $keyword,
            'status' => 1
        ];
        $departments = $modelDepartment->getList($args);
        $departments = $departments->toArray();
        //dump($departments);
        $arr = array();
//        foreach ($departments['data'] as $key => $value){
//            $arr[$key]= $value;
//            $argParent = [
//                '_id' => $value['parent_id'],
//                'dbname' => $dbname
//            ];
//            $parent = $modelDepartment->getById($argParent);
//            $arr[$key]['parent'] = $parent ? $parent['name'] : "0";
//        }
        foreach ($departments as $key => $value){
            $_id = (string)($value['_id']);

            $arr[$key]= $value;
            $argEmp = [
                'departments_id' => $_id,
                'dbname' => $dbname,
            ];
            $arr[$key]['count_staff']= $modelDepartment->countStaff($argEmp);
            $arr[$key]['truongphong']= $modelDepartment->getManager($argEmp)['name'];

        }

        $departments = $arr;

        $data['departments'] = $departments;


        return view('erp.department.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $dbname = Session::get('user')['dbname'];
        $nameKey = $dbname;

        Config::set('database.connections.' . $nameKey, array(
            'driver'    => 'mongodb',
            'host'      => '127.0.0.1',
            'port'		=> 27017,
            'database'  => $dbname,
        ));

        $modelDepartment = new Department;
        $args['dbname'] = $dbname;
        $args['parent_id'] = "0";
        $departmentParent = $modelDepartment->getParent($args);
        $data['departmentParent'] = $departmentParent;
        return view('erp.department.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $dbname = Session::get('dbname');



        $modelDepartment = new Department;
        $requestData = $request->all();
        $requestData['dbname'] = $dbname;

        $rules = [
            'name' => 'required|unique:'.$dbname.'.departments,name',
//            'code' => 'required|unique:'.$dbname.'.departments,code',
        ];
        $validator = \Validator::make($requestData, $rules);

        if ($validator->fails()) {
            return response()->json(array('result' => false,'error' => $validator->errors()),200);
        }

        $result = $modelDepartment->create($requestData);
        $requestData['_id'] = $result;
        $requestData['count_staff'] = 0;
        $requestData['result'] = true;
//        return $result;
        return response()->json(array('result' => true,'data' => $requestData),200);
//        if($result){
//            Session::flash('flash_message', 'Department added!');
//        }
//
//        return redirect('erp/department/list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {


        $modelDepartment = new Department;
        $dbname = Session::get('dbname');
        $argDep = [
            '_id' => $id,
            'dbname' => $dbname
        ];
        $department = $modelDepartment->getById($argDep);

        if(!$department){
            return response()->json(['success' => false]);
        }
        $department['success'] = true;

        return response()->json($department,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $dbname = Session::get('user')['dbname'];
        $nameKey = $dbname;

        Config::set('database.connections.' . $nameKey, array(
            'driver'    => 'mongodb',
            'host'      => '127.0.0.1',
            'port'		=> 27017,
            'database'  => $dbname,
        ));

        $modelDepartment = new Department;
        $args['dbname'] = $dbname;
        $args['parent_id'] = "0";
        $departmentParent = $modelDepartment->getParent($args);
        $data['departmentParent'] = $departmentParent;

        $argDep = [
            '_id' => $id,
            'dbname' => $dbname
        ];
        $data['department'] = $modelDepartment->getById($argDep);
        return view('erp.department.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {

        $requestData = $request->all();
        $dbname = Session::get('dbname');


        $rules = [
            'name' => 'required|unique:'.$dbname.'.departments,name,'.$requestData['_id'].',_id',
//            'code' => 'required|unique:'.$dbname.'.departments,code,'.$requestData['_id'].',_id',
        ];
//        return $rules;
        $validator = \Validator::make($requestData, $rules);

        if ($validator->fails()) {
            return response()->json(array('result' => false,'error' => $validator->errors()),200);
        }
        $modelStaff = new Staff;
        $modelDepartment = new Department;
        $args = $request->all();

        $args['dbname'] = $dbname;
        $department = $modelDepartment->getById($args);

        if(!$department){
            return response()->json(array('result' => false),200);
        }
        $requestData['dbname'] = $dbname;

        $modelStaff->updateData($requestData);

        $modelDepartment->updateData($requestData);
        $data = $modelDepartment->getById($args);

        $argEmp = [
            'departments_id' => $requestData['_id'],
            'dbname' => $dbname,
        ];
        $data['count_staff']= $modelDepartment->countStaff($argEmp);
        $truongphong = $modelDepartment->getManager($argEmp);
        $data['truongphong']= isset($truongphong['name']) ? $truongphong['name'] : '';
        return response()->json(array('result' => true, 'data' => $data),200);

//        Session::flash('flash_message', 'Department updated!');
//
//        return redirect('erp/department/list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {

        $dbname = Session::get('dbname');
        $nameKey = $dbname;

        $modelDepartment = new Department();
//        $args['_id'] = $id;
        $dataRequest = $request->all();
        if(isset($dataRequest['_id'])){
            $idArr = $dataRequest['_id'];
            foreach($idArr as $value){
                $args['_id'] = $value;
                $args['dbname'] = $dbname;

                $department = $modelDepartment->getById($args);

                if($department){
                    $modelDepartment->movetotrash($args);
                }
            }
            return response()->json(array('result' => true),200);
        }
        return response()->json(array('result' => false),200);
    }
}
